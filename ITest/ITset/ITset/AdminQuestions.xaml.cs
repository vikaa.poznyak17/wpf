﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ITset
{
    /// <summary>
    /// Логика взаимодействия для AdminQuestions.xaml
    /// </summary>
    public partial class AdminQuestions : Window
    {
        public static int quas; string otvet;
        public AdminQuestions()
        {
            InitializeComponent();
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            switch (AdminLevel.numm)
            {
                case 1: // Git
                    switch (AdminNumber.levv)
                    {
                        case 1: // Начальный уровень
                            switch (quas)
                            {
                                case 1: // вопросы
                                    Met("Команда git stach сохраняет все новые файлы, которые еще не отслеживаются?", "Да", "Нет", "В определенных ситуациях", "Не знаю");
                                    otvet = b2.Name;
                                    break;
                                case 2:
                                    Met("Опишите, что следующие git-команды делают с историей коммитов.\ngit reset --hard HEAD~5\ngit merge --squash HEAD@{1}", "Сбрасывают HEAD до 5-го коммита в репозитории, затем сливаются с мастером", "Отбрасывает 5 последних коммитов, а затем собирает их в один коммит", "Удаляет последние 5 коммитов", "Мержит последние 5 коммитов в новую ветку");
                                    otvet = b2.Name;
                                    break;
                                case 3:
                                    Met("Что делает команда git reser --soft HEAD^ ?", "Удаляет все предыдущие коммиты и откатывает историю репозиторияя в исходное состояние", "Откатывает рабочую ветку на первый коммит", "Сохраняет HEAD на текущем коммите, но удаляет все предыдущие коммиты", "Отменяет последний коммит в рабочей ветке и устанавливает HEAD на предыдущий коммит");
                                    otvet = b4.Name;
                                    break;
                                case 4:
                                    Met("Какая команда позволит применить последние изменения в stash?", "got stash pop", "git stach last", "git stash pop .", "git stash apply");
                                    otvet = b1.Name;
                                    break;
                                case 5:
                                    Met("Можно ли склонировать локальный репоиторий с помощью команды git clone ?", "Да, можно клонировать и удаленные и локальные репозитории", "Нет, можно клонировать только удаленные репозитории", "Да, можно клонировать только локальные репозитории", "Нет, репозитории тользя клонировать");
                                    otvet = b1.Name;
                                    break;
                                case 6:
                                    Met("Какая команда позволяет создать новый репозиторий?", "git create", "git start", "git init", "git initialize");
                                    otvet = b3.Name;
                                    break;
                                case 7:
                                    Met("Для удобства в склонированном репозитории создается удаленное соединение, которое называется ...?", "remote", "origin", "source", "original");
                                    otvet = b2.Name;
                                    break;
                                case 8:
                                    Met("Что произойдет, если вы попытаетесь удалить ветку, которая еще не была слита, используя команду\ngit branch -d branch_name ?", "Ветка будет удалена", "Гит запросит подтверждение действия", "Error: The branch 'branch_name' is not fully merged", "Ничего не произойдет");
                                    otvet = b3.Name;
                                    break;
                                case 9:
                                    Met("Как можно объединить несколько коммитов без использования git merge --squash ?", "Никак, git merge --squash - это единственная команда, которая позволяет такое сделать", "С помощью git reset", "С помощью git rebase", "С помощью git concat");
                                    otvet = b3.Name;
                                    break;
                                case 10:
                                    Met("Как удалить stash по имени?", "git stash clear <name>", "git stash pop <name>", "git stash drop <name>", "git stash delete <name>");
                                    otvet = b3.Name;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case 2: // Средний уровень
                            switch (quas)
                            {
                                case 1: // вопросы
                                    Met("Что сделает команда git branch без какого-либо параметра?", "Переключится на последнюю используемую ветку", "Выведет ошибку", "Выведет список локальных веток", "Выведет список удаленных (remote) веток");
                                    otvet = b3.Name;
                                    break;
                                case 2:
                                    Met("Как удалить все игнорируемые и неотслеживаемые, но не игнорируемые файлы из рабочей\nдиректории?", "git delete -fx", "git clean-fX", "git remote -fX", "git clean -fx");
                                    otvet = b4.Name;
                                    break;
                                case 3:
                                    Met("Как удалить все игнорируемые файлы из рабочей директории?", "git delete -fx", "git clean -fX", "git clean -fx", "git remote -fX");
                                    otvet = b2.Name;
                                    break;
                                case 4:
                                    Met("Какая команда используется для загрузки данных с удаленного репозитория и сразу выполняет\ngit merge для создания коммита слияния?", "git pull", "git merge", "git fetch", "git remote");
                                    otvet = b1.Name;
                                    break;
                                case 5:
                                    Met("Как удалить все стеши?", "git stash clear", "git stash remote", "git stash delete", "git stash rm");
                                    otvet = b1.Name;
                                    break;
                                case 6:
                                    Met("Можно ли изменять и комитить изменения в репозиторий, который был создан с флагом --bare ?", "Да", "Да, но с использованием флага --force при коммите", "Нет, вы можете использовать только push и pull с таким репозиторием", "Нет, потому что это удаленный репозиторий");
                                    otvet = b3.Name;
                                    break;
                                case 7:
                                    Met("Как удалить локальную ветку независимо от того, была смержена она или нет?", "git branch -d <branch>", "git branch -D <branch>", "git branch -di <branch>", "git branch remote <branch>");
                                    otvet = b2.Name;
                                    break;
                                case 8:
                                    Met("Какая команда позволит увидеть, какие ветки уже объединены в ветку, в которой вы работаете?", "git branch --merged", "git branch --no-merged", "git branch -m", "git branch --yes-merged");
                                    otvet = b1.Name;
                                    break;
                                case 9:
                                    Met("Какие аргументы используются, чтобы указать git stash также сохранить изменения в игнорируемых\nфайлах?", "git stash --all", "git stash -i", "git stash -u", "git stash --iall");
                                    otvet = b1.Name;
                                    break;
                                case 10:
                                    Met("Какая команда используется для начала интерактивного промежуточного сеанса, чтобы выбрать части\nфайла для добавления в коммит?", "git add -s", "git add -p", "git stage -2", "git stage -p");
                                    otvet = b2.Name;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case 3: // Сложный уровень
                            switch (quas)
                            {
                                case 1: // вопросы
                                    Met("Для чего нужен GitFlow?", "Это способ организации работы с Git в большой команде", "Это способ организации работы с Git в большом проекте", "Это способ организации работы с Git в любой команде и на любом проекте", "Такого понятия нет");
                                    otvet = b3.Name;
                                    break;
                                case 2:
                                    Met("Что делает команда git abuse ?", "Удаляет лишние файлы из репозитория, очищает буфер и конфиг от мусора", "Находит и показывает все файлы с неправильным форматом", "Такой команды нет, есть только такой ключ: git --abuse", "Такой команды нет, есть команда: git reset");
                                    otvet = b4.Name;
                                    break;
                                case 3:
                                    Met("Как добавить новую директорию в Git?", "Добавить каждый файл из этой директории в Git", "Добавить хотя бы один файл из этой директории в Git", "Никак, Git работает только с файлами", "Команда git add -d");
                                    otvet = b2.Name;
                                    break;
                                case 4:
                                    Met("Для чего надо добавлять файлы в .gitignore?", "Чтобы Git удалял их историю, храня только последнюю версию", "Чтобы Git при работе с ними переспрашивал 'Are you sure you want to interact with this file?'", "Чтобы Git не замечал их и любые команды Git не могли их заафектить", "");
                                    otvet = b3.Name;
                                    break;
                                case 5:
                                    Met("Как применить патч в Git?", "Команда git apply path/to/file", "Команда git patch path/to/file", "Команда git add path/to/file", "Такого понятия все еще нет");
                                    otvet = b1.Name;
                                    break;
                                case 6:
                                    Met("Как привести измененный файл в начальное состояние (до изменения)?", "Команда git abort path/to/file", "Команда git checkout path/to/file", "Команда git pull path/to/file", "Команда git commit path/to/file");
                                    otvet = b2.Name;
                                    break;
                                case 7:
                                    Met("Как узнать, кто автор строчки в файле, используя систему Git?", "Команда git show --author", "Команда git commit --autho", "Команда git blame", "Команда git status");
                                    otvet = b3.Name;
                                    break;
                                case 8:
                                    Met("Как удалить локальную ветку my_branch?", "Команда git delete my_branch", "Команда git branch -D my_branch", "Команда git reset my_branch", "Никак, Git как раз существует для того, чтобы никакие изменения нельзя было удалить");
                                    otvet = b2.Name;
                                    break;
                                case 9:
                                    Met("Как скачать ветку their_branch, если она уже есть в удаленном (remote) репозитории, но не локально?", "Команда git clone their_branch", "Команда git get origin their_branch", "Команда git fetch origin their_branch", "Команда git clone origin their_branch");
                                    otvet = b3.Name;
                                    break;
                                case 10:
                                    Met("Как сделать ветку с названием my_branch?", "Команда git branch my_branch", "Команда git create branch my_branch", "Команда git commit origin my_branch", "Команда git checkout my_branch");
                                    otvet = b1.Name;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        default:
                            break;
                    }
                    break;
                case 2: // SQL
                    switch (AdminNumber.levv)
                    {
                        case 1: // Начальный уровень
                            switch (quas)
                            {
                                case 1: // вопросы
                                    Met("Какой запрос позволит получить всех пользователей так, чтобы новые оказались в начале.\nДата регистрации - 'reg_date'", "SELECT * FROM users ORDER BY reg_date DESC", "SELECT * FROM users SORT BY reg_date DESC", "SELECT * FROM users ORDER ASC BY reg_date", "SELECT * FROM users SORT(reg_date)");
                                    otvet = b1.Name;
                                    break;
                                case 2:
                                    Met("Как обновить запись в таблице 'Users', где имя пользователя (поле 'name') равно 'FakeUser'?", "MODIFY Users SET activated = 0 WHERE name = 'FakeUser'", "ALTER Users MODIFY activated = 0 WHERE name = 'FakeUser'", "UPDATE Users(activated = 0) WHERE name is 'FakeUser'", "UPDATE Users MODIFY activated = 0 WHERE name = 'FakeUser'");
                                    otvet = b2.Name;
                                    break;
                                case 3:
                                    Met("Как выбрать все записи из таблицы 'Customers', где значение поля 'City' начинается с 'V'?", "SELECT * FROM Customers WHERE City='V%'", "SELECT * FROM Customers WHERE City LIKE 'V%'", "SELECT * FROM Customers WHERE City LIKE '%V'", "SELECT * FROM Customers WHERE City='%V%'");
                                    otvet = b2.Name;
                                    break;
                                case 4:
                                    Met("Какой оператор SQL используется для вставки новых данных в базу данных?", "STORE", "ADD", "INSERT INTO", "ADD RECORD");
                                    otvet = b3.Name;
                                    break;
                                case 5:
                                    Met("Как вернуть количество записей в таблице 'Customers'?", "SELECT COUNT(*) FROM Customers", "SELECT COLUMNS(*) FROM Customers", "SELECT COUNT() FROM Customers", "SELECT COLUMNS() FROM Customers");
                                    otvet = b1.Name;
                                    break;
                                case 6:
                                    Met("Как добавить новую запись в таблицу 'Customers'?", "INSERT VALUES ('Michael','Boston') INTO Customers", "INSERT ('Michael','Boston') INTO Customers", "None of the listed above", "INSERT INTO Customers ('Michael','Boston')");
                                    otvet = b4.Name;
                                    break;
                                case 7:
                                    Met("Какое ключевое слово используется для получения значений в промежутке?", "RANGE", "WITHIN", "BETWEEN", "IN");
                                    otvet = b3.Name;
                                    break;
                                case 8:
                                    Met("Как выбрать столбец с именем 'FirstName' из таблицы с именем 'Persons'?", "EXTRACT FirstName FROM Persons", "SELECT Persons.FirstName", "SELECT Persons FROM FirstName", "SELECT FirstName FROM Persons");
                                    otvet = b4.Name;
                                    break;
                                case 9:
                                    Met("Что такое реляционные базы данных?", "База данных, в которой информация хранится в виде двумерных таблиц, связанных между собой", "База данных, в которой одна ни с чем не связанная таблица", "Любая база данных - реляционная", "Совокупность данных, не связанных между собой");
                                    otvet = b1.Name;
                                    break;
                                case 10:
                                    Met("Как выглядит запрос для вывода всех значений из таблицы 'Orders'?", "SELECT ALL FROM Orders", "SELECT % FROM Orders", "SELECT * FROM Orders", "SELECT * Orders FROM Orders");
                                    otvet = b3.Name;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case 2: // Средний уровень
                            switch (quas)
                            {
                                case 1: // вопросы
                                    Met("Какие данные мы получим из этого запроса?\nselect id, date, customer_name from Orders", "Неотсортированные номера и даты всех заказов с именами заказчиков", "Никакие, запрос составлен неверно", "Номера и даты всех заказов с именами заказчиков, отсортированные по первой колонке", "Номера и даты всех заказов с именами заказчиков, отсортированные по всем колонкам, содержащим слово Order");
                                    otvet = b1.Name;
                                    break;
                                case 2:
                                    Met("Что покажет следующий запрос:\nselect concat('index,' ','city') AS delivery_address from Orders", "соединит поля с индексом и адресом из таблицы Orders и покажет их с псевдонимом delivery_address", "покажет уникальные значения индексов и адресов из таблицы Orders", "ничего, запрос составлен неверно", "соединит поля с индексом и адресом из таблицы Orders, но покажет их без псевдонима");
                                    otvet = b1.Name;
                                    break;
                                case 3:
                                    Met("Что покажет следующий запрос?\nselect * from Orders where date between '2017-01-01' and '2017-12-31'", "Все данные по заказам, совершенным за 2017 год, за исключением 01 января 2017 года", "Все данные по заказам, совершенным за 2017 год, за исключением 31 декабря 2017 года", "Все данные по заказам, совершенным за 2017 год", "Ничего, запрос составлен неверно");
                                    otvet = b3.Name;
                                    break;
                                case 4:
                                    Met("Что не так с этим запросом?\nselect id, date from Orders where seller_id=NULL", "Все верно, запрос покажет все заказы, продавцы которых не проставлены", "NULL нужно взять в кавычки", "Сравнение с NULL можно проводить только с оператором IS", "Сравнение с NULL можно проводить только с оператором ON");
                                    otvet = b3.Name;
                                    break;
                                case 5:
                                    Met("Порядок выполнения операторов AND и OR следующий:", "Сначала выполняется AND, а затем OR", "Сначала выполняется OR, а затем AND", "Порядок выполнения операторов AND и OR зависит от того, какой из операторов стоит первым", "Операторы AND и OR выполняются одновременно");
                                    otvet = b1.Name;
                                    break;
                                case 6:
                                    Met("Что покажет следующий запрос?\nselect DISTINCT seller_id order by seller_id from Orders", "Уникальные ID продавцов, отсортированные по возрастанию", "Уникальные ID продавцов, отсортированные по убыванию", "Ничего, запрос составлен неверно, ORDER BY всегда ставится в конце запроса", "Неотсортированные никак уникальные ID продавцов");
                                    otvet = b3.Name;
                                    break;
                                case 7:
                                    Met("Что делает спецсимвол '_' в паре с оператором LIKT:\nselecr * from Orders where customer_name like 'mik_'", "Запрос составлен неверно, в паре с оператором like не используются спецсимволы", "найдет все имена, которые начинаются на mik, вне зависимости от того, из какого количества символов они состоят", "найдет данные, где имя равно mik", "найдет все имена, которые начинаются на mik и состоят из 4 символов");
                                    otvet = b4.Name;
                                    break;
                                case 8:
                                    Met("Выберите корректный пример использования функции CONCAT:", "select concat = index and city from Orders", "select concat IN ('index','city' from Orders", "select concat ('index',' ','city' from Orders", "нет правильного примера");
                                    otvet = b3.Name;
                                    break;
                                case 9:
                                    Met("Есть ли ошибка в запросе?\nselect id, date, customer_name from Orders where customer_name=Mike", "Запрос составлен правильно", "Mike необходимо записать в кавычках 'Mike'", "Нужно убрать лишние поля из запроса", "Строчку с where поменять местами с from");
                                    otvet = b2.Name;
                                    break;
                                case 10:
                                    Met("Выберите правильный пример использования функции округления ROUND", "select id,price * disAdminNumber.AdminNumber.levvv AS total price from Orders ROUND (2)", "select id,price * discount ROUND(2) AS total price from Orders", "нет правильного примера", "select id, ROUND(price * discount, 2) AS total price from Orders");
                                    otvet = b4.Name;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case 3: // Сложный уровень
                            switch (quas)
                            {
                                case 1: // вопросы
                                    Met("Что покажет следующий запрос:\nselect seller_id, count(*) from Orders GROUP BY seller_id HAVING seller_id IN (2,4,6)", "количество заказов, сгруппированное по продавцам 2,4 и 6", "количество продавцов, у которых 2,4 или 6 товаров", "ничего, запрос составлен неверно, HAVING указыватся до группировки", "ничего, запрос составлен неверно, для указания условия должно быть использовано WHERE");
                                    otvet = b1.Name;
                                    break;
                                case 2:
                                    Met("В какиз командах можно использовать LIMIT?", "Только select", "select и insert", "select, update, delete", "select, insert, delete, update");
                                    otvet = b3.Name;
                                    break;
                                case 3:
                                    Met("Выберите корректный пример составленного запроса с использованием JOIN.\nДанный запросвыведет нам данные ID заказа, имя заказчика и продавца:", "select Orders.id, Orders.customer_name, Sellers.id from Orders LEFT JOIN ON Sellers AND Orders.seller_id = Sellers.id", "select id AND customer_name AND seller_id from Orders LEFT JOIN ON seller_id = id", "select Orders.id, Orders.customer_name, Sellers.id from Orders LEFT JOIN Sellers ON Orders.seller_id = Sellers.id", "select Orders.id, Orders.customer_name, Sellers.id from Orders JOIN Sellers WHEN Orders.seller_id = Sellers.id");
                                    otvet = b3.Name;
                                    break;
                                case 4:
                                    Met("Возможно ли использование одновременно двух агрегирующих функций:\nselect min(price), max(price) from Orders", "Да, но данный запрос составлен неверно, надо так: select * from Orders where price IN (min,max)", "Да, в результате мы получим минимальную и максимальную стоимости", "Да, в результате мы получим стоимости, отсортированные от минимальной к максимальной", "Нет, две функции использовать одновременно нельзя");
                                    otvet = b2.Name;
                                    break;
                                case 5:
                                    Met("Выберите правильный пример запроса с использованием UNION:", "select id, city from Orders order by id union select id, city from Sellers order by city", "select id, city, seller_id from Orders and select city, id from Sellers order by id", "Все запросы верные", "select id, city from Orders union select id, city from Sellers order by id");
                                    otvet = b4.Name;
                                    break;
                                case 6:
                                    Met("Какого строкового типа данных нет в SQL?", "VARCHAR", "STRING", "CHAR", "TEXT");
                                    otvet = b2.Name;
                                    break;
                                case 7:
                                    Met("Какие поля из таблицы обязательно перечислять в INSERT для вставки данных?", "Те, у которых нет DEFAULT значения и которые не имеют атрибут auto_increment", "Абсолютно все", "Только те, у которых нет DEFAULT значения", "Все поля имеют негласное DEFAULT значение, обяхательных полей в SQL нет");
                                    otvet = b1.Name;
                                    break;
                                case 8:
                                    Met("Как получить значение текущего года в SQL?", "select now()", "select year()", "select year(now())", "select year from Date");
                                    otvet = b3.Name;
                                    break;
                                case 9:
                                    Met("Как правильно добавить строку в таблицу? Какой запрос верный?", "UODATE INTO 'SimpleTable' SET 'some_text'='my txt'", "INSERT INTO 'SimpleTable' SET 'some_text'='my text'", "SET INTO 'SimpleTable' VALUE 'some_text'='my text'", "INSERT INTO 'SimpleTable' ('some_text') VALUES('my text')");
                                    otvet = b4.Name;
                                    break;
                                case 10:
                                    Met("Как сделать несколько записей в таблицу за один запрос?", "Использовать MULTI INSERT INTO вместо INSERT INTO", "Перечислить через запятую все наборы значений после VALUES", "Использовать подзапрос", "Никак");
                                    otvet = b2.Name;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        default:
                            break;
                    }
                    break;
                case 3: // Java
                    switch (AdminNumber.levv)
                    {
                        case 1:  // Начальный уровень
                            switch (quas)
                            {
                                case 1: // вопросы
                                    Met("Для чего используется оператор NEW?", "Для создания экземпляра класса.", "Для создания новой переменной.", "Это антагонист оператора OLD.", "Для объявления нового класса.");
                                    otvet = b1.Name;
                                    break;
                                case 2:
                                    Met("Что означает перегрузка метода в Java (overload)?", "Изменение поведения метода класса относительно родительского.", "Изменение поведения метода класса относительно дочернего.", "Несколько разных классов с одинаковым методом.", "Несколько методов с одинаковым названием, но разным набором параметров.");
                                    otvet = b4.Name;
                                    break;
                                case 3:
                                    Met("Что означает ключевое слово extends?", "Что это дополнительный модуль класса, который расширяет его свойства.", "Что данный класс наследуется от другого.", "Что это самый большой класс в программе.", "Что два класса делают одно и то же.");
                                    otvet = b2.Name;
                                    break;
                                case 4:
                                    Met("Что означает переопределение метода в Java (override)?", "Несколько разных классов с одинаковым методом.", "Несколько методов с одинаковым названием, но разным набором параметров.", "Изменение поведения метода класса относительно родительского.", "Изменение поведения метода класса относительно дочернего.");
                                    otvet = b3.Name;
                                    break;
                                case 5:
                                    Met("Что вернет метод, объявленный следующим образом:\npublic static int getAmount()", "Вернет целочисленное значение.", "Не ясно, надо смотреть код метода.", "Вернет static-поле класса.", "Вернет ссылку на объект класса this.");
                                    otvet = b1.Name;
                                    break;
                                case 6:
                                    Met("Чем отличаются static-метод класса от обычного метода класса.", "Static-метод класса можно вызывать только внутри класса, а обычный - в любой части кода.", "Обычный метод класса можно перегрузить, а static-метод нельзя.", "Обычный метод класса работает от объекта класса, а static-метод от всего класса.", "Обычный метод класса можно переопределить, а static-метод нельзя.");
                                    otvet = b3.Name;
                                    break;
                                case 7:
                                    java1_7.Visibility = Visibility.Visible;
                                    Met("Каков результат?", "Cat", "Dog", "Ошибка компиляции.", "Исключение во время выполнения.");
                                    otvet = b2.Name;
                                    break;
                                case 8:
                                    java1_7.Visibility = Visibility.Hidden;
                                    Met("Каков результат выполнения следующего кода?\nint[] a = new int[15];", "a[0] равно null", "a[14] не определено", "a[14] равно 0", "a.length равно 14");
                                    otvet = b3.Name;
                                    break;
                                case 9:
                                    Met("Каков тип литерала 5.345?", "float", "double", "Double", "Float");
                                    otvet = b2.Name;
                                    break;
                                case 10:
                                    Met("Можно ли вызвать static-метод внутри обычного?", "Никак, static-метод можно вызвать только от объекта класса.", "Можно, ничего дополнительно делать не надо.", "Можно, надо перед этим перегрузить обычный метод класса.", "Можно, надо перед этим переопределить обычный метод класса.");
                                    otvet = b2.Name;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case 2: // Средний уровень
                            switch (quas)
                            {
                                case 1: // вопросы
                                    java2_1.Visibility = Visibility.Visible;
                                    Met("Что будет в результате выполнения кода?", "Код выведет на консоль X и программа завершится", "Код выведет на консоль XY и завершится практически сразу", "Код выведет на консоль X и никогда не завершится", "Ошибка времени выполнения");
                                    otvet = b4.Name;
                                    break;
                                case 2:
                                    java2_1.Visibility = Visibility.Hidden;
                                    java2_2.Visibility = Visibility.Visible;
                                    Met("Каков результат выполнения кода?", "Ошибка компиляции.", "На консоль выведется 16.", "На консоль выведется 0.", "Ничего не выведется");
                                    otvet = b1.Name;
                                    break;
                                case 3:
                                    java2_2.Visibility = Visibility.Hidden;
                                    Met("Как вызвать обычный метод класса внутри static-метода?", "Можно, ничего дополнительно делать не надо.", "Можно, надо перед этим переопределить обычный метод класса.", "Можно, надо перед этим перегрузить обычный метод класса.", "Никак, static-метод не работает с объектом класса.");
                                    otvet = b4.Name;
                                    break;
                                case 4:
                                    java2_4.Visibility = Visibility.Visible;
                                    Met("Что будет, если скомпилировать и запустить\nэтот код?", "Код отработает успешно", "StackOverflowException", "NullPointerException", "Ошибка во время компиляции");
                                    otvet = b1.Name;
                                    break;
                                case 5:
                                    java2_4.Visibility = Visibility.Hidden;
                                    java2_5.Visibility = Visibility.Visible;
                                    Met("Каким будет вывод программы?", "Она откомпилируется и ничего не выведет на консоль.", "Компилируется, но выбросится исключение во время выполнения.", "Ошибка компиляции.", "Напечатает \"complete\".");
                                    otvet = b3.Name;
                                    break;
                                case 6:
                                    java2_5.Visibility = Visibility.Hidden;
                                    Met("Что будет в результате выполнения операции?\n2 + 2 == 5 && 12 / 4 == 3 || 2 == 5 % 3?", "true", "false", "null", "0");
                                    otvet = b1.Name;
                                    break;
                                case 7:
                                    java2_7.Visibility = Visibility.Visible;
                                    Met("Каков результат выполнения следующего кода?", "Ничего не выведется на консоль.", "Hello world!!!", "Ошибка времени выполнения.", "Ошибка времени выполнения.");
                                    otvet = b1.Name;
                                    break;
                                case 8:
                                    java2_7.Visibility = Visibility.Hidden;
                                    java2_8.Visibility = Visibility.Visible;
                                    Met("Что будет, если скомпилировать и выполнить код:", "Ошибка во время компиляции", "Код выполнится без ошибок, но и в консоль ничего не будет выведено", "NullPointerException", "В консоль будет выведено «Hello!»");
                                    otvet = b4.Name;
                                    break;
                                case 9:
                                    java2_8.Visibility = Visibility.Hidden;
                                    java2_9.Visibility = Visibility.Visible;
                                    Met("Каким будет результат выполнения кода?", "Файл будет перезаписан.", "\"1\" будет добавлена к содержимому файла.", "Ничего не произойдет, т.к. файл уже существует.", "Будет выброшено IOException исключение.");
                                    otvet = b1.Name;
                                    break;
                                case 10:
                                    java2_9.Visibility = Visibility.Hidden;
                                    Met("Какие методы могут получить доступ к private членам класса?", "Только классы доступные в том же пакете.", "Только методы, определенные в том же классе.", "Только экземпляры того же класса.", "Только статические методы того же класса.");
                                    otvet = b2.Name;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case 3:  // Сложный уровень
                            switch (quas)
                            {
                                case 1: // вопросы
                                    java3_1.Visibility = Visibility.Visible;
                                    Met("Каким будет результат выполнения кода?", "One Two Three + Исключение", "Зациклится", "One + Исключение", "Не скомпилируется");
                                    otvet = b1.Name;
                                    break;
                                case 2:
                                    java3_1.Visibility = Visibility.Hidden;
                                    java3_2.Visibility = Visibility.Visible;
                                    Met("Что будет, если скомпилировать и выполнить код:", "Ошибка компиляции в строке 2", "woof burble", "Ошибка компиляции в строке 4", "Ошибка компиляции в строке 3");
                                    otvet = b2.Name;
                                    break;
                                case 3:
                                    java3_2.Visibility = Visibility.Hidden;
                                    java3_3.Visibility = Visibility.Visible;
                                    Met("Каким будет результат выполнения кода?", "many", "a few", "Ошибка компиляции.", "Ошибка времени выполнения.");
                                    otvet = b3.Name;
                                    break;
                                case 4:
                                    java3_3.Visibility = Visibility.Hidden;
                                    Met("Напишите функциональный дескриптор функционального интерфейса BiFunction<T, U, R>.", "(U)  -> R", "(T)  -> R", "(T, U)  -> R", "(U, R) -> T");
                                    otvet = b3.Name;
                                    break;
                                case 5:
                                    Met("Напишите функциональный дескриптор для интерфейса UnaryOperator.", "T -> T -> T", "T -> T", "(T, T) -> T", "T -> void");
                                    otvet = b2.Name;
                                    break;
                                case 6:
                                    Met("Каков результат выполнения следующего кода?\nLocalDate.of(2014, 1, 2).atTime(14, 30, 59, 999999);", "LocalDateTime со значением 2014-01-02 14:30:59:999999", "LocalTime со значением 14:30:59:999999", "LocalDate со значением 2014-01-02", "Ошибка времени выполнения.");
                                    otvet = b1.Name;
                                    break;
                                case 7:
                                    java3_7.Visibility = Visibility.Visible;
                                    Met("Каким будет результат выполнения кода:", "2 1", "2 + Исключение", "2 2", "Не скомпилируется");
                                    otvet = b3.Name;
                                    break;
                                case 8:
                                    java3_7.Visibility = Visibility.Hidden;
                                    java3_8.Visibility = Visibility.Visible;
                                    Met("Каким будет результат выполнения кода?", "Ошибка компиляции из-за множества ошибок", "Ошибка компиляции из-за ошибки в строке 7", "Ошибка компиляции из-за ошибки в строке 6", "Ошибка компиляции из-за ошибки в строке 11");
                                    otvet = b2.Name;
                                    break;
                                case 9:
                                    java3_8.Visibility = Visibility.Hidden;
                                    java3_9.Visibility = Visibility.Visible;
                                    Met("Что произойдет после вызова метода test()?", "Не скомпилируется", "В консоль выведется «Hello!»", "Ничего не будет напечатано", "Исключение в рантайме");
                                    otvet = b3.Name;
                                    break;
                                case 10:
                                    java3_9.Visibility = Visibility.Hidden;
                                    Met("Какое утверждение верно для статического вложенного класса?", "Должна быть ссылка на экземпляр включающего класса, чтобы создать его экземпляр.", "Он должен расширять класс вложения.", "Это переменные и методы должны быть статическими.", "Он не имеет доступа к нестатическим членам окружающего класса.");
                                    otvet = b4.Name;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        default:
                            break;
                    }
                    break;
                case 4: // Python
                    switch (AdminNumber.levv)
                    {
                        case 1:  // Начальный уровень
                            switch (quas)
                            {
                                case 1: // вопросы
                                    Met("Определить тип данных переменной a:\n>>>a = 2", "dict", "int", "tuple", "list");
                                    otvet = b3.Name;
                                    break;
                                case 2:
                                    Met("Чему равно i в 3 строке?\n>>> i = 0\n>>> while 100.-1: i += 1\n>>> print i", "99", "Очень большому числу", "101", "100");
                                    otvet = b2.Name;
                                    break;
                                case 3:
                                    Met("Каков результат выполнения следующего кода:\n>>>2/3", "TypeError: unsupported operand type(s)", "1", "0", "0.6666666666666666");
                                    otvet = b3.Name;
                                    break;
                                case 4:
                                    Met("Каков результат выполнения следующего кода:\n>>> a = b = c = 3\n>>> b = a/3\n>>> print c", "Ошибка! Строка 1", "0", "1", "3");
                                    otvet = b4.Name;
                                    break;
                                case 5:
                                    Met("Что напечатает следующий код?\nfruits = { 'apple', 'banana', 'apple' }\nprint(fruits)", "{'apple','banana'}", "Возникнет синтаксическая ошибка", "{'apple','banana','apple'}", "{'apple','apple','banana'}");
                                    otvet = b1.Name;
                                    break;
                                case 6:
                                    Met("Каков результат выполнения следующего кода:\n>>> a = [-99.9, -53.14, -78.8, -36.7]\n>>> b = -100.2\n>>> print a > b", "false", "true", "False", "True");
                                    otvet = b4.Name;
                                    break;
                                case 7:
                                    Met("Что будет напечатано при исполнении следующего кода?\nprint(type(1 / 2))", "type 'number'", "type 'double'", "type 'int'", "type 'turple'");
                                    otvet = b3.Name;
                                    break;
                                case 8:
                                    Met("Каков результат выполнения следующего кода:\n>>> y = [0, 5, -10, 0, 82, 99]\n>>> print y[2]", "-10", "2", "Ошибка! Нет такого элемента в списке.", "Ошибка! Неправильное обращение к элементы списка.");
                                    otvet = b1.Name;
                                    break;
                                case 9:
                                    Met("Какой результат даст выражение?\nTrue + 4", "5", "Произойдет ошибка", "True", "False");
                                    otvet = b1.Name;
                                    break;
                                case 10:
                                    Met("Каков результат выполнения следующего кода:\n>>> s = 'as'\n>>> print 2*s", "asas", "as2as2", "as2", "2as2as");
                                    otvet = b1.Name;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case 2: // Средний уровень
                            switch (quas)
                            {
                                case 1: // вопросы
                                    python2_1.Visibility = Visibility.Visible;
                                    Met("Каков результат выполнения следующего кода:", "Ошибка! В 4 строке", "[0, 1, 2, 3, 4, 5]", "[0, 1, 2, 3, 4, 5, 6, 7, 8, 0, 1, 2, 3, 4, 5]", "[0, 1, 2, 3, 4, 5, 6, 7, 8]");
                                    otvet = b1.Name;
                                    break;
                                case 2:
                                    python2_1.Visibility = Visibility.Hidden;
                                    python2_2.Visibility = Visibility.Visible;
                                    Met("Каков результат выполнения следующего кода:", "False", "Ошибка в 4 строке!", "True", "Ошибка в 2 строке!");
                                    otvet = b2.Name;
                                    break;
                                case 3:
                                    python2_2.Visibility = Visibility.Hidden;
                                    python2_3.Visibility = Visibility.Visible;
                                    Met("Каким будет результат выполнения кода?", "1", "2", "3", "Возникнет ошибка");
                                    otvet = b2.Name;
                                    break;
                                case 4:
                                    python2_3.Visibility = Visibility.Hidden;
                                    python2_4.Visibility = Visibility.Visible;
                                    Met("Каков результат выполнения следующего кода:", "[1, ['abc', 'xyz'], 3]", "[1, 2, 'abc', 'xyz']", "[1, 2, ['abc', 'xyz']]", "[[1, 2], 'abc', 'xyz']");
                                    otvet = b3.Name;
                                    break;
                                case 5:
                                    python2_4.Visibility = Visibility.Hidden;
                                    python2_5.Visibility = Visibility.Visible;
                                    Met("Каков результат выполнения следующего кода?", "1", "2", "func(0)", "Возникнет ошибка");
                                    otvet = b1.Name;
                                    break;
                                case 6:
                                    python2_5.Visibility = Visibility.Hidden;
                                    python2_6.Visibility = Visibility.Visible;
                                    Met("Каков результат выполнения следующего кода:", "['two', 3, 1]", "['one', 2, 3]", "['one', 'two', 'three']", "[2, 'three', 'one']");
                                    otvet = b4.Name;
                                    break;
                                case 7:
                                    python2_6.Visibility = Visibility.Hidden;
                                    Met("Что выведет следующий фрагмент кода?\nx = 4.5\ny = 2\nprint(x // y)", "2.25", "21", "2", "9");
                                    otvet = b3.Name;
                                    break;
                                case 8:
                                    Met("Каков результат выполнения следующего кода:\n>>> a = 5\n>>> print 12 < a < -12", "true", "false", "True", "False");
                                    otvet = b4.Name;
                                    break;
                                case 9:
                                    Met("Что будет напечатано?\nname = \"snow storm\"\nprint(\"%s\" % name[6:8])", "to", "st", "sto", "Syntax Error");
                                    otvet = b1.Name;
                                    break;
                                case 10:
                                    Met("Каков результат выполнения следующего кода:\n>>> import random\n>>> d = random.random()\n>>> print abs(d) > 1", "True", "true", "False", "false");
                                    otvet = b3.Name;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case 3:  // Сложный уровень
                            switch (quas)
                            {
                                case 1: // вопросы
                                    Met("Что выведет следующий код при его исполнении?\nprint(type(1 / 2))", "class 'float'", "class 'int'", "class 'double'", "class 'tuple'");
                                    otvet = b1.Name;
                                    break;
                                case 2:
                                    Met("Что делает следующий код?\ndef a(b, c, d): pass", "Определяет список и инициализирует его", "Определяет функцию, которая ничего не делает", "Определяет функцию, которая передает параметры", "Определяет пустой класс");
                                    otvet = b2.Name;
                                    break;
                                case 3:
                                    python3_3.Visibility = Visibility.Visible;
                                    Met("Каков результат выполнения следующего кода:", "Polly is 12 years old. Or 12.2?", "Ошибка! Строка 5", "Polly is 12.2 years old. Or 12?", "Ошибка! Строка 2");
                                    otvet = b2.Name;
                                    break;
                                case 4:
                                    python3_3.Visibility = Visibility.Hidden;
                                    Met("Как получить порядок базовых классов, в котором будет производиться поиск нужного метода во\nвремя исполнения программы?", "Это невозможно", "cls.get_bases()", "cls.__mro__", "cls.__bro__");
                                    otvet = b3.Name;
                                    break;
                                case 5:
                                    python3_5.Visibility = Visibility.Visible;
                                    Met("Что интерпретатор выведет на месте знаков вопроса?", "False, True", "False, False", "True, False", "True, True");
                                    otvet = b1.Name;
                                    break;
                                case 6:
                                    python3_5.Visibility = Visibility.Hidden;
                                    Met("Что выведет следующая программа?\na = [1,2,3,None,(),[],]\nprint(len(a))", "Syntax Error", "4", "5", "6");
                                    otvet = b4.Name;
                                    break;
                                case 7:
                                    python3_7.Visibility = Visibility.Visible;
                                    Met("Каким будет результат выполнения данной программы?", "45", "42", "102", "Ошибка интерпретатора");
                                    otvet = b3.Name;
                                    break;
                                case 8:
                                    python3_7.Visibility = Visibility.Hidden;
                                    python3_8.Visibility = Visibility.Visible;
                                    Met("Каков результат выполнения следующего кода:", "-7", "-5", "9", "10");
                                    otvet = b4.Name;
                                    break;
                                case 9:
                                    python3_8.Visibility = Visibility.Hidden;
                                    Met("Чему будет равно а?\na = [1,2,3]\na[-3:-1] = 10,20,30,40", "[10,20,30,40,3]", "TypeError", "IndexError", "[10,20,30,40,2,3]");
                                    otvet = b1.Name;
                                    break;
                                case 10:
                                    Met("Какая из функций вернет итерируемый объект?", "len()", "xrange()", "range()", "ord()");
                                    otvet = b3.Name;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        default:
                            break;
                    }
                    break;
                case 5: // C++
                    switch (AdminNumber.levv)
                    {
                        case 1:  // Начальный уровень 
                            switch (quas)
                            {
                                case 1: // вопросы
                                    Met("Какой из вариантов записи подключения стандартной библиотеки является правильным с точки\nзрения стандарта C++?", "#include 'iostream'", "#include <iostream>", "Оба варианта неправильные", "Оба варианта правильные");
                                    otvet = b2.Name;
                                    break;
                                case 2:
                                    Met("Какой из перечисленных модификаторов является модификатором разрема?", "long", "signed", "unsigned", "Ни один из перечисленных не является модификатором размера");
                                    otvet = b1.Name;
                                    break;
                                case 3:
                                    Met("Выберите недопустимый идентификатор переменной из нижеуказанного.", "Int", "bool", "DOUBLE", "0");
                                    otvet = b2.Name;
                                    break;
                                case 4:
                                    Met("Какая из записей является ошибочной?", "Все варианты правильные", "Все варианты неправильные", "double a2 {2.3}", "int d2 = {2.3}");
                                    otvet = b4.Name;
                                    break;
                                case 5:
                                    cplus1_5.Visibility = Visibility.Visible;
                                    Met("Какой результат работы следующей программы?", "10", "'мусор", "Ошибка компиляции", "Ошибка выполнения");
                                    otvet = b3.Name;
                                    break;
                                case 6:
                                    cplus1_5.Visibility = Visibility.Hidden;
                                    Met("Какой из вариантов записи спецификатора auto является ошибочным?", "auto b; b = 14", "const auto a = 'Hello'", "auto a = -58", "auto = 0");
                                    otvet = b1.Name;
                                    break;
                                case 7:
                                    Met("Кто является автором языка программирования C++?", "Charles Babbage", "Dennis Ritchie", "Brain Kernighan", "Bjarne Stroustrup");
                                    otvet = b4.Name;
                                    break;
                                case 8:
                                    Met("Какой из перечисленных типов данных не является фундаментальным?", "double", "void", "string", "bool");
                                    otvet = b3.Name;
                                    break;
                                case 9:
                                    cplus1_9.Visibility = Visibility.Visible;
                                    Met("Сколько раз выполнится цикл?", "Один", "Два", "Цикл бесконечный", "Не выполнится, в коде есть ошибка");
                                    otvet = b1.Name;
                                    break;
                                case 10:
                                    cplus1_9.Visibility = Visibility.Hidden;
                                    Met("Как нужно объявлять константу?", "#define x = 10", "int = 10", "const int = 10", "const 5");
                                    otvet = b3.Name;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case 2: // Средний уровень
                            switch (quas)
                            {
                                case 1: // вопросы
                                    cplus2_1.Visibility = Visibility.Visible;
                                    Met("Сколько раз вызовется любая move операция класса x?", "Хотя бы раз", "Хотя бы два раза", "Нисколько", "Хотя бы три раза");
                                    otvet = b1.Name;
                                    break;
                                case 2:
                                    cplus2_1.Visibility = Visibility.Hidden;
                                    Met("Что такое рефакторинг?", "Процесс изменения внутренней структуры программы, не затрагивающий ее внешнего поведения", "Тоже самое, что и компиляция", "Удаление неиспользуемых классов, функций и переменных", "Сборка мусора");
                                    otvet = b1.Name;
                                    break;
                                case 3:
                                    Met("Что такое enum?", "Перечисление", "Целочисленный тип данных", "Такое не используется в C++", "Команда");
                                    otvet = b2.Name;
                                    break;
                                case 4:
                                    Met("Для чего нужен оператор continue в цикле?", "Позволяет сразу перейти в начало тела цикла", "Позволяет продолжить работу после применения оператора return", "Позволяет продолжить работу программы в случае возникновения ошибки или исключения", "Позволяет сразу перейти в конец тела цикла, пропуская весь код, который находится под ним");
                                    otvet = b4.Name;
                                    break;
                                case 5:
                                    Met("Что такое абстрактный метод?", "Метод класса, в котором присутствуют абстрактные поля", "Метод абстрактного класса", "Метод класса, в котором присутствует инкапсуляция", "Метод класса, реализация для которого отсутствует");
                                    otvet = b4.Name;
                                    break;
                                case 6:
                                    Met("Что нужно подключить для работы с файлами?", "Заголовочный файл fstream", "Библиотеку file", "Ничего, работа с файлами есть в стандартной библиотеке", "Библиотеку IO");
                                    otvet = b1.Name;
                                    break;
                                case 7:
                                    Met("Как еще называют логическое отрицание?", "Инверсия", "Дизъюнкция", "Импликация", "Инкрементация");
                                    otvet = b1.Name;
                                    break;
                                case 8:
                                    Met("Что такое flush?", "Чтение файла", "Освобождение буфера", "Запись в файл", "Заполнение буфера");
                                    otvet = b2.Name;
                                    break;
                                case 9:
                                    Met("Что означает ключевое слово override?", "Свойство, которое может принимать несколько значений", "Класс, который наследуется от абстрактного", "Виртуальный метод, который переопределяет виртуальный метод базового класса", "Тип данных");
                                    otvet = b3.Name;
                                    break;
                                case 10:
                                    Met("Как объявить список значений типа под названием num?", "list&lt;T&gt; num", "collection&lt;int&gt; num", "collection&lt;int&gt; num", "list&lt;int&gt; num");
                                    otvet = b4.Name;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case 3:  // Сложный уровень
                            switch (quas)
                            {
                                case 1: // вопросы
                                    cplus3_1.Visibility = Visibility.Visible;
                                    Met("Что выведет программа?", "d", "di", "i", "Не скомпилируется");
                                    otvet = b1.Name;
                                    break;
                                case 2:
                                    cplus3_2.Visibility = Visibility.Visible;
                                    cplus3_1.Visibility = Visibility.Hidden;
                                    Met("Какой контейнер типа Container нельзя передавать в\nфункцию EraseIf?", "std::unordered_set", "std::map", "std::list", "std::vector");
                                    otvet = b4.Name;
                                    break;
                                case 3:
                                    cplus3_3.Visibility = Visibility.Visible;
                                    cplus3_2.Visibility = Visibility.Hidden;
                                    Met("Что напечатает программа?", "Не скомпилируется", "0", "1", "Implementation defined");
                                    otvet = b3.Name;
                                    break;
                                case 4:
                                    cplus3_4.Visibility = Visibility.Visible;
                                    cplus3_3.Visibility = Visibility.Hidden;
                                    Met("Что выведет программа?", "021", "210", "201", "012");
                                    otvet = b2.Name;
                                    break;
                                case 5:
                                    cplus3_5.Visibility = Visibility.Visible;
                                    cplus3_4.Visibility = Visibility.Hidden;
                                    Met("Что выведет программа?", "Не скомпилируется", "0", "1", "Неопределенное поведение");
                                    otvet = b3.Name;
                                    break;
                                case 6:
                                    cplus3_6.Visibility = Visibility.Visible;
                                    cplus3_5.Visibility = Visibility.Hidden;
                                    Met("Укажите performance проблему с эти кодом из представленных\nвариантов.", "Размер вектора будет расти во время цикла, стоит сделать .reserve перед циклом", "Код оптимален, простых оптимизаций нет", "push_back сделает копию возвращаемых значений, надо использовать emplace_back", "Код содержит неопределенное поведение, т.к. ConvertData генерирует временный объект");
                                    otvet = b1.Name;
                                    break;
                                case 7:
                                    cplus3_7.Visibility = Visibility.Visible;
                                    cplus3_6.Visibility = Visibility.Hidden;
                                    Met("Код скомпилируется?", "Нет, вызывать std::move в capture нельзя", "Да, переменные в lambda неконстантные, поэтому их можно менять", "Да, так как только константные переменные могут изменяться, if contexpr не разрешит", "Нет, переменные в lambda иммутабельные, но decltype вернет, что тип мутабельный");
                                    otvet = b4.Name;
                                    break;
                                case 8:
                                    cplus3_8.Visibility = Visibility.Visible;
                                    cplus3_7.Visibility = Visibility.Hidden;
                                    Met("Какой контейнер типа Container нельзя передавать в функцию\nEraself?", "std::list", "std::vector", "std::map", "Все варианты верные");
                                    otvet = b4.Name;
                                    break;
                                case 9:
                                    cplus3_9.Visibility = Visibility.Visible;
                                    cplus3_8.Visibility = Visibility.Hidden;
                                    Met("Что выведет программа?", "A1", "B1", "A2", "B2");
                                    otvet = b2.Name;
                                    break;
                                case 10:
                                    cplus3_9.Visibility = Visibility.Hidden;
                                    Met("Что такое конъюнкция?", "Логическое сложение", "Логическое умножение", "Функция для обработки строк", "Вычитание строк");
                                    otvet = b2.Name;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        default:
                            break;
                    }
                    break;
                case 6: // C#
                    switch (AdminNumber.levv)
                    {
                        case 1:  // Начальный уровень
                            switch (quas)
                            {
                                case 1: // вопросы
                                    Met("Где верно происходит вывод данных в консоль?", "print(\"Hi\");", "console.log(\"Hi\");", "Console.write(\"Hi\");", "Console.WriteLine(\"Hi\");");
                                    otvet = b4.Name;
                                    break;
                                case 2:
                                    Met("В чем отличие между break и continue?", "Continue пропускает итерацию, break выходит из цикла", "Continue работает только в циклах, break дополнительно в методах", "Нет отличий", "Break используется в Switch case, а continue в циклах");
                                    otvet = b2.Name;
                                    break;
                                case 3:
                                    Met("Какие типы переменных существуют?", "int, char, bool, double, uint, short", "Все перечисленные", "Ни один из них", "int, char, bool, float, double");
                                    otvet = b2.Name;
                                    break;
                                case 4:
                                    Met("Какие циклы существуют в языке C#?", "for, while, do while, foreach", "for, while", "for, while, foreach", "for, while, do while");
                                    otvet = b1.Name;
                                    break;
                                case 5:
                                    Met("Где правильно создан массив?", "int arr = [2,5];", "int arr = {2,5};", "int[] arr = new Array [2,5];", "int[] arr = new int [2]{2,5};");
                                    otvet = b4.Name;
                                    break;
                                case 6:
                                    Met("Что такое перегрузка методов?", "Использование одного имени для разных методов", "Передача слишком большого файла через return", "Передача слишком больших данных в функцию", "Написание слишком большого кода в методе");
                                    otvet = b1.Name;
                                    break;
                                case 7:
                                    Met("Какая функция корректно сравнивает две подстроки?", "String.Check(\"hi\",\"hello\");", "String.Match(\"hi\",\"hello\");", "String.Compare(\"hi\",\"hello\");", "String.Equal(\"hi\",\"hello\");");
                                    otvet = b3.Name;
                                    break;
                                case 8:
                                    Met("Где правильно создана переменная?", "float big_num = 23.2234;", "char symbol = 'A';", "int num = \"1\";", "$x = 10;");
                                    otvet = b2.Name;
                                    break;
                                case 9:
                                    Met("Что делает try-catch?", "Работает с исключениями", "Работает с файлами", "Работает с классами", "Работает с базой данных");
                                    otvet = b1.Name;
                                    break;
                                case 10:
                                    Met("Для чего можно использовать язык C#?", "Для написания игр", "Все перечисленное", "Для создания веб сайтов", "Для создания программ под ПК");
                                    otvet = b2.Name;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case 2: // Средний уровень
                            switch (quas)
                            {
                                case 1: // вопросы
                                    Met("Какой из операторов передает управление следующей итерации цикла?", "stop", "continue", "break", "await");
                                    otvet = b2.Name;
                                    break;
                                case 2:
                                    Met("В чем разница между перегрузкой(overloading) и переопределением(overriding)?", "Ее не существует.Оба понятия описывают одно и то же явление.", "Переопределение означает наличие двух или более методов в одном классе с тем же именем, но с разными аргументами.", "Перегрузка означает наличие двух методов с теми же аргументами, но с различными реализациями. ", "Ни один из предложенных вариантов не является верным.");
                                    otvet = b4.Name;
                                    break;
                                case 3:
                                    Met("Какой из перечисленных типов не является значимым?", "int", "enum", "class", "float");
                                    otvet = b3.Name;
                                    break;
                                case 4:
                                    Met("Какое утверждение неверно?", "В каждом свойстве должны быть get и set блоки.", "В свойства мы можем вложить дополнительную логику.", "Свойства позволяют управлять доступом к переменной.", "Благодаря свойствам реализуется один из принципов ООП – инкапсуляция.");
                                    otvet = b1.Name;
                                    break;
                                case 5:
                                    Met("ООП поддерживает два вида связывания объектов с кодом методов.\nСоответствующие методы называются:", "Виртуальными и абстрактными", "Статическими и виртуальными", "Абстрактными и статическими", "Публичными и приватными");
                                    otvet = b2.Name;
                                    break;
                                case 6:
                                    Met("С чем нельзя использовать модификатор abstract?", "Со свойствами.", "С индексаторами.", "С событиями.", "Со структурами.");
                                    otvet = b4.Name;
                                    break;
                                case 7:
                                    Met("В каком из приведенных вариантов правильно описано объявление типа перечисления?", "enum Day {Sat, Sun, Mon, Tue, Wed, Thu, Fri}", "list Day { Sat, Sun, Mon, Tue, Wed, Thu, Fri }", "enum { Sat, Sun, Mon, Tue, Wed, Thu, Fri }", "{ Sat, Sun, Mon, Tue, Wed, Thu, Fri } as Enum");
                                    otvet = b1.Name;
                                    break;
                                case 8:
                                    Met("Какое утверждение верно?", "Если структура или объект класса присваивается новому объекту, то в новую переменную сохраняется копия всех данных.", "Структура является размерным типом, а класс – ссылочный.", "В структуре нельзя определять конструктор.", "От структуры так же, как и от класса, можно наследоваться.");
                                    otvet = b2.Name;
                                    break;
                                case 9:
                                    Met("Пусть объявлен кортеж: var tuple = (5, 10). Как обратиться к значению 5?", "tuple[0]", "tuple.first", "touple.5", "tuple.Item1");
                                    otvet = b4.Name;
                                    break;
                                case 10:
                                    Met("Какое из утверждений верно?", "У интерфейсов можно объявлять свойства с помощью ключевого слова property.", "У интерфейсов можно объявлять свойства так же, как и у классов.", "У интерфейсов можно объявлять свойства, но без тела у методов доступа свойства.", "У интерфейсов нельзя объявлять свойства.");
                                    otvet = b3.Name;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case 3:  // Сложный уровень
                            switch (quas)
                            {
                                case 1: // вопросы
                                    Met("Унифицированная коллекция здается при помощи ключевого слова:", "HashSet<T>", "Lis<T>", "Dictionary<K,T>", "ArrayList");
                                    otvet = b2.Name;
                                    break;
                                case 2:
                                    Met("Можно ли перехватить добавление и удаление делегата из события? Если да, то как?", "Это невозможно", "Можно.Для этого нужно использовать ключевые слова get и set", "Можно. Для этого нужно использовать ключевые слова add и remove", "Можно.Для этого нужно переопределить операторы - и + для делегата");
                                    otvet = b3.Name;
                                    break;
                                case 3:
                                    Met("Какие из перечисленных ниже типов являются ссылочными?\n1. string\n2. delegate\n3. uint\n4. enum\n5. struct", "1 и 2 варианты", "2 и 4 варианты", "1, 3 и 5 варианты", "1, 2, 4 и 5 варианты");
                                    otvet = b1.Name;
                                    break;
                                case 4:
                                    Met("С помощью чего можно создать многопоточное приложение в C#?\n1. Класс ThreadPool\n2. Класс Collection\n3. Action-класс с лямбда-функциями\n4. Метод BeginInvoke()\n5. Метод Join()", "1, 2 и 3 варианты", "1, 3 и 4 варианты", "1 и 5 варианты", "1 и 4 варианты");
                                    otvet = b4.Name;
                                    break;
                                case 5:
                                    Met("Какой уровень изоляции устанавливает разделяемые блокировки на все считываемые данные и\nудерживает их до подтверждения или отмены транзакции?", "READ UNCOMMITTED", "READ COMMITTED", "REPEATABLE READ", "SERIALIZABLE");
                                    otvet = b3.Name;
                                    break;
                                case 6:
                                    Met("Какие операции являются атомарными в C#?\n1. Операции инкремента/декремента\n2. Операции чтения/записи в 32-битные типы вроде bool, char, byte, sbyte, short\n3. Операции ссылочного присваивания\n4. Операции создания потоков, а также их синхронизации и блокировки", "1 и 3 варианты", "2 и 3 варианты", "2 и 4 варианты", "3 и 4 варианты");
                                    otvet = b2.Name;
                                    break;
                                case 7:
                                    Met("Что будет результатом выполнения этого фрагмента кода?\nusing System; using System.Threading; public class Program { public static void Main() {\nobject sync = new object(); var thread = new Thread(()=> { try { Work(); } finally { lock (sync) {\nMonitor.PulseAll(sync); } } }); thread.Start(); lock (sync) { Monitor.Wait(sync); }\nConsole.WriteLine(\"test\"); } private static void Work() { Thread.Sleep(1000); } }", "Будет выброшено исключение SynchronizationLockException", "В результате взаимоблокировки (deadlock) test не будет выведено", "Одно из двух: либо будет выведено test, либо произойдёт взаимоблокировка", "Всегда будет выведено test");
                                    otvet = b4.Name;
                                    break;
                                case 8:
                                    Met("Какой уровень изоляции устанавливает блокировку на всю область данных, считываемых\nсоответствующей транзакцией?", "SERIALIZABLE", "SNAPSHOT", "READ COMMITTED", "READ UNCOMMITTED");
                                    otvet = b1.Name;
                                    break;
                                case 9:
                                    Met("Что поможет оптимизировать SQL-запросы, если нужно ссылаться на две колонки в условии\nвыражения WHERE?", "Композитные индексы", "Распределённая/кластерная конфигурация", "Частое повторение одинаковых запросов", "Отсутствие индексов");
                                    otvet = b1.Name;
                                    break;
                                case 10:
                                    Met("Что из нижеперечисленного не может иметь модификатор virtual?", "Свойства.", "Индексаторы.", "Поля.", "Методы.");
                                    otvet = b3.Name;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        default:
                            break;
                    }
                    break;
                case 7: // Java Script
                    switch (AdminNumber.levv)
                    {
                        case 1:  // Начальный уровень
                            switch (quas)
                            {
                                case 1: // вопросы
                                    Met("Как правильно объявить массив?", "var colors = \"red\", \"green\", \"blue\"", "var colors = 1 = (\"red\"), 2 = (\"green\"), 3 = (\"blue\")", "var colors = (1:\"red\", 2:\"green\", 3:\"blue\"", "var colors = [\"red\", \"green\", \"blue\"]");
                                    otvet = b4.Name;
                                    break;
                                case 2:
                                    Met("Какой из вариантов откроет новое окно?", "w2 = window.new(\"http://proghub.ru\");", "w2 = window.open(\"http://proghub.ru\")", "w2 = window.create(\"http://proghub.ru\")", "w2 = window.get(\"http://proghub.ru\")");
                                    otvet = b2.Name;
                                    break;
                                case 3:
                                    Met("JavaScript регистрозависимый язык?", "Да", "Нет", "Данное свойство можно изменить в настройках", "Зависит от среды разработки");
                                    otvet = b1.Name;
                                    break;
                                case 4:
                                    js1_4.Visibility = Visibility.Visible;
                                    Met("Что будет выведено в консоль?", "0", "1", "Error", "Ничего");
                                    otvet = b2.Name;
                                    break;
                                case 5:
                                    js1_4.Visibility = Visibility.Hidden;
                                    Met("Что такое ajax?", "Библиотека для асинхронной работы с данными", "Библиотека для асинхронной работы с DOM-элементами", "Группа связанных технологий, используемых для асинхронного отображения данных", "Библиотека для работы с потоками");
                                    otvet = b3.Name;
                                    break;
                                case 6:
                                    Met("Как объявить функцию в JS?", "def fn(){}", "set fn = function(){}", "public static function(){}", "function fn(){}");
                                    otvet = b4.Name;
                                    break;
                                case 7:
                                    Met("Как написать if в JavaScript?", "if i = 5 then", "if i = 5", "if (i == 5)", "if i == 5 then");
                                    otvet = b3.Name;
                                    break;
                                case 8:
                                    Met("Как правильно объявить цикл while?", "while (i <= 10)", "while (i <= 10; i++)", "while i = 1 to 10", "while i == 1 then");
                                    otvet = b1.Name;
                                    break;
                                case 9:
                                    js1_9.Visibility = Visibility.Visible;
                                    Met("Что выведет в консоль?", "Ничего не выведет, возникнет ошибка", "false", "true", "undefined");
                                    otvet = b2.Name;
                                    break;
                                case 10:
                                    js1_9.Visibility = Visibility.Hidden;
                                    Met("Что выведет следующий код?\nconsole.log(employeeId);\nvar employeeId = '19000';", "undefined", "some value", "TypeError", "ReferenceError: employeeId is not defined");
                                    otvet = b1.Name;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case 2: // Средний уровень
                            switch (quas)
                            {
                                case 1: // вопросы
                                    Met("Как сгенерировать случайное целое число от 1 до 10?", "Math.random(10)+1;", "Math.random() * 10+1;", "Math.floor(Math.random(10)+1);", "Math.floor((Math.random() * 10) + 1;");
                                    otvet = b4.Name;
                                    break;
                                case 2:
                                    js2_2.Visibility = Visibility.Visible;
                                    Met("Результатов выполнения следующего кода будет:", "31337", "ReferenceError", "TypeError", "SyntaxError");
                                    otvet = b3.Name;
                                    break;
                                case 3:
                                    js2_2.Visibility = Visibility.Hidden;
                                    js2_3.Visibility = Visibility.Visible;
                                    Met("Что выведет на экран следующий код?", "2", "3", "obj", "Произойдет ошибка");
                                    otvet = b1.Name;
                                    break;
                                case 4:
                                    js2_4.Visibility = Visibility.Visible;
                                    js2_3.Visibility = Visibility.Hidden;
                                    Met("Что выведет этот код?", "false false", "boolean false", "true false", "boolean true");
                                    otvet = b2.Name;
                                    break;
                                case 5:
                                    js2_5.Visibility = Visibility.Visible;
                                    js2_4.Visibility = Visibility.Hidden;
                                    Met("Что выведет код?", "{1, 2, 3, 4}", "{1, 1, 2, 3, 4}", "[1, 2, 3, 4]", "[1, 1, 2, 3, 4]");
                                    otvet = b1.Name;
                                    break;
                                case 6:
                                    js2_6.Visibility = Visibility.Visible;
                                    js2_5.Visibility = Visibility.Hidden;
                                    Met("Дан код. Какое значение будет выведено в консоли?", "true", "false", "1", "0");
                                    otvet = b4.Name;
                                    break;
                                case 7:
                                    js2_7.Visibility = Visibility.Visible;
                                    js2_6.Visibility = Visibility.Hidden;
                                    Met("Что выведет этот код?", "false true true false", "false false true false", "false true true true", "true true true false");
                                    otvet = b3.Name;
                                    break;
                                case 8:
                                    js2_8.Visibility = Visibility.Visible;
                                    js2_7.Visibility = Visibility.Hidden;
                                    Met("Что выведет следующий код?", "1", "5", "undefined", "null");
                                    otvet = b3.Name;
                                    break;
                                case 9:
                                    js2_8.Visibility = Visibility.Hidden;
                                    js2_9.Visibility = Visibility.Visible;
                                    Met("Каким будет результат выполнения данного кода?", "[\"mytest\"]", "[\"myteststring\"]", "[\"m,y,t,e,s,t,s,t,i,n,g\"]", "[\"m,y,t,e,s,t\"]");
                                    otvet = b2.Name;
                                    break;
                                case 10:
                                    js2_9.Visibility = Visibility.Hidden;
                                    Met("Что выведет Alert?\nalert( 0 / 0 );", "NaN", "Infinity", "Undefined", "Ничего");
                                    otvet = b1.Name;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case 3:  // Сложный уровень
                            switch (quas)
                            {
                                case 1: // вопросы
                                    Met("Какое значение будет на выходе?\nconsole.loog('${(x => x)('I love')} to program')", "I love to program", "undefined to program", "${(x => x)('I love') to program}", "TypeError");
                                    otvet = b1.Name;
                                    break;
                                case 2:
                                    js3_2.Visibility = Visibility.Visible;
                                    Met("Что выведет код?", "null \"\" true", "{} \"\" []", "null null true", "false null []");
                                    otvet = b2.Name;
                                    break;
                                case 3:
                                    js3_3.Visibility = Visibility.Visible;
                                    js3_2.Visibility = Visibility.Hidden;
                                    Met("Какие методы вернут значение Hello world! ?", "myMap.get('greeting')", "myMap.get(() => 'greeting')", "myMap.get(myFinc)", "Нет верного ответа");
                                    otvet = b3.Name;
                                    break;
                                case 4:
                                    js3_4.Visibility = Visibility.Visible;
                                    js3_3.Visibility = Visibility.Hidden;
                                    Met("Что выведет код?", "Прости, ты слишком молод для этого :(", "А ты достаточно стар для этого :)", "ReferenceError", "undefined");
                                    otvet = b3.Name;
                                    break;
                                case 5:
                                    js3_4.Visibility = Visibility.Hidden;
                                    Met("Что возвращает метод setInterval?\nset interval(() => console.log(\"Hi\"), 1000);", "Уникальный id", "Указанное количество миллисекунд", "Переданную функцию", "undefined");
                                    otvet = b1.Name;
                                    break;
                                case 6:
                                    js3_6.Visibility = Visibility.Visible;
                                    Met("Что выведет код?", "true", "false", "undefined", "TypeError");
                                    otvet = b4.Name;
                                    break;
                                case 7:
                                    js3_7.Visibility = Visibility.Visible;
                                    js3_6.Visibility = Visibility.Hidden;
                                    Met("Что выведет код?", "undefined", "{ name: \"taras\", age: 26 }", "Error", "taras, 26");
                                    otvet = b1.Name;
                                    break;
                                case 8:
                                    js3_8.Visibility = Visibility.Visible;
                                    js3_7.Visibility = Visibility.Hidden;
                                    Met("Что выведет код?", "{ constructor: ...} { constructor: ...}", "{ constructor: ...} undefined", "{} { constructor: ...}", "{ constructor: ...} {}");
                                    otvet = b2.Name;
                                    break;
                                case 9:
                                    js3_9.Visibility = Visibility.Visible;
                                    js3_8.Visibility = Visibility.Hidden;
                                    Met("Какой будет вывод?", "\"Ihor\"", "\"myName\"", "undefined", "ReferenceError");
                                    otvet = b4.Name;
                                    break;
                                case 10:
                                    js3_9.Visibility = Visibility.Hidden;
                                    Met("Чему равно значение?\nPromise.resolve(5)", "5", "Promise {<pending>: 5}", "Promise {<fullfilled>: 5}", "Error");
                                    otvet = b3.Name;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        default:
                            break;
                    }
                    break;
                case 8: // HTML
                    switch (AdminNumber.levv)
                    {
                        case 1:  // Начальный уровень
                            switch (quas)
                            {
                                case 1: // вопросы
                                    Met("Какой атрибут тега <ol> начинает нумерацию списка с определенного значения?", "type", "number", "start", "begin");
                                    otvet = b3.Name;
                                    break;
                                case 2:
                                    Met("Какой HTML-тэг используется для отображения на странице программного кода?", "programm", "script", "program", "code");
                                    otvet = b4.Name;
                                    break;
                                case 3:
                                    Met("Что означает тег <marquee>?", "Бегущая строка", "Маркированный текст", "Параграф", "Изображение-ссылка");
                                    otvet = b1.Name;
                                    break;
                                case 4:
                                    Met("Из предложенных вариантов указания заголовка в таблице, выберите правильный.", "<table><caption>title</caption>...</table>", "<table caption=\"titile\">...</table>", "<table title=\"title\">...</table>", "<table lable=\"title\">...</table>");
                                    otvet = b1.Name;
                                    break;
                                case 5:
                                    Met("Выберите html-код, создающий checkbox.", "<checkbox/>", "<input type=\"checkbox\"/>", "<check/>", "<input type=\"check\"/>");
                                    otvet = b2.Name;
                                    break;
                                case 6:
                                    Met("Какого значения не может быть у параметра align в теге <img>?", "bottom", "right", "left", "center");
                                    otvet = b4.Name;
                                    break;
                                case 7:
                                    Met("Выберите фрагмент HTML-кода, создающий ссылку со всплывающей подсказкой.", "<a title='подсказка'>текст ссылки</a>", "<a help='подсказка'>текст ссылки</a>", "<a baloon='подсказка'>текст ссылки</a>", "<a tip='подсказка'>текст ссылки</a>");
                                    otvet = b3.Name;
                                    break;
                                case 8:
                                    Met("Что определяет атрибут BORDER у элемента разметки TABLE?", "Определяет толщину рамки", "Определяет расположение таблицы в документе", "Управляет линиями, разделяющими ячейки таблицы", "Устанавливает цвет окантовки");
                                    otvet = b1.Name;
                                    break;
                                case 9:
                                    html1_9.Visibility = Visibility.Visible;
                                    Met("Является ли следующий HTML код валидным\nи well-formed?", "Да", "Только валидным", "Только well-formed", "Код не является валидным и well-formed");
                                    otvet = b3.Name;
                                    break;
                                case 10:
                                    html1_9.Visibility = Visibility.Hidden;
                                    Met("Какой тег предназначен для заголовков наименьшего размера?", "<h1>", "<h6>", "<hmin>", "<h7>");
                                    otvet = b2.Name;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case 2: // Средний уровень
                            switch (quas)
                            {
                                case 1: // вопросы..
                                    Met("С помощью какого тега следует разделять абзацы?", "<p>", "<br>", "<span>", "<b>");
                                    otvet = b1.Name;
                                    break;
                                case 2:
                                    Met("Какое число заголовков первого уровня считается допустимым?", "3", "2", "1", "Неограниченное количество");
                                    otvet = b3.Name;
                                    break;
                                case 3:
                                    Met("С помощью какого атрибута задается ширина поля textarea?", "width", "cols", "size", "rows");
                                    otvet = b2.Name;
                                    break;
                                case 4:
                                    Met("Каким является следующий адрес ссылки:\n/page2.html", "Полным", "Абсолютным", "Неполным", "Относительным");
                                    otvet = b4.Name;
                                    break;
                                case 5:
                                    Met("Как выделить текст курсивом?", "<hr>курсив</hr>", "<em>курсив</em>", "<br>курсив</br>", "<p>курсив</p>");
                                    otvet = b1.Name;
                                    break;
                                case 6:
                                    Met("С помощью какого атрибута можно задать текст для картинки, который будет отображен, если ее\nне удастся загрузить?", "title", "alt", "popup", "caption");
                                    otvet = b2.Name;
                                    break;
                                case 7:
                                    Met("С помощью какого свойства можно сделать отступы внутри ячейки в таблице?", "margin", "case", "space", "padding");
                                    otvet = b4.Name;
                                    break;
                                case 8:
                                    Met("Как сделать текст жирным?", "<strong>жирный</strong>", "<p>жирный</p>", "<a>жирный</a>", "<br>жирный</br>");
                                    otvet = b1.Name;
                                    break;
                                case 9:
                                    Met("Какой тег при создании страницы является необязательным?", "doctype", "head", "strong", "body");
                                    otvet = b3.Name;
                                    break;
                                case 10:
                                    Met("Каких тегов в HTML не существует?", "Парных", "Все есть", "Одиночных", "Тройных");
                                    otvet = b4.Name;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case 3:  // Сложный уровень
                            switch (quas)
                            {
                                case 1: // вопросы
                                    Met("Выберите теги для работы с таблицами.", "<table><tr><td>", "<table><head><tfoot>", "<table><tr><tt>", "<thead><body><tr>");
                                    otvet = b1.Name;
                                    break;
                                case 2:
                                    Met("Выберите тег для создания ненумерованного списка.", "<dl>", "<list>", "<ol>", "<ul>");
                                    otvet = b4.Name;
                                    break;
                                case 3:
                                    Met("Выберите верный способ создания выпадающего списка.", "<select>", "<list>", "<input type=\"dropdown\">", "<input type=\"list\">");
                                    otvet = b1.Name;
                                    break;
                                case 4:
                                    Met("Какое значение по умолчанию имеет атрибут method у лега <form>?", "POST", "Атрибут methodn является обязательным и не имеет значения по умолчанию", "GET", "Значение по умолчани зависит от браузера");
                                    otvet = b3.Name;
                                    break;
                                case 5:
                                    Met("Необходимо защитить текстовое поле формы от изменения значения пользователем.\nКакое из представленных фрагментов кода позволят решить поставленную задачу?", "<imput value=\"$999\" checked/>", "<input value=\"$999\" size=\"0\"/>", "<input value=\"#999\" readonly/>", "<input value=\"$999\" maxlength=\"0\"/>");
                                    otvet = b3.Name;
                                    break;
                                case 6:
                                    Met("Выберите верный способ создания текстового поля для ввода информации.", "<input type=\"textfield\">", "<textinput type=\"text\">", "<textfield>", "<input type=\"text\"");
                                    otvet = b4.Name;
                                    break;
                                case 7:
                                    Met("Выберите верный способ создания многострочного текстового поля.", "<input type=\"textbox\">", "<textarea>", "<input type=\"textarea\"", "<textarea=\"textbox\">");
                                    otvet = b2.Name;
                                    break;
                                case 8:
                                    Met("Выберите верный способ создания чекбокса.", "<checkbox>", "<input type=\"checkbox\">", "<input type=\"check\">", "<check>");
                                    otvet = b2.Name;
                                    break;
                                case 9:
                                    Met("Выберите верный способ вставки изображения?", "<img alt=\"MyImage\">image.gif", "<img src=\"image.gif\" alt=\"MyImage\">", "<img href=\"image.gif\" alt=\"MyImage\">", "<image src=\"image.gif\" alt=\"MyImage\">");
                                    otvet = b2.Name;
                                    break;
                                case 10:
                                    Met("Выберите верный способ установки фонового изображения страницы?", "<body background=\"background.gif\">", "<ing src=\"background.gif\" background>", "<background img=\"background.gif\">", "<img background=\"background.gif\">");
                                    otvet = b1.Name;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        default:
                            break;
                    }
                    break;
                case 9: // CSS
                    switch (AdminNumber.levv)
                    {
                        case 1:  // Начальный уровень
                            switch (quas)
                            {
                                case 1: // вопросы
                                    Met("Чтобы задать форму курсора, необходимо использовать CSS свойство:", "cursor", "pointer", "arrow", "Нельзя задать свою форму курсора в CSS");
                                    otvet = b1.Name;
                                    break;
                                case 2:
                                    Met("Чтобы задать прозрачность элемента на веб-странице необходимо использовать свойство:", "background-color", "opacity", "display", "position");
                                    otvet = b2.Name;
                                    break;
                                case 3:
                                    Met("Предназначение свойства clear в CSS:", "Очищает всю страницу, кроме блока к которому применяется данное свойство", "Устанавливает, с какой стороны элемента запрещено его обтекание другими элементами", "Очищает блок, к которому применяется данное свойство", "Очищает всю страницу");
                                    otvet = b2.Name;
                                    break;
                                case 4:
                                    Met("В чем отличие свойства outline от border:", "Здесь нет правильного ответа", "Между ними нет разницы", "Свойство outline не влияет на положение блока и его ширину", "Свойство border не влияет на положение блока и его ширину");
                                    otvet = b3.Name;
                                    break;
                                case 5:
                                    Met("При абсолютном позиционировании, чтобы изменить позицию элемента необходимо использовать\nсвойства:", "left, top, right и bottom", "При абсолютном позиционировании нельзя изменять положение элемента", "Только top и bottom", "Только left и right");
                                    otvet = b1.Name;
                                    break;
                                case 6:
                                    Met("За что отвечает свойство text-transform?", "Вращение текста по часовой стрелке", "Изменением цвета текста", "Динамическим изменением шрифта", "Преобразованием текста в заглавные и прописные символы");
                                    otvet = b4.Name;
                                    break;
                                case 7:
                                    Met("Для чего предназначено свойство padding в CSS?", "Фоновый цвет", "Рамка для контейнера", "Внутренний отступ", "Внешний отступ");
                                    otvet = b3.Name;
                                    break;
                                case 8:
                                    Met("Для чего предназначено свойство z-index:", "Определяет размер элемента", "Ни на что оно не влияет", "Обычное позиционирование влево-вправо, вверх-вниз", "Располагать элементы в определенном порядке, имитируя третье измерение, которое перпендикулярно экрану");
                                    otvet = b4.Name;
                                    break;
                                case 9:
                                    Met("Выберите правильное значение свойства position:", "absolute", "top", "bottom", "left");
                                    otvet = b1.Name;
                                    break;
                                case 10:
                                    Met("Какое CSS свойство используется для изменения стиля самой ссылки?", "a:hover", "a:visited", "a:link", "a:vlink");
                                    otvet = b3.Name;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case 2: // Средний уровень
                            switch (quas)
                            {
                                case 1: // вопросы
                                    Met("Какое из указанных значений свойства border-style, не существует?", "dashed", "dotted", "hiden", "Все свойства существуют");
                                    otvet = b3.Name;
                                    break;
                                case 2:
                                    Met("Сколько числовых значений может быть указано у свойства paddding?", "только одно", "до 4 включительно", "только два", "до 8 включительно");
                                    otvet = b2.Name;
                                    break;
                                case 3:
                                    css2_3.Visibility = Visibility.Visible;
                                    Met("Какой будет цвет у слова \"blah\"?", "Красный", "Фиолетовый", "Цвет по умолчанию", "Чёрный");
                                    otvet = b1.Name;
                                    break;
                                case 4:
                                    css2_3.Visibility = Visibility.Hidden;
                                    Met("Какую из операций не поддерживает функция calc()?", "+", "-", "*", "%");
                                    otvet = b4.Name;
                                    break;
                                case 5:
                                    Met("Выберите неправильный вариант указания размера текста.", "ek", "px", "pt", "проценты");
                                    otvet = b1.Name;
                                    break;
                                case 6:
                                    Met("Что необходимо написать в CSS чтобы изображение повторялось по вертикали:", "background-repeat: no-repeat;", "background-repeat: inherit;", "background-repeat: repeat-x;", "background-repeat: repeat-y;");
                                    otvet = b4.Name;
                                    break;
                                case 7:
                                    Met("Какой из вариантов задания цвета являются неверным?", "background-color: RGB(255, 0, 255);", "background-color: red;", "background-color:&hE99295;", "background-color:#ff00ff;");
                                    otvet = b3.Name;
                                    break;
                                case 8:
                                    Met("Выберите правильный вариант отступа сверху в 10 пикселей:", "margin: 0 10px 0 10px;", "Нет правильного ответа", "margin: 0 10px 10px 10px;", "margin: 10px 0 0 0;");
                                    otvet = b4.Name;
                                    break;
                                case 9:
                                    Met("Если для контейнера в CSS написать margin: 10px, то к чему будет применено данное свойство?", "К каждой из сторон контейнера", "К трем сторонам на усмотрение браузера", "Только к правой и левой стороне контейнера", "Только к верхней и нижней стороне контейнера");
                                    otvet = b1.Name;
                                    break;
                                case 10:
                                    Met("Какое CSS свойство используется для изменения стиля самой ссылки?", "a:hover", "a:visited", "a:link", "a:vlink");
                                    otvet = b3.Name;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case 3:  // Сложный уровень
                            switch (quas)
                            {
                                case 1: // вопросы
                                    Met("Какая величина в медиа-запросах позволяет применять стили при определенном сотношении сторон\nна устройстве отображения?", "device-aspect-ratio", "min-orientation", "orientation", "min-device-width");
                                    otvet = b1.Name;
                                    break;
                                case 2:
                                    Met("При задании свойств padding в процентах, относительно чего считаются эти проценты?", "padding родительского элемента", "Ширины самого элемента", "margin родительского элемента", "Ширины родительского элемента");
                                    otvet = b4.Name;
                                    break;
                                case 3:
                                    Met("Какое из значений свойств display делает HTML-элемент строчно-блочным?", "inline", "inline-block", "block", "list-item");
                                    otvet = b2.Name;
                                    break;
                                case 4:
                                    Met("Какой порядок следования отступов у свойства padding: 10px 20px 30px 40px", "сверха, слева, снизу, справа", "справа, снизу, слева, сверху", "сверха, слева, справа, снизу", "сверха, справа, снизу, слева");
                                    otvet = b4.Name;
                                    break;
                                case 5:
                                    Met("Какое CSS свойство используется для изменения стиля уже кликнутой ссылки?", "a:hover", "a:vlink", "a:visited", "a:link");
                                    otvet = b3.Name;
                                    break;
                                case 6:
                                    Met("Какой из стилей подключения css верен?", "@import css(“styles.css”)", "<link href=”styles.css” type=”stylesheet”>", "@import url(“styles.css”)", "<style href=”styles.css” />");
                                    otvet = b3.Name;
                                    break;
                                case 7:
                                    Met("Какой из селекторов выберет все HTML-элементы div с атрибутом title содержащим внутри значения\nподслово «bar»?", "div[title*=\"bar\"]", "div[title|=\"bar\"]", "div[title^=\"bar\"]", "div[title~=\"bar\"]");
                                    otvet = b1.Name;
                                    break;
                                case 8:
                                    css3_8.Visibility = Visibility.Visible;
                                    Met("Какой будет цвет у слова \"blah\"?", "Фиолетовый", "Цвет по умолчанию", "Чёрный", "Красный");
                                    otvet = b2.Name;
                                    break;
                                case 9:
                                    css3_8.Visibility = Visibility.Hidden;
                                    Met("Селектор «.some .next span» выберет для оформления HTML-элемент span в следующих фрагментах\nразметки", "<div class=\"some next\"> <div><span>текст</span></div> </div>", "<div class=\"some\"> <div class=\"next\"><span>текст</span></div> </div>", "<div class=\"some next\"> <div><p class=\"next\"><span>текст</span></p></div> </div>", "<nav class=\"some\"><a href=\"#\" class=\"next\"><span></span></a></nav>");
                                    otvet = b1.Name;
                                    break;
                                case 10:
                                    Met("Какое CSS свойство используется для определения стиля при наведении на ссылку курсора мыши,\nно при этом элемент еще не активирован?", "a:hover", "a:visited", "a:vlink", "a:link");
                                    otvet = b1.Name;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }
        }
        void Met(string v, string o1, string o2, string o3, string o4)
        {
            txt.Content = v;
            b1.Content = o1;
            b2.Content = o2;
            b3.Content = o3;
            b4.Content = o4;
        }

        private void git_Click(object sender, RoutedEventArgs e)
        {

        }

        private void button_back_Click(object sender, RoutedEventArgs e)
        {
            switch (AdminLevel.numm)
            {
                case 1: // Git
                    switch (AdminNumber.levv)
                    {
                        case 1: // Начальный уровень
                            switch (quas)
                            {
                                case 1: // вопросы
                                    Questions.git1 = ques.Text; Questions.git1_1 = o1.Text; Questions.git1_2 = o2.Text; Questions.git1_3 = o3.Text; Questions.git1_4 = o4.Text;
                                    break;
                                case 2:
                                    Questions.git2 = ques.Text; Questions.git2_1 = o1.Text; Questions.git2_2 = o2.Text; Questions.git2_3 = o3.Text; Questions.git2_4 = o4.Text;
                                    otvet = b2.Name;
                                    break;
                                case 3:
                                    Questions.git3 = ques.Text; Questions.git3_1 = o1.Text; Questions.git3_2 = o2.Text; Questions.git3_3 = o3.Text; Questions.git3_4 = o4.Text;
                                    otvet = b4.Name;
                                    break;
                                case 4:
                                    Questions.git4 = ques.Text; Questions.git4_1 = o1.Text; Questions.git4_2 = o2.Text; Questions.git4_3 = o3.Text; Questions.git4_4 = o4.Text;
                                    otvet = b1.Name;
                                    break;
                                case 5:
                                    Questions.git5 = ques.Text; Questions.git5_1 = o1.Text; Questions.git5_2 = o2.Text; Questions.git5_3 = o3.Text; Questions.git5_4 = o4.Text;
                                    otvet = b1.Name;
                                    break;
                                case 6:
                                    Questions.git6 = ques.Text; Questions.git6_1 = o1.Text; Questions.git6_2 = o2.Text; Questions.git6_3 = o3.Text; Questions.git6_4 = o4.Text;
                                    otvet = b3.Name;
                                    break;
                                case 7:
                                    Questions.git7 = ques.Text; Questions.git7_1 = o1.Text; Questions.git7_2 = o2.Text; Questions.git7_3 = o3.Text; Questions.git7_4 = o4.Text;
                                    otvet = b2.Name;
                                    break;
                                case 8:
                                    Questions.git8 = ques.Text; Questions.git8_1 = o1.Text; Questions.git8_2 = o2.Text; Questions.git8_3 = o3.Text; Questions.git8_4 = o4.Text;
                                    otvet = b3.Name;
                                    break;
                                case 9:
                                    Questions.git9 = ques.Text; Questions.git9_1 = o1.Text; Questions.git9_2 = o2.Text; Questions.git9_3 = o3.Text; Questions.git9_4 = o4.Text;
                                    otvet = b3.Name;
                                    break;
                                case 10:
                                    Questions.git10 = ques.Text; Questions.git10_1 = o1.Text; Questions.git10_2 = o2.Text; Questions.git10_3 = o3.Text; Questions.git10_4 = o4.Text;
                                    otvet = b3.Name;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case 2: // Средний уровень
                            switch (quas)
                            {
                                case 1: // вопросы
                                    Questions.gitt1 = ques.Text; Questions.gitt1_1 = o1.Text; Questions.gitt1_2 = o2.Text; Questions.gitt1_3 = o3.Text; Questions.gitt1_4 = o4.Text;
                                    break;
                                case 2:
                                    Questions.gitt2 = ques.Text; Questions.gitt2_1 = o1.Text; Questions.gitt2_2 = o2.Text; Questions.gitt2_3 = o3.Text; Questions.gitt2_4 = o4.Text;
                                    break;
                                case 3:
                                    Questions.gitt3 = ques.Text; Questions.gitt3_1 = o1.Text; Questions.gitt3_2 = o2.Text; Questions.gitt3_3 = o3.Text; Questions.gitt3_4 = o4.Text;
                                    break;
                                case 4:
                                    Questions.gitt4 = ques.Text; Questions.gitt4_1 = o1.Text; Questions.gitt4_2 = o2.Text; Questions.gitt4_3 = o3.Text; Questions.gitt4_4 = o4.Text;
                                    break;
                                case 5:
                                    Questions.gitt5 = ques.Text; Questions.gitt5_1 = o1.Text; Questions.gitt5_2 = o2.Text; Questions.gitt5_3 = o3.Text; Questions.gitt5_4 = o4.Text;
                                    break;
                                case 6:
                                    Questions.gitt6 = ques.Text; Questions.gitt6_1 = o1.Text; Questions.gitt6_2 = o2.Text; Questions.gitt6_3 = o3.Text; Questions.gitt6_4 = o4.Text;
                                    break;
                                case 7:
                                    Questions.gitt7 = ques.Text; Questions.gitt7_1 = o1.Text; Questions.gitt7_2 = o2.Text; Questions.gitt7_3 = o3.Text; Questions.gitt7_4 = o4.Text;
                                    break;
                                case 8:
                                    Questions.gitt8 = ques.Text; Questions.gitt8_1 = o1.Text; Questions.gitt8_2 = o2.Text; Questions.gitt8_3 = o3.Text; Questions.gitt8_4 = o4.Text;
                                    break;
                                case 9:
                                    Questions.gitt9 = ques.Text; Questions.gitt9_1 = o1.Text; Questions.gitt9_2 = o2.Text; Questions.gitt9_3 = o3.Text; Questions.gitt9_4 = o4.Text;
                                    break;
                                case 10:
                                    Questions.gitt10 = ques.Text; Questions.gitt10_1 = o1.Text; Questions.gitt10_2 = o2.Text; Questions.gitt10_3 = o3.Text; Questions.gitt10_4 = o4.Text;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case 3: // Сложный уровень
                            switch (quas)
                            {
                                case 1: // вопросы
                                    Questions.gittt1 = ques.Text; Questions.gittt1_1 = o1.Text; Questions.gittt1_2 = o2.Text; Questions.gittt1_3 = o3.Text; Questions.gittt1_4 = o4.Text;
                                    break;
                                case 2:
                                    Questions.gittt2 = ques.Text; Questions.gittt2_1 = o1.Text; Questions.gittt2_2 = o2.Text; Questions.gittt2_3 = o3.Text; Questions.gittt2_4 = o4.Text;
                                    break;
                                case 3:
                                    Questions.gittt3 = ques.Text; Questions.gittt3_1 = o1.Text; Questions.gittt3_2 = o2.Text; Questions.gittt3_3 = o3.Text; Questions.gittt3_4 = o4.Text;
                                    break;
                                case 4:
                                    Questions.gittt4 = ques.Text; Questions.gittt4_1 = o1.Text; Questions.gittt4_2 = o2.Text; Questions.gittt4_3 = o3.Text; Questions.gittt4_4 = o4.Text;
                                    break;
                                case 5:
                                    Questions.gittt5 = ques.Text; Questions.gittt5_1 = o1.Text; Questions.gittt5_2 = o2.Text; Questions.gittt5_3 = o3.Text; Questions.gittt5_4 = o4.Text;
                                    break;
                                case 6:
                                    Questions.gittt6 = ques.Text; Questions.gittt6_1 = o1.Text; Questions.gittt6_2 = o2.Text; Questions.gittt6_3 = o3.Text; Questions.gittt6_4 = o4.Text;
                                    break;
                                case 7:
                                    Questions.gittt7 = ques.Text; Questions.gittt7_1 = o1.Text; Questions.gittt7_2 = o2.Text; Questions.gittt7_3 = o3.Text; Questions.gittt7_4 = o4.Text;
                                    break;
                                case 8:
                                    Questions.gittt8 = ques.Text; Questions.gittt8_1 = o1.Text; Questions.gittt8_2 = o2.Text; Questions.gittt8_3 = o3.Text; Questions.gittt8_4 = o4.Text;
                                    break;
                                case 9:
                                    Questions.gittt9 = ques.Text; Questions.gittt9_1 = o1.Text; Questions.gittt9_2 = o2.Text; Questions.gittt9_3 = o3.Text; Questions.gittt9_4 = o4.Text;
                                    break;
                                case 10:
                                    Questions.gittt10 = ques.Text; Questions.gittt10_1 = o1.Text; Questions.gittt10_2 = o2.Text; Questions.gittt10_3 = o3.Text; Questions.gittt10_4 = o4.Text;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        default:
                            break;
                    }
                    break;
                case 2: // SQL
                    switch (AdminNumber.levv)
                    {
                        case 1: // Начальный уровень
                            switch (quas)
                            {
                                case 1: // вопросы
                                    Questions.sql1 = ques.Text; Questions.sql1_1 = o1.Text; Questions.sql1_2 = o2.Text; Questions.sql1_3 = o3.Text; Questions.sql1_4 = o4.Text;
                                    otvet = b1.Name;
                                    break;
                                case 2:
                                    Questions.sql2 = ques.Text; Questions.sql2_1 = o1.Text; Questions.sql2_2 = o2.Text; Questions.sql2_3 = o3.Text; Questions.sql2_4 = o4.Text;
                                    otvet = b2.Name;
                                    break;
                                case 3:
                                    Questions.sql3 = ques.Text; Questions.sql3_1 = o1.Text; Questions.sql3_2 = o2.Text; Questions.sql3_3 = o3.Text; Questions.sql3_4 = o4.Text;
                                    otvet = b2.Name;
                                    break;
                                case 4:
                                    Questions.sql4 = ques.Text; Questions.sql4_1 = o1.Text; Questions.sql4_2 = o2.Text; Questions.sql4_3 = o3.Text; Questions.sql4_4 = o4.Text;
                                    otvet = b3.Name;
                                    break;
                                case 5:
                                    Questions.sql5 = ques.Text; Questions.sql5_1 = o1.Text; Questions.sql5_2 = o2.Text; Questions.sql5_3 = o3.Text; Questions.sql5_4 = o4.Text;
                                    otvet = b1.Name;
                                    break;
                                case 6:
                                    Questions.sql6 = ques.Text; Questions.sql6_1 = o1.Text; Questions.sql6_2 = o2.Text; Questions.sql6_3 = o3.Text; Questions.sql6_4 = o4.Text;
                                    otvet = b4.Name;
                                    break;
                                case 7:
                                    Questions.sql7 = ques.Text; Questions.sql7_1 = o1.Text; Questions.sql7_2 = o2.Text; Questions.sql7_3 = o3.Text; Questions.sql7_4 = o4.Text;
                                    otvet = b3.Name;
                                    break;
                                case 8:
                                    Questions.sql8 = ques.Text; Questions.sql8_1 = o1.Text; Questions.sql8_2 = o2.Text; Questions.sql8_3 = o3.Text; Questions.sql8_4 = o4.Text;
                                    otvet = b4.Name;
                                    break;
                                case 9:
                                    Questions.sql9 = ques.Text; Questions.sql9_1 = o1.Text; Questions.sql9_2 = o2.Text; Questions.sql9_3 = o3.Text; Questions.sql9_4 = o4.Text;
                                    otvet = b1.Name;
                                    break;
                                case 10:
                                    Questions.sql10 = ques.Text; Questions.sql10_1 = o1.Text; Questions.sql10_2 = o2.Text; Questions.sql10_3 = o3.Text; Questions.sql10_4 = o4.Text;
                                    otvet = b3.Name;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case 2: // Средний уровень
                            switch (quas)
                            {
                                case 1: // вопросы
                                    Questions.sqll1 = ques.Text; Questions.sqll1_1 = o1.Text; Questions.sqll1_2 = o2.Text; Questions.sqll1_3 = o3.Text; Questions.sqll1_4 = o4.Text;
                                    otvet = b1.Name;
                                    break;
                                case 2:
                                    Questions.sqll2 = ques.Text; Questions.sqll2_1 = o1.Text; Questions.sqll2_2 = o2.Text; Questions.sqll2_3 = o3.Text; Questions.sqll2_4 = o4.Text;
                                    otvet = b2.Name;
                                    break;
                                case 3:
                                    Questions.sqll3 = ques.Text; Questions.sqll3_1 = o1.Text; Questions.sqll3_2 = o2.Text; Questions.sqll3_3 = o3.Text; Questions.sqll3_4 = o4.Text;
                                    otvet = b2.Name;
                                    break;
                                case 4:
                                    Questions.sqll4 = ques.Text; Questions.sqll4_1 = o1.Text; Questions.sqll4_2 = o2.Text; Questions.sqll4_3 = o3.Text; Questions.sqll4_4 = o4.Text;
                                    otvet = b3.Name;
                                    break;
                                case 5:
                                    Questions.sqll5 = ques.Text; Questions.sqll5_1 = o1.Text; Questions.sqll5_2 = o2.Text; Questions.sqll5_3 = o3.Text; Questions.sqll5_4 = o4.Text;
                                    otvet = b1.Name;
                                    break;
                                case 6:
                                    Questions.sqll6 = ques.Text; Questions.sqll6_1 = o1.Text; Questions.sqll6_2 = o2.Text; Questions.sqll6_3 = o3.Text; Questions.sqll6_4 = o4.Text;
                                    otvet = b4.Name;
                                    break;
                                case 7:
                                    Questions.sqll7 = ques.Text; Questions.sqll7_1 = o1.Text; Questions.sqll7_2 = o2.Text; Questions.sqll7_3 = o3.Text; Questions.sqll7_4 = o4.Text;
                                    otvet = b3.Name;
                                    break;
                                case 8:
                                    Questions.sqll8 = ques.Text; Questions.sqll8_1 = o1.Text; Questions.sqll8_2 = o2.Text; Questions.sqll8_3 = o3.Text; Questions.sqll8_4 = o4.Text;
                                    otvet = b4.Name;
                                    break;
                                case 9:
                                    Questions.sqll9 = ques.Text; Questions.sqll9_1 = o1.Text; Questions.sqll9_2 = o2.Text; Questions.sqll9_3 = o3.Text; Questions.sqll9_4 = o4.Text;
                                    otvet = b1.Name;
                                    break;
                                case 10:
                                    Questions.sqll10 = ques.Text; Questions.sqll10_1 = o1.Text; Questions.sqll10_2 = o2.Text; Questions.sqll10_3 = o3.Text; Questions.sqll10_4 = o4.Text;
                                    otvet = b3.Name;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case 3: // Сложный уровень
                            switch (quas)
                            {
                                case 1: // вопросы
                                    Questions.sqlll1 = ques.Text; Questions.sqlll1_1 = o1.Text; Questions.sqlll1_2 = o2.Text; Questions.sqlll1_3 = o3.Text; Questions.sqlll1_4 = o4.Text;
                                    otvet = b1.Name;
                                    break;
                                case 2:
                                    Questions.sqlll2 = ques.Text; Questions.sqlll2_1 = o1.Text; Questions.sqlll2_2 = o2.Text; Questions.sqlll2_3 = o3.Text; Questions.sqlll2_4 = o4.Text;
                                    otvet = b2.Name;
                                    break;
                                case 3:
                                    Questions.sqll3 = ques.Text; Questions.sqll3_1 = o1.Text; Questions.sqll3_2 = o2.Text; Questions.sqll3_3 = o3.Text; Questions.sqll3_4 = o4.Text;
                                    otvet = b2.Name;
                                    break;
                                case 4:
                                    Questions.sqlll4 = ques.Text; Questions.sqlll4_1 = o1.Text; Questions.sqlll4_2 = o2.Text; Questions.sqlll4_3 = o3.Text; Questions.sqlll4_4 = o4.Text;
                                    otvet = b3.Name;
                                    break;
                                case 5:
                                    Questions.sqlll5 = ques.Text; Questions.sqlll5_1 = o1.Text; Questions.sqlll5_2 = o2.Text; Questions.sqlll5_3 = o3.Text; Questions.sqlll5_4 = o4.Text;
                                    otvet = b1.Name;
                                    break;
                                case 6:
                                    Questions.sqlll6 = ques.Text; Questions.sqlll6_1 = o1.Text; Questions.sqlll6_2 = o2.Text; Questions.sqlll6_3 = o3.Text; Questions.sqlll6_4 = o4.Text;
                                    otvet = b4.Name;
                                    break;
                                case 7:
                                    Questions.sqlll7 = ques.Text; Questions.sqlll7_1 = o1.Text; Questions.sqlll7_2 = o2.Text; Questions.sqlll7_3 = o3.Text; Questions.sqlll7_4 = o4.Text;
                                    otvet = b3.Name;
                                    break;
                                case 8:
                                    Questions.sqlll8 = ques.Text; Questions.sqlll8_1 = o1.Text; Questions.sqlll8_2 = o2.Text; Questions.sqlll8_3 = o3.Text; Questions.sqlll8_4 = o4.Text;
                                    otvet = b4.Name;
                                    break;
                                case 9:
                                    Questions.sqlll9 = ques.Text; Questions.sqlll9_1 = o1.Text; Questions.sqlll9_2 = o2.Text; Questions.sqlll9_3 = o3.Text; Questions.sqlll9_4 = o4.Text;
                                    otvet = b1.Name;
                                    break;
                                case 10:
                                    Questions.sqlll10 = ques.Text; Questions.sqlll10_1 = o1.Text; Questions.sqlll10_2 = o2.Text; Questions.sqlll10_3 = o3.Text; Questions.sqlll10_4 = o4.Text;
                                    otvet = b3.Name;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        default:
                            break;
                    }
                    break;
                case 3: // Java
                    switch (AdminNumber.levv)
                    {
                        case 1:  // Начальный уровень
                            switch (quas)
                            {
                                case 1: // вопросы
                                    Questions.java1 = ques.Text; Questions.java1_1 = o1.Text; Questions.java1_2 = o2.Text; Questions.java1_3 = o3.Text; Questions.java1_4 = o4.Text;
                                    otvet = b1.Name;
                                    break;
                                case 2:
                                    Questions.java2 = ques.Text; Questions.java22_1 = o1.Text; Questions.java22_2 = o2.Text; Questions.java2_3 = o3.Text; Questions.java22_4 = o4.Text;
                                    otvet = b4.Name;
                                    break;
                                case 3:
                                    Questions.java3 = ques.Text; Questions.java33_1 = o1.Text; Questions.java33_2 = o2.Text; Questions.java33_3 = o3.Text; Questions.java3_4 = o4.Text;
                                    otvet = b2.Name;
                                    break;
                                case 4:
                                    Questions.java4 = ques.Text; Questions.java4_1 = o1.Text; Questions.java4_2 = o2.Text; Questions.java4_3 = o3.Text; Questions.java4_4 = o4.Text;
                                    otvet = b3.Name;
                                    break;
                                case 5:
                                    Questions.java5 = ques.Text; Questions.java5_1 = o1.Text; Questions.java5_2 = o2.Text; Questions.java5_3 = o3.Text; Questions.java5_4 = o4.Text;
                                    otvet = b1.Name;
                                    break;
                                case 6:
                                    Questions.java6 = ques.Text; Questions.java6_1 = o1.Text; Questions.java6_2 = o2.Text; Questions.java6_3 = o3.Text; Questions.java6_4 = o4.Text;
                                    otvet = b3.Name;
                                    break;
                                case 7:
                                    java1_7.Visibility = Visibility.Visible;
                                    Questions.java7 = ques.Text; Questions.java7_1 = o1.Text; Questions.java7_2 = o2.Text; Questions.java7_3 = o3.Text; Questions.java7_4 = o4.Text;
                                    otvet = b2.Name;
                                    break;
                                case 8:
                                    java1_7.Visibility = Visibility.Hidden;
                                    Questions.java8 = ques.Text; Questions.java8_1 = o1.Text; Questions.java8_2 = o2.Text; Questions.java8_3 = o3.Text; Questions.java8_4 = o4.Text;
                                    otvet = b3.Name;
                                    break;
                                case 9:
                                    Questions.java9 = ques.Text; Questions.java9_1 = o1.Text; Questions.java9_2 = o2.Text; Questions.java9_3 = o3.Text; Questions.java9_4 = o4.Text;
                                    otvet = b2.Name;
                                    break;
                                case 10:
                                    Questions.java10 = ques.Text; Questions.java10_1 = o1.Text; Questions.java10_2 = o2.Text; Questions.java10_3 = o3.Text; Questions.java10_4 = o4.Text;
                                    otvet = b2.Name;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case 2: // Средний уровень
                            switch (quas)
                            {
                                case 1: // вопросы
                                    java2_1.Visibility = Visibility.Visible;
                                    Questions.javaa1 = ques.Text; Questions.javaa1_1 = o1.Text; Questions.javaa1_2 = o2.Text; Questions.javaa1_3 = o3.Text; Questions.javaa1_4 = o4.Text;
                                    otvet = b4.Name;
                                    break;
                                case 2:
                                    java2_1.Visibility = Visibility.Hidden;
                                    java2_2.Visibility = Visibility.Visible;
                                    Questions.javaa2 = ques.Text; Questions.javaa2_1 = o1.Text; Questions.javaa2_2 = o2.Text; Questions.javaa2_3 = o3.Text; Questions.javaa2_4 = o4.Text;
                                    otvet = b1.Name;
                                    break;
                                case 3:
                                    java2_2.Visibility = Visibility.Hidden;
                                    Questions.javaa3 = ques.Text; Questions.javaa3_1 = o1.Text; Questions.javaa3_2 = o2.Text; Questions.javaa3_3 = o3.Text; Questions.javaa3_4 = o4.Text;
                                    otvet = b4.Name;
                                    break;
                                case 4:
                                    java2_4.Visibility = Visibility.Visible;
                                    Questions.javaa4 = ques.Text; Questions.javaa4_1 = o1.Text; Questions.javaa4_2 = o2.Text; Questions.javaa4_3 = o3.Text; Questions.javaa4_4 = o4.Text;
                                    otvet = b1.Name;
                                    break;
                                case 5:
                                    java2_4.Visibility = Visibility.Hidden;
                                    java2_5.Visibility = Visibility.Visible;
                                    Questions.javaa5 = ques.Text; Questions.javaa5_1 = o1.Text; Questions.javaa5_2 = o2.Text; Questions.javaa5_3 = o3.Text; Questions.javaa5_4 = o4.Text;
                                    otvet = b3.Name;
                                    break;
                                case 6:
                                    java2_5.Visibility = Visibility.Hidden;
                                    Questions.javaa6 = ques.Text; Questions.javaa6_1 = o1.Text; Questions.javaa6_2 = o2.Text; Questions.javaa6_3 = o3.Text; Questions.javaa6_4 = o4.Text;
                                    otvet = b1.Name;
                                    break;
                                case 7:
                                    java2_7.Visibility = Visibility.Visible;
                                    Questions.javaa7 = ques.Text; Questions.javaa7_1 = o1.Text; Questions.javaa7_2 = o2.Text; Questions.javaa7_3 = o3.Text; Questions.javaa7_4 = o4.Text;
                                    otvet = b1.Name;
                                    break;
                                case 8:
                                    java2_7.Visibility = Visibility.Hidden;
                                    java2_8.Visibility = Visibility.Visible;
                                    Questions.javaa8 = ques.Text; Questions.javaa8_1 = o1.Text; Questions.javaa8_2 = o2.Text; Questions.javaa8_3 = o3.Text; Questions.javaa8_4 = o4.Text;
                                    otvet = b4.Name;
                                    break;
                                case 9:
                                    java2_8.Visibility = Visibility.Hidden;
                                    java2_9.Visibility = Visibility.Visible;
                                    Questions.javaa9 = ques.Text; Questions.javaa9_1 = o1.Text; Questions.javaa9_2 = o2.Text; Questions.javaa9_3 = o3.Text; Questions.javaa9_4 = o4.Text;
                                    otvet = b1.Name;
                                    break;
                                case 10:
                                    java2_9.Visibility = Visibility.Hidden;
                                    Questions.javaa10 = ques.Text; Questions.javaa10_1 = o1.Text; Questions.javaa10_2 = o2.Text; Questions.javaa10_3 = o3.Text; Questions.javaa10_4 = o4.Text;
                                    otvet = b2.Name;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case 3:  // Сложный уровень
                            switch (quas)
                            {
                                case 1: // вопросы
                                    java3_1.Visibility = Visibility.Visible;
                                    Questions.javaaa1 = ques.Text; Questions.javaaa1_1 = o1.Text; Questions.javaaa1_2 = o2.Text; Questions.javaaa1_3 = o3.Text; Questions.javaaa1_4 = o4.Text;
                                    otvet = b1.Name;
                                    break;
                                case 2:
                                    java3_1.Visibility = Visibility.Hidden;
                                    java3_2.Visibility = Visibility.Visible;
                                    Questions.javaaa2 = ques.Text; Questions.javaaa2_1 = o1.Text; Questions.javaaa2_2 = o2.Text; Questions.javaaa2_3 = o3.Text; Questions.javaaa2_4 = o4.Text;
                                    otvet = b2.Name;
                                    break;
                                case 3:
                                    java3_2.Visibility = Visibility.Hidden;
                                    java3_3.Visibility = Visibility.Visible;
                                    Questions.javaaa3 = ques.Text; Questions.javaaa3_1 = o1.Text; Questions.javaaa3_2 = o2.Text; Questions.javaaa3_3 = o3.Text; Questions.javaaa3_4 = o4.Text;
                                    otvet = b3.Name;
                                    break;
                                case 4:
                                    java3_3.Visibility = Visibility.Hidden;
                                    Questions.javaaa4 = ques.Text; Questions.javaaa4_1 = o1.Text; Questions.javaaa4_2 = o2.Text; Questions.javaaa4_3 = o3.Text; Questions.javaaa4_4 = o4.Text;
                                    otvet = b3.Name;
                                    break;
                                case 5:
                                    Questions.javaaa5 = ques.Text; Questions.javaaa5_1 = o1.Text; Questions.javaaa5_2 = o2.Text; Questions.javaaa5_3 = o3.Text; Questions.javaaa5_4 = o4.Text;
                                    otvet = b2.Name;
                                    break;
                                case 6:
                                    Questions.javaaa6 = ques.Text; Questions.javaaa6_1 = o1.Text; Questions.javaaa6_2 = o2.Text; Questions.javaaa6_3 = o3.Text; Questions.javaaa6_4 = o4.Text;
                                    otvet = b1.Name;
                                    break;
                                case 7:
                                    java3_7.Visibility = Visibility.Visible;
                                    Questions.javaaa7 = ques.Text; Questions.javaaa7_1 = o1.Text; Questions.javaaa7_2 = o2.Text; Questions.javaaa7_3 = o3.Text; Questions.javaaa7_4 = o4.Text;
                                    otvet = b3.Name;
                                    break;
                                case 8:
                                    java3_7.Visibility = Visibility.Hidden;
                                    java3_8.Visibility = Visibility.Visible;
                                    Questions.javaaa8 = ques.Text; Questions.javaaa8_1 = o1.Text; Questions.javaaa8_2 = o2.Text; Questions.javaaa8_3 = o3.Text; Questions.javaaa8_4 = o4.Text;
                                    otvet = b2.Name;
                                    break;
                                case 9:
                                    java3_8.Visibility = Visibility.Hidden;
                                    java3_9.Visibility = Visibility.Visible;
                                    Questions.javaaa9 = ques.Text; Questions.javaaa9_1 = o1.Text; Questions.javaaa9_2 = o2.Text; Questions.javaaa9_3 = o3.Text; Questions.javaaa9_4 = o4.Text;
                                    otvet = b3.Name;
                                    break;
                                case 10:
                                    java3_9.Visibility = Visibility.Hidden;
                                    Questions.javaaa10 = ques.Text; Questions.javaaa10_1 = o1.Text; Questions.javaaa10_2 = o2.Text; Questions.javaaa10_3 = o3.Text; Questions.javaaa10_4 = o4.Text;
                                    otvet = b4.Name;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        default:
                            break;
                    }
                    break;
                case 4: // Python
                    switch (AdminNumber.levv)
                    {
                        case 1:  // Начальный уровень
                            switch (quas)
                            {
                                case 1: // вопросы
                                    Questions.python1 = ques.Text; Questions.python1_1 = o1.Text; Questions.python1_2 = o2.Text; Questions.python1_3 = o3.Text; Questions.python1_4 = o4.Text;
                                    otvet = b3.Name;
                                    break;
                                case 2:
                                    Questions.python2 = ques.Text; Questions.python22_1 = o1.Text; Questions.python22_2 = o2.Text; Questions.python22_3 = o3.Text; Questions.python22_4 = o4.Text;
                                    otvet = b2.Name;
                                    break;
                                case 3:
                                    Questions.python3 = ques.Text; Questions.python3_1 = o1.Text; Questions.python3_2 = o2.Text; Questions.python33_3 = o3.Text; Questions.python3_4 = o4.Text;
                                    otvet = b3.Name;
                                    break;
                                case 4:
                                    Questions.python4 = ques.Text; Questions.python4_1 = o1.Text; Questions.python4_2 = o2.Text; Questions.python4_3 = o3.Text; Questions.python4_4 = o4.Text;
                                    otvet = b4.Name;
                                    break;
                                case 5:
                                    Questions.python5 = ques.Text; Questions.python5_1 = o1.Text; Questions.python5_2 = o2.Text; Questions.python5_3 = o3.Text; Questions.python5_4 = o4.Text;
                                    otvet = b1.Name;
                                    break;
                                case 6:
                                    Questions.python6 = ques.Text; Questions.python6_1 = o1.Text; Questions.python6_2 = o2.Text; Questions.python6_3 = o3.Text; Questions.python6_4 = o4.Text;
                                    otvet = b4.Name;
                                    break;
                                case 7:
                                    Questions.python7 = ques.Text; Questions.python7_1 = o1.Text; Questions.python7_2 = o2.Text; Questions.python7_3 = o3.Text; Questions.python7_4 = o4.Text;
                                    otvet = b3.Name;
                                    break;
                                case 8:
                                    Questions.python8 = ques.Text; Questions.python8_1 = o1.Text; Questions.python8_2 = o2.Text; Questions.python8_3 = o3.Text; Questions.python8_4 = o4.Text;
                                    otvet = b1.Name;
                                    break;
                                case 9:
                                    Questions.python9 = ques.Text; Questions.python9_1 = o1.Text; Questions.python9_2 = o2.Text; Questions.python9_3 = o3.Text; Questions.python9_4 = o4.Text;
                                    otvet = b1.Name;
                                    break;
                                case 10:
                                    Questions.python10 = ques.Text; Questions.python10_1 = o1.Text; Questions.python10_2 = o2.Text; Questions.python10_3 = o3.Text; Questions.python10_4 = o4.Text;
                                    otvet = b1.Name;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case 2: // Средний уровень
                            switch (quas)
                            {
                                case 1: // вопросы
                                    python2_1.Visibility = Visibility.Visible;
                                    Questions.pythonn1 = ques.Text; Questions.pythonn1_1 = o1.Text; Questions.pythonn1_2 = o2.Text; Questions.pythonn1_3 = o3.Text; Questions.pythonn1_4 = o4.Text;
                                    otvet = b1.Name;
                                    break;
                                case 2:
                                    python2_1.Visibility = Visibility.Hidden;
                                    python2_2.Visibility = Visibility.Visible;
                                    Questions.pythonn2 = ques.Text; Questions.pythonn2_1 = o1.Text; Questions.pythonn2_2 = o2.Text; Questions.pythonn2_3 = o3.Text; Questions.pythonn2_4 = o4.Text;
                                    otvet = b2.Name;
                                    break;
                                case 3:
                                    python2_2.Visibility = Visibility.Hidden;
                                    python2_3.Visibility = Visibility.Visible;
                                    Questions.pythonn3 = ques.Text; Questions.pythonn3_1 = o1.Text; Questions.pythonn3_2 = o2.Text; Questions.pythonn3_3 = o3.Text; Questions.pythonn3_4 = o4.Text;
                                    otvet = b2.Name;
                                    break;
                                case 4:
                                    python2_3.Visibility = Visibility.Hidden;
                                    python2_4.Visibility = Visibility.Visible;
                                    Questions.pythonn4 = ques.Text; Questions.pythonn4_1 = o1.Text; Questions.pythonn4_2 = o2.Text; Questions.pythonn4_3 = o3.Text; Questions.pythonn4_4 = o4.Text;
                                    otvet = b3.Name;
                                    break;
                                case 5:
                                    python2_4.Visibility = Visibility.Hidden;
                                    python2_5.Visibility = Visibility.Visible;
                                    Questions.pythonn5 = ques.Text; Questions.pythonn5_1 = o1.Text; Questions.pythonn5_2 = o2.Text; Questions.pythonn5_3 = o3.Text; Questions.pythonn5_4 = o4.Text;
                                    otvet = b1.Name;
                                    break;
                                case 6:
                                    python2_5.Visibility = Visibility.Hidden;
                                    python2_6.Visibility = Visibility.Visible;
                                    Questions.pythonn6 = ques.Text; Questions.pythonn6_1 = o1.Text; Questions.pythonn6_2 = o2.Text; Questions.pythonn6_3 = o3.Text; Questions.pythonn6_4 = o4.Text;
                                    otvet = b4.Name;
                                    break;
                                case 7:
                                    python2_6.Visibility = Visibility.Hidden;
                                    Questions.pythonn7 = ques.Text; Questions.pythonn7_1 = o1.Text; Questions.pythonn7_2 = o2.Text; Questions.pythonn7_3 = o3.Text; Questions.pythonn7_4 = o4.Text;
                                    otvet = b3.Name;
                                    break;
                                case 8:
                                    Questions.pythonn8 = ques.Text; Questions.pythonn8_1 = o1.Text; Questions.pythonn8_2 = o2.Text; Questions.pythonn8_3 = o3.Text; Questions.pythonn8_4 = o4.Text;
                                    otvet = b4.Name;
                                    break;
                                case 9:
                                    Questions.pythonn9 = ques.Text; Questions.pythonn9_1 = o1.Text; Questions.pythonn9_2 = o2.Text; Questions.pythonn9_3 = o3.Text; Questions.pythonn9_4 = o4.Text;
                                    otvet = b1.Name;
                                    break;
                                case 10:
                                    Questions.pythonn10 = ques.Text; Questions.pythonn10_1 = o1.Text; Questions.pythonn10_2 = o2.Text; Questions.pythonn10_3 = o3.Text; Questions.pythonn10_4 = o4.Text;
                                    otvet = b3.Name;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case 3:  // Сложный уровень
                            switch (quas)
                            {
                                case 1: // вопросы
                                    Questions.pythonnn1 = ques.Text; Questions.pythonnn1_1 = o1.Text; Questions.pythonnn1_2 = o2.Text; Questions.pythonnn1_3 = o3.Text; Questions.pythonnn1_4 = o4.Text;
                                    otvet = b1.Name;
                                    break;
                                case 2:
                                    Questions.pythonnn2 = ques.Text; Questions.pythonnn2_1 = o1.Text; Questions.pythonnn2_2 = o2.Text; Questions.pythonnn2_3 = o3.Text; Questions.pythonnn2_4 = o4.Text;
                                    otvet = b2.Name;
                                    break;
                                case 3:
                                    python3_3.Visibility = Visibility.Visible;
                                    Questions.pythonnn3 = ques.Text; Questions.pythonnn3_1 = o1.Text; Questions.pythonnn3_2 = o2.Text; Questions.pythonnn3_3 = o3.Text; Questions.pythonnn3_4 = o4.Text;
                                    otvet = b2.Name;
                                    break;
                                case 4:
                                    python3_3.Visibility = Visibility.Hidden;
                                    Questions.pythonnn4 = ques.Text; Questions.pythonnn4_1 = o1.Text; Questions.pythonnn4_2 = o2.Text; Questions.pythonnn4_3 = o3.Text; Questions.pythonnn4_4 = o4.Text;
                                    otvet = b3.Name;
                                    break;
                                case 5:
                                    python3_5.Visibility = Visibility.Visible;
                                    Questions.pythonnn5 = ques.Text; Questions.pythonnn5_1 = o1.Text; Questions.pythonnn5_2 = o2.Text; Questions.pythonnn5_3 = o3.Text; Questions.pythonnn5_4 = o4.Text;
                                    otvet = b1.Name;
                                    break;
                                case 6:
                                    python3_5.Visibility = Visibility.Hidden;
                                    Questions.pythonnn6 = ques.Text; Questions.pythonnn6_1 = o1.Text; Questions.pythonnn6_2 = o2.Text; Questions.pythonnn6_3 = o3.Text; Questions.pythonnn6_4 = o4.Text;
                                    otvet = b4.Name;
                                    break;
                                case 7:
                                    python3_7.Visibility = Visibility.Visible;
                                    Questions.pythonnn7 = ques.Text; Questions.pythonnn7_1 = o1.Text; Questions.pythonnn7_2 = o2.Text; Questions.pythonnn7_3 = o3.Text; Questions.pythonnn7_4 = o4.Text;
                                    otvet = b3.Name;
                                    break;
                                case 8:
                                    python3_7.Visibility = Visibility.Hidden;
                                    python3_8.Visibility = Visibility.Visible;
                                    Questions.pythonnn8 = ques.Text; Questions.pythonnn8_1 = o1.Text; Questions.pythonnn8_2 = o2.Text; Questions.pythonnn8_3 = o3.Text; Questions.pythonnn8_4 = o4.Text;
                                    otvet = b4.Name;
                                    break;
                                case 9:
                                    Questions.pythonnn9 = ques.Text; Questions.pythonnn9_1 = o1.Text; Questions.pythonnn9_2 = o2.Text; Questions.pythonnn9_3 = o3.Text; Questions.pythonnn9_4 = o4.Text;
                                    python3_8.Visibility = Visibility.Hidden;
                                    otvet = b1.Name;
                                    break;
                                case 10:
                                    Questions.pythonnn10 = ques.Text; Questions.pythonnn10_1 = o1.Text; Questions.pythonnn10_2 = o2.Text; Questions.pythonnn10_3 = o3.Text; Questions.pythonnn10_4 = o4.Text;
                                    otvet = b3.Name;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        default:
                            break;
                    }
                    break;
                case 5: // C++
                    switch (AdminNumber.levv)
                    {
                        case 1:  // Начальный уровень 
                            switch (quas)
                            {
                                case 1: // вопросы
                                    Met("Какой из вариантов записи подключения стандартной библиотеки является правильным с точки\nзрения стандарта C++?", "#include 'iostream'", "#include <iostream>", "Оба варианта неправильные", "Оба варианта правильные");
                                    otvet = b2.Name;
                                    break;
                                case 2:
                                    Met("Какой из перечисленных модификаторов является модификатором разрема?", "long", "signed", "unsigned", "Ни один из перечисленных не является модификатором размера");
                                    otvet = b1.Name;
                                    break;
                                case 3:
                                    Met("Выберите недопустимый идентификатор переменной из нижеуказанного.", "Int", "bool", "DOUBLE", "0");
                                    otvet = b2.Name;
                                    break;
                                case 4:
                                    Met("Какая из записей является ошибочной?", "Все варианты правильные", "Все варианты неправильные", "double a2 {2.3}", "int d2 = {2.3}");
                                    otvet = b4.Name;
                                    break;
                                case 5:
                                    cplus1_5.Visibility = Visibility.Visible;
                                    Met("Какой результат работы следующей программы?", "10", "'мусор", "Ошибка компиляции", "Ошибка выполнения");
                                    otvet = b3.Name;
                                    break;
                                case 6:
                                    cplus1_5.Visibility = Visibility.Hidden;
                                    Met("Какой из вариантов записи спецификатора auto является ошибочным?", "auto b; b = 14", "const auto a = 'Hello'", "auto a = -58", "auto = 0");
                                    otvet = b1.Name;
                                    break;
                                case 7:
                                    Met("Кто является автором языка программирования C++?", "Charles Babbage", "Dennis Ritchie", "Brain Kernighan", "Bjarne Stroustrup");
                                    otvet = b4.Name;
                                    break;
                                case 8:
                                    Met("Какой из перечисленных типов данных не является фундаментальным?", "double", "void", "string", "bool");
                                    otvet = b3.Name;
                                    break;
                                case 9:
                                    cplus1_9.Visibility = Visibility.Visible;
                                    Met("Сколько раз выполнится цикл?", "Один", "Два", "Цикл бесконечный", "Не выполнится, в коде есть ошибка");
                                    otvet = b1.Name;
                                    break;
                                case 10:
                                    cplus1_9.Visibility = Visibility.Hidden;
                                    Met("Как нужно объявлять константу?", "#define x = 10", "int = 10", "const int = 10", "const 5");
                                    otvet = b3.Name;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case 2: // Средний уровень
                            switch (quas)
                            {
                                case 1: // вопросы
                                    cplus2_1.Visibility = Visibility.Visible;
                                    Met("Сколько раз вызовется любая move операция класса x?", "Хотя бы раз", "Хотя бы два раза", "Нисколько", "Хотя бы три раза");
                                    otvet = b1.Name;
                                    break;
                                case 2:
                                    cplus2_1.Visibility = Visibility.Hidden;
                                    Met("Что такое рефакторинг?", "Процесс изменения внутренней структуры программы, не затрагивающий ее внешнего поведения", "Тоже самое, что и компиляция", "Удаление неиспользуемых классов, функций и переменных", "Сборка мусора");
                                    otvet = b1.Name;
                                    break;
                                case 3:
                                    Met("Что такое enum?", "Перечисление", "Целочисленный тип данных", "Такое не используется в C++", "Команда");
                                    otvet = b2.Name;
                                    break;
                                case 4:
                                    Met("Для чего нужен оператор continue в цикле?", "Позволяет сразу перейти в начало тела цикла", "Позволяет продолжить работу после применения оператора return", "Позволяет продолжить работу программы в случае возникновения ошибки или исключения", "Позволяет сразу перейти в конец тела цикла, пропуская весь код, который находится под ним");
                                    otvet = b4.Name;
                                    break;
                                case 5:
                                    Met("Что такое абстрактный метод?", "Метод класса, в котором присутствуют абстрактные поля", "Метод абстрактного класса", "Метод класса, в котором присутствует инкапсуляция", "Метод класса, реализация для которого отсутствует");
                                    otvet = b4.Name;
                                    break;
                                case 6:
                                    Met("Что нужно подключить для работы с файлами?", "Заголовочный файл fstream", "Библиотеку file", "Ничего, работа с файлами есть в стандартной библиотеке", "Библиотеку IO");
                                    otvet = b1.Name;
                                    break;
                                case 7:
                                    Met("Как еще называют логическое отрицание?", "Инверсия", "Дизъюнкция", "Импликация", "Инкрементация");
                                    otvet = b1.Name;
                                    break;
                                case 8:
                                    Met("Что такое flush?", "Чтение файла", "Освобождение буфера", "Запись в файл", "Заполнение буфера");
                                    otvet = b2.Name;
                                    break;
                                case 9:
                                    Met("Что означает ключевое слово override?", "Свойство, которое может принимать несколько значений", "Класс, который наследуется от абстрактного", "Виртуальный метод, который переопределяет виртуальный метод базового класса", "Тип данных");
                                    otvet = b3.Name;
                                    break;
                                case 10:
                                    Met("Как объявить список значений типа под названием num?", "list&lt;T&gt; num", "collection&lt;int&gt; num", "collection&lt;int&gt; num", "list&lt;int&gt; num");
                                    otvet = b4.Name;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case 3:  // Сложный уровень
                            switch (quas)
                            {
                                case 1: // вопросы
                                    cplus3_1.Visibility = Visibility.Visible;
                                    Met("Что выведет программа?", "d", "di", "i", "Не скомпилируется");
                                    otvet = b1.Name;
                                    break;
                                case 2:
                                    cplus3_2.Visibility = Visibility.Visible;
                                    cplus3_1.Visibility = Visibility.Hidden;
                                    Met("Какой контейнер типа Container нельзя передавать в\nфункцию EraseIf?", "std::unordered_set", "std::map", "std::list", "std::vector");
                                    otvet = b4.Name;
                                    break;
                                case 3:
                                    cplus3_3.Visibility = Visibility.Visible;
                                    cplus3_2.Visibility = Visibility.Hidden;
                                    Met("Что напечатает программа?", "Не скомпилируется", "0", "1", "Implementation defined");
                                    otvet = b3.Name;
                                    break;
                                case 4:
                                    cplus3_4.Visibility = Visibility.Visible;
                                    cplus3_3.Visibility = Visibility.Hidden;
                                    Met("Что выведет программа?", "021", "210", "201", "012");
                                    otvet = b2.Name;
                                    break;
                                case 5:
                                    cplus3_5.Visibility = Visibility.Visible;
                                    cplus3_4.Visibility = Visibility.Hidden;
                                    Met("Что выведет программа?", "Не скомпилируется", "0", "1", "Неопределенное поведение");
                                    otvet = b3.Name;
                                    break;
                                case 6:
                                    cplus3_6.Visibility = Visibility.Visible;
                                    cplus3_5.Visibility = Visibility.Hidden;
                                    Met("Укажите performance проблему с эти кодом из представленных\nвариантов.", "Размер вектора будет расти во время цикла, стоит сделать .reserve перед циклом", "Код оптимален, простых оптимизаций нет", "push_back сделает копию возвращаемых значений, надо использовать emplace_back", "Код содержит неопределенное поведение, т.к. ConvertData генерирует временный объект");
                                    otvet = b1.Name;
                                    break;
                                case 7:
                                    cplus3_7.Visibility = Visibility.Visible;
                                    cplus3_6.Visibility = Visibility.Hidden;
                                    Met("Код скомпилируется?", "Нет, вызывать std::move в capture нельзя", "Да, переменные в lambda неконстантные, поэтому их можно менять", "Да, так как только константные переменные могут изменяться, if contexpr не разрешит", "Нет, переменные в lambda иммутабельные, но decltype вернет, что тип мутабельный");
                                    otvet = b4.Name;
                                    break;
                                case 8:
                                    cplus3_8.Visibility = Visibility.Visible;
                                    cplus3_7.Visibility = Visibility.Hidden;
                                    Met("Какой контейнер типа Container нельзя передавать в функцию\nEraself?", "std::list", "std::vector", "std::map", "Все варианты верные");
                                    otvet = b4.Name;
                                    break;
                                case 9:
                                    cplus3_9.Visibility = Visibility.Visible;
                                    cplus3_8.Visibility = Visibility.Hidden;
                                    Met("Что выведет программа?", "A1", "B1", "A2", "B2");
                                    otvet = b2.Name;
                                    break;
                                case 10:
                                    cplus3_9.Visibility = Visibility.Hidden;
                                    Met("Что такое конъюнкция?", "Логическое сложение", "Логическое умножение", "Функция для обработки строк", "Вычитание строк");
                                    otvet = b2.Name;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        default:
                            break;
                    }
                    break;
                case 6: // C#
                    switch (AdminNumber.levv)
                    {
                        case 1:  // Начальный уровень
                            switch (quas)
                            {
                                case 1: // вопросы
                                    Questions.csh1 = ques.Text; Questions.csh1_1 = o1.Text; Questions.csh1_2 = o2.Text; Questions.csh1_3 = o3.Text; Questions.csh1_4 = o4.Text;
                                    otvet = b4.Name;
                                    break;
                                case 2:
                                    Questions.csh2 = ques.Text; Questions.csh2_1 = o1.Text; Questions.csh2_2 = o2.Text; Questions.csh2_3 = o3.Text; Questions.csh2_4 = o4.Text;
                                    otvet = b2.Name;
                                    break;
                                case 3:
                                    Questions.csh3 = ques.Text; Questions.csh3_1 = o1.Text; Questions.csh3_2 = o2.Text; Questions.csh3_3 = o3.Text; Questions.csh3_4 = o4.Text;
                                    otvet = b2.Name;
                                    break;
                                case 4:
                                    Questions.csh4 = ques.Text; Questions.csh4_1 = o1.Text; Questions.csh4_2 = o2.Text; Questions.csh4_3 = o3.Text; Questions.csh4_4 = o4.Text;
                                    otvet = b1.Name;
                                    break;
                                case 5:
                                    Questions.csh5 = ques.Text; Questions.csh5_1 = o1.Text; Questions.csh5_2 = o2.Text; Questions.csh5_3 = o3.Text; Questions.csh5_4 = o4.Text;
                                    otvet = b4.Name;
                                    break;
                                case 6:
                                    Questions.csh6 = ques.Text; Questions.csh6_1 = o1.Text; Questions.csh6_2 = o2.Text; Questions.csh6_3 = o3.Text; Questions.csh6_4 = o4.Text;
                                    otvet = b1.Name;
                                    break;
                                case 7:
                                    Questions.csh7 = ques.Text; Questions.csh7_1 = o1.Text; Questions.csh7_2 = o2.Text; Questions.csh7_3 = o3.Text; Questions.csh7_4 = o4.Text;
                                    otvet = b3.Name;
                                    break;
                                case 8:
                                    Questions.csh8 = ques.Text; Questions.csh8_1 = o1.Text; Questions.csh8_2 = o2.Text; Questions.csh8_3 = o3.Text; Questions.csh8_4 = o4.Text;
                                    otvet = b2.Name;
                                    break;
                                case 9:
                                    Questions.csh9 = ques.Text; Questions.csh9_1 = o1.Text; Questions.csh9_2 = o2.Text; Questions.csh9_3 = o3.Text; Questions.csh9_4 = o4.Text;
                                    otvet = b1.Name;
                                    break;
                                case 10:
                                    Questions.csh10 = ques.Text; Questions.csh10_1 = o1.Text; Questions.csh10_2 = o2.Text; Questions.csh10_3 = o3.Text; Questions.csh10_4 = o4.Text;
                                    otvet = b2.Name;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case 2: // Средний уровень
                            switch (quas)
                            {
                                case 1: // вопросы
                                    Questions.cshh1 = ques.Text; Questions.cshh1_1 = o1.Text; Questions.cshh1_2 = o2.Text; Questions.cshh1_3 = o3.Text; Questions.cshh1_4 = o4.Text;
                                    otvet = b2.Name;
                                    break;
                                case 2:
                                    Questions.cshh2 = ques.Text; Questions.cshh2_1 = o1.Text; Questions.cshh2_2 = o2.Text; Questions.cshh2_3 = o3.Text; Questions.cshh2_4 = o4.Text;
                                    otvet = b4.Name;
                                    break;
                                case 3:
                                    Questions.cshh3 = ques.Text; Questions.cshh3_1 = o1.Text; Questions.cshh3_2 = o2.Text; Questions.cshh3_3 = o3.Text; Questions.cshh3_4 = o4.Text;
                                    otvet = b3.Name;
                                    break;
                                case 4:
                                    Questions.cshh4 = ques.Text; Questions.cshh4_1 = o1.Text; Questions.cshh4_2 = o2.Text; Questions.cshh4_3 = o3.Text; Questions.cshh4_4 = o4.Text;
                                    otvet = b1.Name;
                                    break;
                                case 5:
                                    Questions.cshh5 = ques.Text; Questions.cshh5_1 = o1.Text; Questions.cshh5_2 = o2.Text; Questions.cshh5_3 = o3.Text; Questions.cshh5_4 = o4.Text;
                                    otvet = b2.Name;
                                    break;
                                case 6:
                                    Questions.cshh6 = ques.Text; Questions.cshh6_1 = o1.Text; Questions.cshh6_2 = o2.Text; Questions.cshh6_3 = o3.Text; Questions.cshh6_4 = o4.Text;
                                    otvet = b4.Name;
                                    break;
                                case 7:
                                    Questions.cshh7 = ques.Text; Questions.cshh7_1 = o1.Text; Questions.cshh7_2 = o2.Text; Questions.cshh7_3 = o3.Text; Questions.cshh7_4 = o4.Text;
                                    otvet = b1.Name;
                                    break;
                                case 8:
                                    Questions.cshh8 = ques.Text; Questions.cshh8_1 = o1.Text; Questions.cshh8_2 = o2.Text; Questions.cshh8_3 = o3.Text; Questions.cshh8_4 = o4.Text;
                                    otvet = b2.Name;
                                    break;
                                case 9:
                                    Questions.cshh9 = ques.Text; Questions.cshh9_1 = o1.Text; Questions.cshh9_2 = o2.Text; Questions.cshh9_3 = o3.Text; Questions.cshh9_4 = o4.Text;
                                    otvet = b4.Name;
                                    break;
                                case 10:
                                    Questions.cshh10 = ques.Text; Questions.cshh10_1 = o1.Text; Questions.cshh10_2 = o2.Text; Questions.cshh10_3 = o3.Text; Questions.cshh10_4 = o4.Text;
                                    otvet = b3.Name;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case 3:  // Сложный уровень
                            switch (quas)
                            {
                                case 1: // вопросы
                                    Questions.cshhh1 = ques.Text; Questions.cshhh1_1 = o1.Text; Questions.cshhh1_2 = o2.Text; Questions.cshhh1_3 = o3.Text; Questions.cshhh1_4 = o4.Text;
                                    otvet = b2.Name;
                                    break;
                                case 2:
                                    Questions.cshhh2 = ques.Text; Questions.cshhh2_1 = o1.Text; Questions.cshhh2_2 = o2.Text; Questions.cshhh2_3 = o3.Text; Questions.cshhh2_4 = o4.Text;
                                    otvet = b3.Name;
                                    break;
                                case 3:
                                    Questions.cshhh3 = ques.Text; Questions.cshhh3_1 = o1.Text; Questions.cshhh3_2 = o2.Text; Questions.cshhh3_3 = o3.Text; Questions.cshhh3_4 = o4.Text;
                                    otvet = b1.Name;
                                    break;
                                case 4:
                                    Questions.cshhh4 = ques.Text; Questions.cshhh4_1 = o1.Text; Questions.cshhh4_2 = o2.Text; Questions.cshhh4_3 = o3.Text; Questions.cshhh4_4 = o4.Text;
                                    otvet = b4.Name;
                                    break;
                                case 5:
                                    Questions.cshhh5 = ques.Text; Questions.cshhh5_1 = o1.Text; Questions.cshhh5_2 = o2.Text; Questions.cshhh5_3 = o3.Text; Questions.cshhh5_4 = o4.Text;
                                    otvet = b3.Name;
                                    break;
                                case 6:
                                    Questions.cshhh6 = ques.Text; Questions.cshhh6_1 = o1.Text; Questions.cshhh6_2 = o2.Text; Questions.cshhh6_3 = o3.Text; Questions.cshhh6_4 = o4.Text;
                                    otvet = b2.Name;
                                    break;
                                case 7:
                                    Questions.cshhh7 = ques.Text; Questions.cshhh7_1 = o1.Text; Questions.cshhh7_2 = o2.Text; Questions.cshhh7_3 = o3.Text; Questions.cshhh7_4 = o4.Text;
                                    otvet = b4.Name;
                                    break;
                                case 8:
                                    Questions.cshhh8 = ques.Text; Questions.cshhh8_1 = o1.Text; Questions.cshhh8_2 = o2.Text; Questions.cshhh8_3 = o3.Text; Questions.cshhh8_4 = o4.Text;
                                    otvet = b1.Name;
                                    break;
                                case 9:
                                    Questions.cshhh9 = ques.Text; Questions.cshhh9_1 = o1.Text; Questions.cshhh9_2 = o2.Text; Questions.cshhh9_3 = o3.Text; Questions.cshhh9_4 = o4.Text;
                                    otvet = b1.Name;
                                    break;
                                case 10:
                                    Questions.cshhh10 = ques.Text; Questions.cshhh10_1 = o1.Text; Questions.cshhh10_2 = o2.Text; Questions.cshhh10_3 = o3.Text; Questions.cshhh10_4 = o4.Text;
                                    otvet = b3.Name;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        default:
                            break;
                    }
                    break;
                case 7: // Java Script
                    switch (AdminNumber.levv)
                    {
                        case 1:  // Начальный уровень
                            switch (quas)
                            {
                                case 1: // вопросы
                                    Met("Как правильно объявить массив?", "var colors = \"red\", \"green\", \"blue\"", "var colors = 1 = (\"red\"), 2 = (\"green\"), 3 = (\"blue\")", "var colors = (1:\"red\", 2:\"green\", 3:\"blue\"", "var colors = [\"red\", \"green\", \"blue\"]");
                                    otvet = b4.Name;
                                    break;
                                case 2:
                                    Met("Какой из вариантов откроет новое окно?", "w2 = window.new(\"http://proghub.ru\");", "w2 = window.open(\"http://proghub.ru\")", "w2 = window.create(\"http://proghub.ru\")", "w2 = window.get(\"http://proghub.ru\")");
                                    otvet = b2.Name;
                                    break;
                                case 3:
                                    Met("JavaScript регистрозависимый язык?", "Да", "Нет", "Данное свойство можно изменить в настройках", "Зависит от среды разработки");
                                    otvet = b1.Name;
                                    break;
                                case 4:
                                    js1_4.Visibility = Visibility.Visible;
                                    Met("Что будет выведено в консоль?", "0", "1", "Error", "Ничего");
                                    otvet = b2.Name;
                                    break;
                                case 5:
                                    js1_4.Visibility = Visibility.Hidden;
                                    Met("Что такое ajax?", "Библиотека для асинхронной работы с данными", "Библиотека для асинхронной работы с DOM-элементами", "Группа связанных технологий, используемых для асинхронного отображения данных", "Библиотека для работы с потоками");
                                    otvet = b3.Name;
                                    break;
                                case 6:
                                    Met("Как объявить функцию в JS?", "def fn(){}", "set fn = function(){}", "public static function(){}", "function fn(){}");
                                    otvet = b4.Name;
                                    break;
                                case 7:
                                    Met("Как написать if в JavaScript?", "if i = 5 then", "if i = 5", "if (i == 5)", "if i == 5 then");
                                    otvet = b3.Name;
                                    break;
                                case 8:
                                    Met("Как правильно объявить цикл while?", "while (i <= 10)", "while (i <= 10; i++)", "while i = 1 to 10", "while i == 1 then");
                                    otvet = b1.Name;
                                    break;
                                case 9:
                                    js1_9.Visibility = Visibility.Visible;
                                    Met("Что выведет в консоль?", "Ничего не выведет, возникнет ошибка", "false", "true", "undefined");
                                    otvet = b2.Name;
                                    break;
                                case 10:
                                    js1_9.Visibility = Visibility.Hidden;
                                    Met("Что выведет следующий код?\nconsole.log(employeeId);\nvar employeeId = '19000';", "undefined", "some value", "TypeError", "ReferenceError: employeeId is not defined");
                                    otvet = b1.Name;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case 2: // Средний уровень
                            switch (quas)
                            {
                                case 1: // вопросы
                                    Met("Как сгенерировать случайное целое число от 1 до 10?", "Math.random(10)+1;", "Math.random() * 10+1;", "Math.floor(Math.random(10)+1);", "Math.floor((Math.random() * 10) + 1;");
                                    otvet = b4.Name;
                                    break;
                                case 2:
                                    js2_2.Visibility = Visibility.Visible;
                                    Met("Результатов выполнения следующего кода будет:", "31337", "ReferenceError", "TypeError", "SyntaxError");
                                    otvet = b3.Name;
                                    break;
                                case 3:
                                    js2_2.Visibility = Visibility.Hidden;
                                    js2_3.Visibility = Visibility.Visible;
                                    Met("Что выведет на экран следующий код?", "2", "3", "obj", "Произойдет ошибка");
                                    otvet = b1.Name;
                                    break;
                                case 4:
                                    js2_4.Visibility = Visibility.Visible;
                                    js2_3.Visibility = Visibility.Hidden;
                                    Met("Что выведет этот код?", "false false", "boolean false", "true false", "boolean true");
                                    otvet = b2.Name;
                                    break;
                                case 5:
                                    js2_5.Visibility = Visibility.Visible;
                                    js2_4.Visibility = Visibility.Hidden;
                                    Met("Что выведет код?", "{1, 2, 3, 4}", "{1, 1, 2, 3, 4}", "[1, 2, 3, 4]", "[1, 1, 2, 3, 4]");
                                    otvet = b1.Name;
                                    break;
                                case 6:
                                    js2_6.Visibility = Visibility.Visible;
                                    js2_5.Visibility = Visibility.Hidden;
                                    Met("Дан код. Какое значение будет выведено в консоли?", "true", "false", "1", "0");
                                    otvet = b4.Name;
                                    break;
                                case 7:
                                    js2_7.Visibility = Visibility.Visible;
                                    js2_6.Visibility = Visibility.Hidden;
                                    Met("Что выведет этот код?", "false true true false", "false false true false", "false true true true", "true true true false");
                                    otvet = b3.Name;
                                    break;
                                case 8:
                                    js2_8.Visibility = Visibility.Visible;
                                    js2_7.Visibility = Visibility.Hidden;
                                    Met("Что выведет следующий код?", "1", "5", "undefined", "null");
                                    otvet = b3.Name;
                                    break;
                                case 9:
                                    js2_8.Visibility = Visibility.Hidden;
                                    js2_9.Visibility = Visibility.Visible;
                                    Met("Каким будет результат выполнения данного кода?", "[\"mytest\"]", "[\"myteststring\"]", "[\"m,y,t,e,s,t,s,t,i,n,g\"]", "[\"m,y,t,e,s,t\"]");
                                    otvet = b2.Name;
                                    break;
                                case 10:
                                    js2_9.Visibility = Visibility.Hidden;
                                    Met("Что выведет Alert?\nalert( 0 / 0 );", "NaN", "Infinity", "Undefined", "Ничего");
                                    otvet = b1.Name;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case 3:  // Сложный уровень
                            switch (quas)
                            {
                                case 1: // вопросы
                                    Met("Какое значение будет на выходе?\nconsole.loog('${(x => x)('I love')} to program')", "I love to program", "undefined to program", "${(x => x)('I love') to program}", "TypeError");
                                    otvet = b1.Name;
                                    break;
                                case 2:
                                    js3_2.Visibility = Visibility.Visible;
                                    Met("Что выведет код?", "null \"\" true", "{} \"\" []", "null null true", "false null []");
                                    otvet = b2.Name;
                                    break;
                                case 3:
                                    js3_3.Visibility = Visibility.Visible;
                                    js3_2.Visibility = Visibility.Hidden;
                                    Met("Какие методы вернут значение Hello world! ?", "myMap.get('greeting')", "myMap.get(() => 'greeting')", "myMap.get(myFinc)", "Нет верного ответа");
                                    otvet = b3.Name;
                                    break;
                                case 4:
                                    js3_4.Visibility = Visibility.Visible;
                                    js3_3.Visibility = Visibility.Hidden;
                                    Met("Что выведет код?", "Прости, ты слишком молод для этого :(", "А ты достаточно стар для этого :)", "ReferenceError", "undefined");
                                    otvet = b3.Name;
                                    break;
                                case 5:
                                    js3_4.Visibility = Visibility.Hidden;
                                    Met("Что возвращает метод setInterval?\nset interval(() => console.log(\"Hi\"), 1000);", "Уникальный id", "Указанное количество миллисекунд", "Переданную функцию", "undefined");
                                    otvet = b1.Name;
                                    break;
                                case 6:
                                    js3_6.Visibility = Visibility.Visible;
                                    Met("Что выведет код?", "true", "false", "undefined", "TypeError");
                                    otvet = b4.Name;
                                    break;
                                case 7:
                                    js3_7.Visibility = Visibility.Visible;
                                    js3_6.Visibility = Visibility.Hidden;
                                    Met("Что выведет код?", "undefined", "{ name: \"taras\", age: 26 }", "Error", "taras, 26");
                                    otvet = b1.Name;
                                    break;
                                case 8:
                                    js3_8.Visibility = Visibility.Visible;
                                    js3_7.Visibility = Visibility.Hidden;
                                    Met("Что выведет код?", "{ constructor: ...} { constructor: ...}", "{ constructor: ...} undefined", "{} { constructor: ...}", "{ constructor: ...} {}");
                                    otvet = b2.Name;
                                    break;
                                case 9:
                                    js3_9.Visibility = Visibility.Visible;
                                    js3_8.Visibility = Visibility.Hidden;
                                    Met("Какой будет вывод?", "\"Ihor\"", "\"myName\"", "undefined", "ReferenceError");
                                    otvet = b4.Name;
                                    break;
                                case 10:
                                    js3_9.Visibility = Visibility.Hidden;
                                    Met("Чему равно значение?\nPromise.resolve(5)", "5", "Promise {<pending>: 5}", "Promise {<fullfilled>: 5}", "Error");
                                    otvet = b3.Name;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        default:
                            break;
                    }
                    break;
                case 8: // HTML
                    switch (AdminNumber.levv)
                    {
                        case 1:  // Начальный уровень
                            switch (quas)
                            {
                                case 1: // вопросы
                                    Met("Какой атрибут тега <ol> начинает нумерацию списка с определенного значения?", "type", "number", "start", "begin");
                                    otvet = b3.Name;
                                    break;
                                case 2:
                                    Met("Какой HTML-тэг используется для отображения на странице программного кода?", "programm", "script", "program", "code");
                                    otvet = b4.Name;
                                    break;
                                case 3:
                                    Met("Что означает тег <marquee>?", "Бегущая строка", "Маркированный текст", "Параграф", "Изображение-ссылка");
                                    otvet = b1.Name;
                                    break;
                                case 4:
                                    Met("Из предложенных вариантов указания заголовка в таблице, выберите правильный.", "<table><caption>title</caption>...</table>", "<table caption=\"titile\">...</table>", "<table title=\"title\">...</table>", "<table lable=\"title\">...</table>");
                                    otvet = b1.Name;
                                    break;
                                case 5:
                                    Met("Выберите html-код, создающий checkbox.", "<checkbox/>", "<input type=\"checkbox\"/>", "<check/>", "<input type=\"check\"/>");
                                    otvet = b2.Name;
                                    break;
                                case 6:
                                    Met("Какого значения не может быть у параметра align в теге <img>?", "bottom", "right", "left", "center");
                                    otvet = b4.Name;
                                    break;
                                case 7:
                                    Met("Выберите фрагмент HTML-кода, создающий ссылку со всплывающей подсказкой.", "<a title='подсказка'>текст ссылки</a>", "<a help='подсказка'>текст ссылки</a>", "<a baloon='подсказка'>текст ссылки</a>", "<a tip='подсказка'>текст ссылки</a>");
                                    otvet = b3.Name;
                                    break;
                                case 8:
                                    Met("Что определяет атрибут BORDER у элемента разметки TABLE?", "Определяет толщину рамки", "Определяет расположение таблицы в документе", "Управляет линиями, разделяющими ячейки таблицы", "Устанавливает цвет окантовки");
                                    otvet = b1.Name;
                                    break;
                                case 9:
                                    html1_9.Visibility = Visibility.Visible;
                                    Met("Является ли следующий HTML код валидным\nи well-formed?", "Да", "Только валидным", "Только well-formed", "Код не является валидным и well-formed");
                                    otvet = b3.Name;
                                    break;
                                case 10:
                                    html1_9.Visibility = Visibility.Hidden;
                                    Met("Какой тег предназначен для заголовков наименьшего размера?", "<h1>", "<h6>", "<hmin>", "<h7>");
                                    otvet = b2.Name;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case 2: // Средний уровень
                            switch (quas)
                            {
                                case 1: // вопросы..
                                    Met("С помощью какого тега следует разделять абзацы?", "<p>", "<br>", "<span>", "<b>");
                                    otvet = b1.Name;
                                    break;
                                case 2:
                                    Met("Какое число заголовков первого уровня считается допустимым?", "3", "2", "1", "Неограниченное количество");
                                    otvet = b3.Name;
                                    break;
                                case 3:
                                    Met("С помощью какого атрибута задается ширина поля textarea?", "width", "cols", "size", "rows");
                                    otvet = b2.Name;
                                    break;
                                case 4:
                                    Met("Каким является следующий адрес ссылки:\n/page2.html", "Полным", "Абсолютным", "Неполным", "Относительным");
                                    otvet = b4.Name;
                                    break;
                                case 5:
                                    Met("Как выделить текст курсивом?", "<hr>курсив</hr>", "<em>курсив</em>", "<br>курсив</br>", "<p>курсив</p>");
                                    otvet = b1.Name;
                                    break;
                                case 6:
                                    Met("С помощью какого атрибута можно задать текст для картинки, который будет отображен, если ее\nне удастся загрузить?", "title", "alt", "popup", "caption");
                                    otvet = b2.Name;
                                    break;
                                case 7:
                                    Met("С помощью какого свойства можно сделать отступы внутри ячейки в таблице?", "margin", "case", "space", "padding");
                                    otvet = b4.Name;
                                    break;
                                case 8:
                                    Met("Как сделать текст жирным?", "<strong>жирный</strong>", "<p>жирный</p>", "<a>жирный</a>", "<br>жирный</br>");
                                    otvet = b1.Name;
                                    break;
                                case 9:
                                    Met("Какой тег при создании страницы является необязательным?", "doctype", "head", "strong", "body");
                                    otvet = b3.Name;
                                    break;
                                case 10:
                                    Met("Каких тегов в HTML не существует?", "Парных", "Все есть", "Одиночных", "Тройных");
                                    otvet = b4.Name;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case 3:  // Сложный уровень
                            switch (quas)
                            {
                                case 1: // вопросы
                                    Met("Выберите теги для работы с таблицами.", "<table><tr><td>", "<table><head><tfoot>", "<table><tr><tt>", "<thead><body><tr>");
                                    otvet = b1.Name;
                                    break;
                                case 2:
                                    Met("Выберите тег для создания ненумерованного списка.", "<dl>", "<list>", "<ol>", "<ul>");
                                    otvet = b4.Name;
                                    break;
                                case 3:
                                    Met("Выберите верный способ создания выпадающего списка.", "<select>", "<list>", "<input type=\"dropdown\">", "<input type=\"list\">");
                                    otvet = b1.Name;
                                    break;
                                case 4:
                                    Met("Какое значение по умолчанию имеет атрибут method у лега <form>?", "POST", "Атрибут methodn является обязательным и не имеет значения по умолчанию", "GET", "Значение по умолчани зависит от браузера");
                                    otvet = b3.Name;
                                    break;
                                case 5:
                                    Met("Необходимо защитить текстовое поле формы от изменения значения пользователем.\nКакое из представленных фрагментов кода позволят решить поставленную задачу?", "<imput value=\"$999\" checked/>", "<input value=\"$999\" size=\"0\"/>", "<input value=\"#999\" readonly/>", "<input value=\"$999\" maxlength=\"0\"/>");
                                    otvet = b3.Name;
                                    break;
                                case 6:
                                    Met("Выберите верный способ создания текстового поля для ввода информации.", "<input type=\"textfield\">", "<textinput type=\"text\">", "<textfield>", "<input type=\"text\"");
                                    otvet = b4.Name;
                                    break;
                                case 7:
                                    Met("Выберите верный способ создания многострочного текстового поля.", "<input type=\"textbox\">", "<textarea>", "<input type=\"textarea\"", "<textarea=\"textbox\">");
                                    otvet = b2.Name;
                                    break;
                                case 8:
                                    Met("Выберите верный способ создания чекбокса.", "<checkbox>", "<input type=\"checkbox\">", "<input type=\"check\">", "<check>");
                                    otvet = b2.Name;
                                    break;
                                case 9:
                                    Met("Выберите верный способ вставки изображения?", "<img alt=\"MyImage\">image.gif", "<img src=\"image.gif\" alt=\"MyImage\">", "<img href=\"image.gif\" alt=\"MyImage\">", "<image src=\"image.gif\" alt=\"MyImage\">");
                                    otvet = b2.Name;
                                    break;
                                case 10:
                                    Met("Выберите верный способ установки фонового изображения страницы?", "<body background=\"background.gif\">", "<ing src=\"background.gif\" background>", "<background img=\"background.gif\">", "<img background=\"background.gif\">");
                                    otvet = b1.Name;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        default:
                            break;
                    }
                    break;
                case 9: // CSS
                    switch (AdminNumber.levv)
                    {
                        case 1:  // Начальный уровень
                            switch (quas)
                            {
                                case 1: // вопросы
                                    Met("Чтобы задать форму курсора, необходимо использовать CSS свойство:", "cursor", "pointer", "arrow", "Нельзя задать свою форму курсора в CSS");
                                    otvet = b1.Name;
                                    break;
                                case 2:
                                    Met("Чтобы задать прозрачность элемента на веб-странице необходимо использовать свойство:", "background-color", "opacity", "display", "position");
                                    otvet = b2.Name;
                                    break;
                                case 3:
                                    Met("Предназначение свойства clear в CSS:", "Очищает всю страницу, кроме блока к которому применяется данное свойство", "Устанавливает, с какой стороны элемента запрещено его обтекание другими элементами", "Очищает блок, к которому применяется данное свойство", "Очищает всю страницу");
                                    otvet = b2.Name;
                                    break;
                                case 4:
                                    Met("В чем отличие свойства outline от border:", "Здесь нет правильного ответа", "Между ними нет разницы", "Свойство outline не влияет на положение блока и его ширину", "Свойство border не влияет на положение блока и его ширину");
                                    otvet = b3.Name;
                                    break;
                                case 5:
                                    Met("При абсолютном позиционировании, чтобы изменить позицию элемента необходимо использовать\nсвойства:", "left, top, right и bottom", "При абсолютном позиционировании нельзя изменять положение элемента", "Только top и bottom", "Только left и right");
                                    otvet = b1.Name;
                                    break;
                                case 6:
                                    Met("За что отвечает свойство text-transform?", "Вращение текста по часовой стрелке", "Изменением цвета текста", "Динамическим изменением шрифта", "Преобразованием текста в заглавные и прописные символы");
                                    otvet = b4.Name;
                                    break;
                                case 7:
                                    Met("Для чего предназначено свойство padding в CSS?", "Фоновый цвет", "Рамка для контейнера", "Внутренний отступ", "Внешний отступ");
                                    otvet = b3.Name;
                                    break;
                                case 8:
                                    Met("Для чего предназначено свойство z-index:", "Определяет размер элемента", "Ни на что оно не влияет", "Обычное позиционирование влево-вправо, вверх-вниз", "Располагать элементы в определенном порядке, имитируя третье измерение, которое перпендикулярно экрану");
                                    otvet = b4.Name;
                                    break;
                                case 9:
                                    Met("Выберите правильное значение свойства position:", "absolute", "top", "bottom", "left");
                                    otvet = b1.Name;
                                    break;
                                case 10:
                                    Met("Какое CSS свойство используется для изменения стиля самой ссылки?", "a:hover", "a:visited", "a:link", "a:vlink");
                                    otvet = b3.Name;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case 2: // Средний уровень
                            switch (quas)
                            {
                                case 1: // вопросы
                                    Met("Какое из указанных значений свойства border-style, не существует?", "dashed", "dotted", "hiden", "Все свойства существуют");
                                    otvet = b3.Name;
                                    break;
                                case 2:
                                    Met("Сколько числовых значений может быть указано у свойства paddding?", "только одно", "до 4 включительно", "только два", "до 8 включительно");
                                    otvet = b2.Name;
                                    break;
                                case 3:
                                    css2_3.Visibility = Visibility.Visible;
                                    Met("Какой будет цвет у слова \"blah\"?", "Красный", "Фиолетовый", "Цвет по умолчанию", "Чёрный");
                                    otvet = b1.Name;
                                    break;
                                case 4:
                                    css2_3.Visibility = Visibility.Hidden;
                                    Met("Какую из операций не поддерживает функция calc()?", "+", "-", "*", "%");
                                    otvet = b4.Name;
                                    break;
                                case 5:
                                    Met("Выберите неправильный вариант указания размера текста.", "ek", "px", "pt", "проценты");
                                    otvet = b1.Name;
                                    break;
                                case 6:
                                    Met("Что необходимо написать в CSS чтобы изображение повторялось по вертикали:", "background-repeat: no-repeat;", "background-repeat: inherit;", "background-repeat: repeat-x;", "background-repeat: repeat-y;");
                                    otvet = b4.Name;
                                    break;
                                case 7:
                                    Met("Какой из вариантов задания цвета являются неверным?", "background-color: RGB(255, 0, 255);", "background-color: red;", "background-color:&hE99295;", "background-color:#ff00ff;");
                                    otvet = b3.Name;
                                    break;
                                case 8:
                                    Met("Выберите правильный вариант отступа сверху в 10 пикселей:", "margin: 0 10px 0 10px;", "Нет правильного ответа", "margin: 0 10px 10px 10px;", "margin: 10px 0 0 0;");
                                    otvet = b4.Name;
                                    break;
                                case 9:
                                    Met("Если для контейнера в CSS написать margin: 10px, то к чему будет применено данное свойство?", "К каждой из сторон контейнера", "К трем сторонам на усмотрение браузера", "Только к правой и левой стороне контейнера", "Только к верхней и нижней стороне контейнера");
                                    otvet = b1.Name;
                                    break;
                                case 10:
                                    Met("Какое CSS свойство используется для изменения стиля самой ссылки?", "a:hover", "a:visited", "a:link", "a:vlink");
                                    otvet = b3.Name;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case 3:  // Сложный уровень
                            switch (quas)
                            {
                                case 1: // вопросы
                                    Met("Какая величина в медиа-запросах позволяет применять стили при определенном сотношении сторон\nна устройстве отображения?", "device-aspect-ratio", "min-orientation", "orientation", "min-device-width");
                                    otvet = b1.Name;
                                    break;
                                case 2:
                                    Met("При задании свойств padding в процентах, относительно чего считаются эти проценты?", "padding родительского элемента", "Ширины самого элемента", "margin родительского элемента", "Ширины родительского элемента");
                                    otvet = b4.Name;
                                    break;
                                case 3:
                                    Met("Какое из значений свойств display делает HTML-элемент строчно-блочным?", "inline", "inline-block", "block", "list-item");
                                    otvet = b2.Name;
                                    break;
                                case 4:
                                    Met("Какой порядок следования отступов у свойства padding: 10px 20px 30px 40px", "сверха, слева, снизу, справа", "справа, снизу, слева, сверху", "сверха, слева, справа, снизу", "сверха, справа, снизу, слева");
                                    otvet = b4.Name;
                                    break;
                                case 5:
                                    Met("Какое CSS свойство используется для изменения стиля уже кликнутой ссылки?", "a:hover", "a:vlink", "a:visited", "a:link");
                                    otvet = b3.Name;
                                    break;
                                case 6:
                                    Met("Какой из стилей подключения css верен?", "@import css(“styles.css”)", "<link href=”styles.css” type=”stylesheet”>", "@import url(“styles.css”)", "<style href=”styles.css” />");
                                    otvet = b3.Name;
                                    break;
                                case 7:
                                    Met("Какой из селекторов выберет все HTML-элементы div с атрибутом title содержащим внутри значения\nподслово «bar»?", "div[title*=\"bar\"]", "div[title|=\"bar\"]", "div[title^=\"bar\"]", "div[title~=\"bar\"]");
                                    otvet = b1.Name;
                                    break;
                                case 8:
                                    css3_8.Visibility = Visibility.Visible;
                                    Met("Какой будет цвет у слова \"blah\"?", "Фиолетовый", "Цвет по умолчанию", "Чёрный", "Красный");
                                    otvet = b2.Name;
                                    break;
                                case 9:
                                    css3_8.Visibility = Visibility.Hidden;
                                    Met("Селектор «.some .next span» выберет для оформления HTML-элемент span в следующих фрагментах\nразметки", "<div class=\"some next\"> <div><span>текст</span></div> </div>", "<div class=\"some\"> <div class=\"next\"><span>текст</span></div> </div>", "<div class=\"some next\"> <div><p class=\"next\"><span>текст</span></p></div> </div>", "<nav class=\"some\"><a href=\"#\" class=\"next\"><span></span></a></nav>");
                                    otvet = b1.Name;
                                    break;
                                case 10:
                                    Met("Какое CSS свойство используется для определения стиля при наведении на ссылку курсора мыши,\nно при этом элемент еще не активирован?", "a:hover", "a:visited", "a:vlink", "a:link");
                                    otvet = b1.Name;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }

            MainWindow W1 = new MainWindow();
            W1.Show();
            this.Close();
            save.Visibility = Visibility.Hidden;
        }

        private void button_get_Click(object sender, RoutedEventArgs e)
        {
            save.Visibility = Visibility.Visible;
        }
    }
}
