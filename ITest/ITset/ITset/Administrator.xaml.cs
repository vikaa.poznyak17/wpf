﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ITset
{
    /// <summary>
    /// Логика взаимодействия для Administrator.xaml
    /// </summary>
    class Bd2
    {
        public string git1 { get; set; }
        public string git2 { get; set; }
        public string git3 { get; set; }
        public string sql1 { get; set; }
        public string sql2 { get; set; }
        public string sql3 { get; set; }
        public string java1 { get; set; }
        public string java2 { get; set; }
        public string java3 { get; set; }
        public string python1 { get; set; }
        public string python2 { get; set; }
        public string python3 { get; set; }
        public string c1 { get; set; }
        public string c2 { get; set; }
        public string c3 { get; set; }
        public string cplus1 { get; set; }
        public string cplus2 { get; set; }
        public string cplus3 { get; set; }
        public string js1 { get; set; }
        public string js2 { get; set; }
        public string js3 { get; set; }
        public string html1 { get; set; }
        public string html2 { get; set; }
        public string html3 { get; set; }
        public string css1 { get; set; }
        public string css2 { get; set; }
        public string css3 { get; set; }

        public Bd2()
        {
            git1 = git1;
            git2 = git2;
            git3 = git3;

            sql1 = sql1;
            sql2 = sql2;
            sql3 = sql3;

            java1 = java1;
            java2 = java2;
            java3 = java3;

            python1 = python1;
            python2 = python2;
            python3 = python3;

            c1 = c1;
            c2 = c2;
            c3 = c3;

            cplus1 = cplus1;
            cplus2 = cplus2;
            cplus3 = cplus3;

            js1 = js1;
            js2 = js2;
            js3 = js3;

            html1 = html1;
            html2 = html2;
            html3 = html3;

            css1 = css1;
            css2 = css2;
            css3 = css3;
        }

    }
    public partial class Administrator : Window
    {
        public Administrator()
        {
            InitializeComponent();
        }

        private void git_Click(object sender, RoutedEventArgs e)
        {
            AdminLevel W1 = new AdminLevel();
            Button but = (Button)sender;
            switch (but.Name)
            {
                case "git":
                    AdminLevel.numm = 1;
                    break;
                case "sql":
                    AdminLevel.numm = 2;
                    break;
                case "java":
                    AdminLevel.numm = 3;
                    break;
                case "python":
                    AdminLevel.numm = 4;
                    break;
                case "C":
                    AdminLevel.numm = 5;
                    break;
                case "C2":
                    AdminLevel.numm = 6;
                    break;
                case "JS":
                    AdminLevel.numm = 7;
                    break;
                case "Html":
                    AdminLevel.numm = 8;
                    break;
                case "Css":
                    AdminLevel.numm = 9;
                    break;
                default:
                    break;
            }

            W1.Show();
            this.Close();
        }

        private void button_back_Click(object sender, RoutedEventArgs e) // сохранение
        {
            Bd2[] bd = new Bd2[1];
            bd[0] = new Bd2();
            using (var connection = new SqlConnection("Data Source=DESKTOP-U99NK91\\SQLEPRESS; Initial Catalog = ТестыЯзыкиПрогр; User ID = sa; Password = 123456")) // чтение данных из бд
            {
                connection.Open();
                using (var cmd = new SqlCommand("SELECT Git1 FROM [ТестыЯзыкиПрогр].[dbo].[Языки]", connection))
                {
                    using (var rd = cmd.ExecuteReader())
                    {
                        if (rd.Read())
                        {
                            bd[0].git1 = rd.GetValue(0).ToString();
                        }
                    }
                }
                using (var cmd = new SqlCommand("SELECT Git2 FROM [ТестыЯзыкиПрогр].[dbo].[Языки]", connection))
                {
                    using (var rd = cmd.ExecuteReader())
                    {
                        if (rd.Read())
                        {
                            bd[0].git2 = rd.GetValue(0).ToString();
                        }
                    }
                }
                using (var cmd = new SqlCommand("SELECT Git3 FROM [ТестыЯзыкиПрогр].[dbo].[Языки]", connection))
                {
                    using (var rd = cmd.ExecuteReader())
                    {
                        if (rd.Read())
                        {
                            bd[0].git3 = rd.GetValue(0).ToString();
                        }
                    }
                }
                using (var cmd = new SqlCommand("SELECT SQ1 FROM [ТестыЯзыкиПрогр].[dbo].[Языки]", connection))
                {
                    using (var rd = cmd.ExecuteReader())
                    {
                        if (rd.Read())
                        {
                            bd[0].sql1 = rd.GetValue(0).ToString();
                        }
                    }
                }
                using (var cmd = new SqlCommand("SELECT SQ2 FROM [ТестыЯзыкиПрогр].[dbo].[Языки]", connection))
                {
                    using (var rd = cmd.ExecuteReader())
                    {
                        if (rd.Read())
                        {
                            bd[0].sql2 = rd.GetValue(0).ToString();
                        }
                    }
                }
                using (var cmd = new SqlCommand("SELECT SQ3 FROM [ТестыЯзыкиПрогр].[dbo].[Языки]", connection))
                {
                    using (var rd = cmd.ExecuteReader())
                    {
                        if (rd.Read())
                        {
                            bd[0].sql3 = rd.GetValue(0).ToString();
                        }
                    }
                }
                using (var cmd = new SqlCommand("SELECT Java1 FROM [ТестыЯзыкиПрогр].[dbo].[Языки]", connection))
                {
                    using (var rd = cmd.ExecuteReader())
                    {
                        if (rd.Read())
                        {
                            bd[0].java1 = rd.GetValue(0).ToString();
                        }
                    }
                }
                using (var cmd = new SqlCommand("SELECT Java2 FROM [ТестыЯзыкиПрогр].[dbo].[Языки]", connection))
                {
                    using (var rd = cmd.ExecuteReader())
                    {
                        if (rd.Read())
                        {
                            bd[0].java2 = rd.GetValue(0).ToString();
                        }
                    }
                }
                using (var cmd = new SqlCommand("SELECT Java3 FROM [ТестыЯзыкиПрогр].[dbo].[Языки]", connection))
                {
                    using (var rd = cmd.ExecuteReader())
                    {
                        if (rd.Read())
                        {
                            bd[0].java3 = rd.GetValue(0).ToString();
                        }
                    }
                }
                using (var cmd = new SqlCommand("SELECT Python1 FROM [ТестыЯзыкиПрогр].[dbo].[Языки]", connection))
                {
                    using (var rd = cmd.ExecuteReader())
                    {
                        if (rd.Read())
                        {
                            bd[0].python1 = rd.GetValue(0).ToString();
                        }
                    }
                }
                using (var cmd = new SqlCommand("SELECT Python2 FROM [ТестыЯзыкиПрогр].[dbo].[Языки]", connection))
                {
                    using (var rd = cmd.ExecuteReader())
                    {
                        if (rd.Read())
                        {
                            bd[0].python2 = rd.GetValue(0).ToString();
                        }
                    }
                }
                using (var cmd = new SqlCommand("SELECT Python3 FROM [ТестыЯзыкиПрогр].[dbo].[Языки]", connection))
                {
                    using (var rd = cmd.ExecuteReader())
                    {
                        if (rd.Read())
                        {
                            bd[0].python3 = rd.GetValue(0).ToString();
                        }
                    }
                }
                using (var cmd = new SqlCommand("SELECT C#1 FROM [ТестыЯзыкиПрогр].[dbo].[Языки]", connection))
                {
                    using (var rd = cmd.ExecuteReader())
                    {
                        if (rd.Read())
                        {
                            bd[0].c1 = rd.GetValue(0).ToString();
                        }
                    }
                }
                using (var cmd = new SqlCommand("SELECT C#2 FROM [ТестыЯзыкиПрогр].[dbo].[Языки]", connection))
                {
                    using (var rd = cmd.ExecuteReader())
                    {
                        if (rd.Read())
                        {
                            bd[0].c2 = rd.GetValue(0).ToString();
                        }
                    }
                }
                using (var cmd = new SqlCommand("SELECT C#3 FROM [ТестыЯзыкиПрогр].[dbo].[Языки]", connection))
                {
                    using (var rd = cmd.ExecuteReader())
                    {
                        if (rd.Read())
                        {
                            bd[0].c3 = rd.GetValue(0).ToString();
                        }
                    }
                }
                using (var cmd = new SqlCommand("SELECT Cplus1 FROM [ТестыЯзыкиПрогр].[dbo].[Языки]", connection))
                {
                    using (var rd = cmd.ExecuteReader())
                    {
                        if (rd.Read())
                        {
                            bd[0].cplus1 = rd.GetValue(0).ToString();
                        }
                    }
                }
                using (var cmd = new SqlCommand("SELECT Cplus2 FROM [ТестыЯзыкиПрогр].[dbo].[Языки]", connection))
                {
                    using (var rd = cmd.ExecuteReader())
                    {
                        if (rd.Read())
                        {
                            bd[0].cplus2 = rd.GetValue(0).ToString();
                        }
                    }
                }
                using (var cmd = new SqlCommand("SELECT Cplus3 FROM [ТестыЯзыкиПрогр].[dbo].[Языки]", connection))
                {
                    using (var rd = cmd.ExecuteReader())
                    {
                        if (rd.Read())
                        {
                            bd[0].cplus3 = rd.GetValue(0).ToString();
                        }
                    }
                }
                using (var cmd = new SqlCommand("SELECT JS1 FROM [ТестыЯзыкиПрогр].[dbo].[Языки]", connection))
                {
                    using (var rd = cmd.ExecuteReader())
                    {
                        if (rd.Read())
                        {
                            bd[0].js1 = rd.GetValue(0).ToString();
                        }
                    }
                }
                using (var cmd = new SqlCommand("SELECT JS2 FROM [ТестыЯзыкиПрогр].[dbo].[Языки]", connection))
                {
                    using (var rd = cmd.ExecuteReader())
                    {
                        if (rd.Read())
                        {
                            bd[0].js2 = rd.GetValue(0).ToString();
                        }
                    }
                }
                using (var cmd = new SqlCommand("SELECT JS3 FROM [ТестыЯзыкиПрогр].[dbo].[Языки]", connection))
                {
                    using (var rd = cmd.ExecuteReader())
                    {
                        if (rd.Read())
                        {
                            bd[0].js3 = rd.GetValue(0).ToString();
                        }
                    }
                }
                using (var cmd = new SqlCommand("SELECT HTML1 FROM [ТестыЯзыкиПрогр].[dbo].[Языки]", connection))
                {
                    using (var rd = cmd.ExecuteReader())
                    {
                        if (rd.Read())
                        {
                            bd[0].html1 = rd.GetValue(0).ToString();
                        }
                    }
                }
                using (var cmd = new SqlCommand("SELECT HTML2 FROM [ТестыЯзыкиПрогр].[dbo].[Языки]", connection))
                {
                    using (var rd = cmd.ExecuteReader())
                    {
                        if (rd.Read())
                        {
                            bd[0].html2 = rd.GetValue(0).ToString();
                        }
                    }
                }
                using (var cmd = new SqlCommand("SELECT HTML3 FROM [ТестыЯзыкиПрогр].[dbo].[Языки]", connection))
                {
                    using (var rd = cmd.ExecuteReader())
                    {
                        if (rd.Read())
                        {
                            bd[0].html3 = rd.GetValue(0).ToString();
                        }
                    }
                }
                using (var cmd = new SqlCommand("SELECT CSS1 FROM [ТестыЯзыкиПрогр].[dbo].[Языки]", connection))
                {
                    using (var rd = cmd.ExecuteReader())
                    {
                        if (rd.Read())
                        {
                            bd[0].css1 = rd.GetValue(0).ToString();
                        }
                    }
                }
                using (var cmd = new SqlCommand("SELECT CSS2 FROM [ТестыЯзыкиПрогр].[dbo].[Языки]", connection))
                {
                    using (var rd = cmd.ExecuteReader())
                    {
                        if (rd.Read())
                        {
                            bd[0].css2 = rd.GetValue(0).ToString();
                        }
                    }
                }
                using (var cmd = new SqlCommand("SELECT CSS3 FROM [ТестыЯзыкиПрогр].[dbo].[Языки]", connection))
                {
                    using (var rd = cmd.ExecuteReader())
                    {
                        if (rd.Read())
                        {
                            bd[0].css3 = rd.GetValue(0).ToString();
                        }
                    }
                }
            }

            string text = "Git" + "\nпростой уровень " + bd[0].git1 + "\nсредний уровень " + bd[0].git2 + "\nсложный уровень " + bd[0].git3
                + "\nSQL" + "\nпростой уровень " + bd[0].sql1 + "\nсредний уровень " + bd[0].sql2 + "\nсложный уровень" + bd[0].sql3
                + "\nJava" + "\nпростой уровень " + bd[0].java1 + "\nсредний уровень " + bd[0].java2 + "\nсложный уровень" + bd[0].java3
                + "\nPython" + "\nпростой уровень " + bd[0].python1 + "\nсредний уровень " + bd[0].python2 + "\nсложный уровень" + bd[0].python3
                + "\nC++" + "\nпростой уровень " + bd[0].cplus1 + "\nсредний уровень " + bd[0].cplus2 + "\nсложный уровень" + bd[0].cplus3
                + "\nC#" + "\nпростой уровень " + bd[0].c1 + "\nсредний уровень " + bd[0].c2 + "\nсложный уровень" + bd[0].c3
                + "\nJava Script" + "\nпростой уровень " + bd[0].js1 + "\nсредний уровень " + bd[0].js2 + "\nсложный уровень" + bd[0].js3
                + "\nHTML" + "\nпростой уровень " + bd[0].html1 + "\nсредний уровень " + bd[0].html2 + "\nсложный уровень" + bd[0].html3
                + "\nCSS" + "\nпростой уровень " + bd[0].css1 + "\nсредний уровень " + bd[0].css1 + "\nсложный уровень" + bd[0].css1;

            // запись в файл
            using (FileStream fstream = new FileStream(@"C:\Users\37533\Desktop\ТЕСТЫ ПРОГЕРСТВО ПРАКТИКА\ITest\ITset\ITset\bin\Debug\note.txt", FileMode.OpenOrCreate))
            {
                // преобразуем строку в байты
                byte[] array = System.Text.Encoding.Default.GetBytes(text);
                // запись массива байтов в файл
                fstream.Write(array, 0, array.Length);
            }
        }

        private void button_backk_Click(object sender, RoutedEventArgs e)
        {
            AdminWindow W1 = new AdminWindow();
            W1.Show();
            this.Close();
        }

        private void button_spr_Click(object sender, RoutedEventArgs e)
        {
            SpravkaWindow W1 = new SpravkaWindow();
            W1.Show();
            this.Close();
        }
    }
}
