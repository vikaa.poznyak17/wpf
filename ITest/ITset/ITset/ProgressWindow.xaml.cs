﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ITset
{
    /// <summary>
    /// Логика взаимодействия для ProgressWindow.xaml
    /// </summary>
    public partial class ProgressWindow : Window
    {
        public ProgressWindow()
        {
            InitializeComponent();
        }
        private void button_back_Click(object sender, RoutedEventArgs e)
        {
            MainWindow W1 = new MainWindow();
            W1.Show();
            this.Close();
        }

        private void git2_Click(object sender, RoutedEventArgs e)
        {
            ProgressLanguage W1 = new ProgressLanguage();
            Button but2 = (Button)sender;
            switch (but2.Name)
            {
                case "git2":
                    ProgressLanguage.num2 = 1;
                    break;
                case "sql2":
                    ProgressLanguage.num2 = 2;
                    break;
                case "java2":
                    ProgressLanguage.num2 = 3;
                    break;
                case "python2":
                    ProgressLanguage.num2 = 4;
                    break;
                case "Cpl":
                    ProgressLanguage.num2 = 5;
                    break;
                case "C2sh":
                    ProgressLanguage.num2 = 6;
                    break;
                case "JS2":
                    ProgressLanguage.num2 = 7;
                    break;
                case "Html2":
                    ProgressLanguage.num2 = 8;
                    break;
                case "Css2":
                    ProgressLanguage.num2 = 9;
                    break;
                default:
                    break;
            }

            W1.Show();
            this.Close();
        }
    }
}
