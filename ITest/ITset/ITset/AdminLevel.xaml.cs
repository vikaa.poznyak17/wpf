﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ITset
{
    /// <summary>
    /// Логика взаимодействия для AdminLevel.xaml
    /// </summary>
    public partial class AdminLevel : Window
    {
        public static int numm;

        public AdminLevel()
        {
            InitializeComponent();
        }

        private void level_Click(object sender, RoutedEventArgs e)
        {
            AdminNumber W1 = new AdminNumber();
            Button but2 = (Button)sender;
            switch (but2.Name)
            {
                case "EasyLevel":
                    AdminNumber.levv = 1;
                    break;
                case "MiddleLevel":
                    AdminNumber.levv = 2;
                    break;
                case "HardLevel":
                    AdminNumber.levv = 3;
                    break;
                default:
                    break;
            }

            W1.Show();
            this.Close();
        }

        private void button_back_Click(object sender, RoutedEventArgs e)
        {
            Administrator W1 = new Administrator();
            W1.Show();
            this.Close();
        }
    }
}
