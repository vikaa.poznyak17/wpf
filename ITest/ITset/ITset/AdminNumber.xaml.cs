﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ITset
{
    /// <summary>
    /// Логика взаимодействия для AdminNumber.xaml
    /// </summary>
    public partial class AdminNumber : Window
    {
        public static int levv;
        public AdminNumber()
        {
            InitializeComponent();
        }

        private void git_Click(object sender, RoutedEventArgs e)
        {
            AdminQuestions W1 = new AdminQuestions();
            Button but = (Button)sender;
            switch (but.Name)
            {
                case "v1":
                    AdminQuestions.quas = 1;
                    break;
                case "v2":
                    AdminQuestions.quas = 2;
                    break;
                case "v3":
                    AdminQuestions.quas = 3;
                    break;
                case "v4":
                    AdminQuestions.quas = 4;
                    break;
                case "v5":
                    AdminQuestions.quas = 5;
                    break;
                case "v6":
                    AdminQuestions.quas = 6;
                    break;
                case "v7":
                    AdminQuestions.quas = 7;
                    break;
                case "v8":
                    AdminQuestions.quas = 8;
                    break;
                case "v9":
                    AdminQuestions.quas = 9;
                    break;
                case "v10":
                    AdminQuestions.quas = 10;
                    break;
                default:
                    break;
            }

            W1.Show();
            this.Close();
        }

        private void button_back_Click(object sender, RoutedEventArgs e)
        {
            AdminLevel W1 = new AdminLevel();
            W1.Show();
            this.Close();
        }
    }
}
