﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ITset
{
    /// <summary>
    /// Логика взаимодействия для StartWindow.xaml
    /// </summary>
    public partial class StartWindow : Window
    {
        public StartWindow()
        {
            InitializeComponent();
        }

        private void button_back_Click(object sender, RoutedEventArgs e)
        {
            MainWindow W1 = new MainWindow();
            W1.Show();
            this.Close();
        }

        private void git_Click(object sender, RoutedEventArgs e)
        {
            DifficultyLevels W1 = new DifficultyLevels();
            Button but = (Button)sender;
            switch(but.Name)
            {
                case "git":
                    DifficultyLevels.num = 1;
                    break;
                case "sql":
                    DifficultyLevels.num = 2;
                    break;
                case "java":
                    DifficultyLevels.num = 3;
                    break;
                case "python":
                    DifficultyLevels.num = 4;
                    break;
                case "C":
                    DifficultyLevels.num = 5;
                    break;
                case "C2":
                    DifficultyLevels.num = 6;
                    break;
                case "JS":
                    DifficultyLevels.num = 7;
                    break;
                case "Html":
                    DifficultyLevels.num = 8;
                    break;
                case "Css":
                    DifficultyLevels.num = 9;
                    break;
                default:
                    break;
            }

            W1.Show();
            this.Close();
        }
    }
}
