﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ITset
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void button_info_Click(object sender, RoutedEventArgs e)
        {
            InfoWindow W1 = new InfoWindow();
            W1.Show();
            this.Close();
        }

        private void button_start_Click(object sender, RoutedEventArgs e)
        {
            StartWindow W1 = new StartWindow();
            W1.Show();
            this.Close();
        }

        private void button_progress_Click(object sender, RoutedEventArgs e)
        {
            ProgressWindow W1 = new ProgressWindow();
            W1.Show();
            this.Close();
        }

        private void button_admin_Click(object sender, RoutedEventArgs e)
        {
            AdminWindow W1 = new AdminWindow();
            W1.Show();
            this.Close();
        }
    }
}
