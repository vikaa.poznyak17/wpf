﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ITset
{
    /// <summary>
    /// Логика взаимодействия для ResultWindow.xaml
    /// </summary>
    public partial class ResultWindow : Window
    {
        public ResultWindow()
        {
            InitializeComponent();
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            switch(Questions.res)
            {
                case 0:
                    resu.Content = "0 из 10";
                    break;
                case 1:
                    resu.Content = "1 из 10";
                    break;
                case 2:
                    resu.Content = "2 из 10";
                    break;
                case 3:
                    resu.Content = "3 из 10";
                    break;
                case 4:
                    resu.Content = "4 из 10";
                    break;
                case 5:
                    resu.Content = "5 из 10";
                    break;
                case 6:
                    resu.Content = "6 из 10";
                    break;
                case 7:
                    resu.Content = "7 из 10";
                    break;
                case 8:
                    resu.Content = "8 из 10";
                    break;
                case 9:
                    resu.Content = "9 из 10";
                    break;
                case 10:
                    resu.Content = "10 из 10";
                    break;
                default:
                    break;
            }

            if (Questions.res >= 0 && Questions.res <= 4)
            {
                labell.Content = "Ой... Попробуйте еще раз!";
            }
            else if (Questions.res > 4 && Questions.res < 8)
            {
                labell.Content = "Неплохо, но могло быть и лучше!";
            }
            else if (Questions.res >= 8 && Questions.res <= 9)
            {
                labell.Content = "Еще бы чуть-чуть и отлично!";
            }
            else if (Questions.res == 10)
            {
                labell.Content = "Замечательный результат!";
            }

            string str = string.Empty;
            if (DifficultyLevels.num == 1 && Questions.lev == 1)
            {
                str = "Git1";
            }
            else if (DifficultyLevels.num == 1 && Questions.lev == 2)
            {
                str = "Git2";
            }
            else if (DifficultyLevels.num == 1 && Questions.lev == 3)
            {
                str = "Git3";
            }
            else if (DifficultyLevels.num == 2 && Questions.lev == 1)
            {
                str = "SQ1";
            }
            else if (DifficultyLevels.num == 2 && Questions.lev == 2)
            {
                str = "SQ2";
            }
            else if (DifficultyLevels.num == 2 && Questions.lev == 3)
            {
                str = "SQ3";
            }
            else if (DifficultyLevels.num == 3 && Questions.lev == 1)
            {
                str = "Java1";
            }
            else if (DifficultyLevels.num == 3 && Questions.lev == 2)
            {
                str = "Java2";
            }
            else if (DifficultyLevels.num == 3 && Questions.lev == 3)
            {
                str = "Java3";
            }
            else if (DifficultyLevels.num == 4 && Questions.lev == 1)
            {
                str = "Python1";
            }
            else if (DifficultyLevels.num == 4 && Questions.lev == 2)
            {
                str = "Python2";
            }
            else if (DifficultyLevels.num == 4 && Questions.lev == 3)
            {
                str = "Python3";
            }
            else if (DifficultyLevels.num == 6 && Questions.lev == 1)
            {
                str = "C#1";
            }
            else if (DifficultyLevels.num == 6 && Questions.lev == 2)
            {
                str = "C#2";
            }
            else if (DifficultyLevels.num == 6 && Questions.lev == 3)
            {
                str = "C#3";
            }
            else if (DifficultyLevels.num == 5 && Questions.lev == 1)
            {
                str = "Cplus1";
            }
            else if (DifficultyLevels.num == 5 && Questions.lev == 2)
            {
                str = "Cplus2";
            }
            else if (DifficultyLevels.num == 5 && Questions.lev == 3)
            {
                str = "Cplus3";
            }
            else if (DifficultyLevels.num == 7 && Questions.lev == 1)
            {
                str = "JS1";
            }
            else if (DifficultyLevels.num == 7 && Questions.lev == 2)
            {
                str = "JS2";
            }
            else if (DifficultyLevels.num == 7 && Questions.lev == 3)
            {
                str = "JS3";
            }
            else if (DifficultyLevels.num == 8 && Questions.lev == 1)
            {
                str = "HTML1";
            }
            else if (DifficultyLevels.num == 8 && Questions.lev == 2)
            {
                str = "HTML2";
            }
            else if (DifficultyLevels.num == 8 && Questions.lev == 3)
            {
                str = "HTML3";
            }
            else if (DifficultyLevels.num == 9 && Questions.lev == 1)
            {
                str = "CSS1";
            }
            else if (DifficultyLevels.num == 9 && Questions.lev == 2)
            {
                str = "CSS2";
            }
            else if (DifficultyLevels.num == 9 && Questions.lev == 3)
            {
                str = "CSS3";
            }


            string connectionString = "Data Source=DESKTOP-U99NK91\\SQLEPRESS; Initial Catalog = ТестыЯзыкиПрогр; User ID = sa; Password = 123456";
            string sqlExpression = String.Format("UPDATE Языки SET {0}='{1}'", str, Questions.res);
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(sqlExpression, connection);
                var numbers = command.ExecuteReader();
            }
        }
        private void button_back_Click(object sender, RoutedEventArgs e)
        {
            MainWindow W1 = new MainWindow();
            W1.Show();
            this.Close();
        }
    }
}
