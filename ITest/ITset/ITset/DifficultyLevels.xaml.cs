﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ITset
{
    /// <summary>
    /// Логика взаимодействия для DifficultyLevels.xaml
    /// </summary>
    public partial class DifficultyLevels : Window
    {
        public static int num;
        public DifficultyLevels()
        {
            InitializeComponent();
        }

        private void button_back_Click(object sender, RoutedEventArgs e)
        {
            StartWindow W1 = new StartWindow();
            W1.Show();
            this.Close();
        }

        private void level_Click(object sender, RoutedEventArgs e)
        {
            Questions W1 = new Questions();
            Button but2 = (Button)sender;
            switch (but2.Name)
            {
                case "EasyLevel":
                    Questions.lev = 1;
                    break;
                case "MiddleLevel":
                    Questions.lev = 2;
                    break;
                case "HardLevel":
                    Questions.lev = 3;
                    break;
                default:
                    break;
            }

            W1.Show();
            this.Close();
        }
    }
}
