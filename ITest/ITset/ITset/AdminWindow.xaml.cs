﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ITset
{
    /// <summary>
    /// Логика взаимодействия для AdminWindow.xaml
    /// </summary>
    public partial class AdminWindow : Window
    {
        public AdminWindow()
        {
            InitializeComponent();
        }

        private void button_go_Click(object sender, RoutedEventArgs e)
        {
            nepr.Visibility = Visibility.Hidden;
            string text = textBox1.Text;
            string subString = textBox2.Text;
            if ((text == "vika poznyak"  || text == "Vika Poznyak") && subString == "admin001")
            {
                Administrator W1 = new Administrator();
                W1.Show();
                this.Close();
            }
            else
            {
                nepr.Visibility = Visibility.Visible;
            }
        }

        private void button_back_Click(object sender, RoutedEventArgs e)
        {
            MainWindow W1 = new MainWindow();
            W1.Show();
            this.Close();
        }
    }
}
