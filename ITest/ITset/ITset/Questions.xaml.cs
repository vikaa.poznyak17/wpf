﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ITset
{
    /// <summary>
    /// Логика взаимодействия для Questions.xaml
    /// </summary>
    public partial class Questions : Window
    {
        public static int lev; int count = 1; string otvet;
        public static int res = 0;
        public static string git1 = "Команда git stach сохраняет все новые файлы, которые еще не отслеживаются?", git1_1 = "Да", git1_2 = "Да", git1_3 = "В определенных ситуациях", git1_4 = "Не знаю";
        public static string git2 = "Опишите, что следующие git-команды делают с историей коммитов.\ngit reset --hard HEAD~5\ngit merge --squash HEAD@{1}", git2_1 = "Сбрасывают HEAD до 5-го коммита в репозитории, затем сливаются с мастером", git2_2 = "Отбрасывает 5 последних коммитов, а затем собирает их в один коммит", git2_3 = "Удаляет последние 5 коммитов", git2_4 = "Мержит последние 5 коммитов в новую ветку";
        public static string git3 = "Что делает команда git reser --soft HEAD^ ?", git3_1 = "Удаляет все предыдущие коммиты и откатывает историю репозиторияя в исходное состояние", git3_2 = "Откатывает рабочую ветку на первый коммит", git3_3 = "Сохраняет HEAD на текущем коммите, но удаляет все предыдущие коммиты", git3_4 = "Отменяет последний коммит в рабочей ветке и устанавливает HEAD на предыдущий коммит";
        public static string git4 = "Какая команда позволит применить последние изменения в stash?", git4_1 = "got stash pop", git4_2 = "git stach last", git4_3 = "git stash pop .", git4_4 = "git stash apply";
        public static string git5 = "Можно ли склонировать локальный репоиторий с помощью команды git clone ?", git5_1 = "Да, можно клонировать и удаленные и локальные репозитории", git5_2 = "Нет, можно клонировать только удаленные репозитории", git5_3 = "Да, можно клонировать только локальные репозитории", git5_4 = "Нет, репозитории тользя клонировать";
        public static string git6 = "Какая команда позволяет создать новый репозиторий?", git6_1 = "git create", git6_2 = "git start", git6_3 = "git init", git6_4 = "git initialize";
        public static string git7 = "Для удобства в склонированном репозитории создается удаленное соединение, которое называется ...?", git7_1 = "remote", git7_2 = "origin", git7_3 = "source", git7_4 = "original";
        public static string git8 = "Что произойдет, если вы попытаетесь удалить ветку, которая еще не была слита, используя команду\ngit branch -d branch_name ?", git8_1 = "Ветка будет удалена", git8_2 = "Гит запросит подтверждение действия", git8_3 = "Error: The branch 'branch_name' is not fully merged", git8_4 = "Ничего не произойдет";
        public static string git9 = "Как можно объединить несколько коммитов без использования git merge --squash ?", git9_1 = "Никак, git merge --squash - это единственная команда, которая позволяет такое сделать", git9_2 = "С помощью git reset", git9_3 = "С помощью git rebase", git9_4 = "С помощью git concat";
        public static string git10 = "Как удалить stash по имени?", git10_1 = "git stash clear <name>", git10_2 = "git stash pop <name>", git10_3 = "git stash drop <name>", git10_4 = "git stash delete <name>";
        
        public static string gitt1 = "Что сделает команда git branch без какого-либо параметра?", gitt1_1 = "Переключится на последнюю используемую ветку", gitt1_2 = "Выведет ошибку", gitt1_3 = "Выведет список локальных веток", gitt1_4 = "Выведет список удаленных (remote) веток";
        public static string gitt2 = "Как удалить все игнорируемые и неотслеживаемые, но не игнорируемые файлы из рабочей\nдиректории?", gitt2_1 = "git delete -fx", gitt2_2 = "git clean-fX", gitt2_3 = "git remote -fX", gitt2_4 = "git clean -fx";
        public static string gitt3 = "Как удалить все игнорируемые файлы из рабочей директории?", gitt3_1 = "git delete -fx", gitt3_2 = "git clean -fX", gitt3_3 = "git clean -fx", gitt3_4 = "git remote -fX";
        public static string gitt4 = "Какая команда используется для загрузки данных с удаленного репозитория и сразу выполняет\ngit merge для создания коммита слияния?", gitt4_1 = "git pull", gitt4_2 = "git merge", gitt4_3 = "git fetch", gitt4_4 = "git remote";
        public static string gitt5 = "Как удалить все стеши?", gitt5_1 = "git stash clear", gitt5_2 = "git stash remote", gitt5_3 = "git stash delete", gitt5_4 = "git stash rm";
        public static string gitt6 = "Можно ли изменять и комитить изменения в репозиторий, который был создан с флагом --bare ?", gitt6_1 = "Да", gitt6_2 = "Да, но с использованием флага --force при коммите", gitt6_3 = "Нет, вы можете использовать только push и pull с таким репозиторием", gitt6_4 = "Нет, потому что это удаленный репозиторий";
        public static string gitt7 = "Как удалить локальную ветку независимо от того, была смержена она или нет?", gitt7_1 = "git branch -d <branch>", gitt7_2 = "git branch -D <branch>", gitt7_3 = "git branch -di <branch>", gitt7_4 = "git branch remote <branch>";
        public static string gitt8 = "Какая команда позволит увидеть, какие ветки уже объединены в ветку, в которой вы работаете?", gitt8_1 = "git branch --merged", gitt8_2 = "git branch --no-merged", gitt8_3 = "git branch -mz-", gitt8_4 = "git branch --yes-merged";
        public static string gitt9 = "Какие аргументы используются, чтобы указать git stash также сохранить изменения в игнорируемых\nфайлах?", gitt9_1 = "git stash --all", gitt9_2 = "git stash -i", gitt9_3 = "git stash -u", gitt9_4 = "git stash --iall";
        public static string gitt10 = "Какая команда используется для начала интерактивного промежуточного сеанса, чтобы выбрать части\nфайла для добавления в коммит?", gitt10_1 = "git add -s", gitt10_2 = "git add -p", gitt10_3 = "git stage -2", gitt10_4 = "git stage -p";

        public static string gittt1 = "Для чего нужен GitFlow?", gittt1_1 = "Это способ организации работы с Git в большой команде", gittt1_2 = "Это способ организации работы с Git в большом проекте", gittt1_3 = "Это способ организации работы с Git в любой команде и на любом проекте", gittt1_4 = "Такого понятия нет";
        public static string gittt2 = "Что делает команда git abuse ?", gittt2_1 = "Удаляет лишние файлы из репозитория, очищает буфер и конфиг от мусора", gittt2_2 = "Находит и показывает все файлы с неправильным форматом", gittt2_3 = "Такой команды нет, есть только такой ключ: git --abuse", gittt2_4 = "Такой команды нет, есть команда: git reset";
        public static string gittt3 = "Как добавить новую директорию в Git?", gittt3_1 = "Добавить каждый файл из этой директории в Git", gittt3_2 = "Добавить хотя бы один файл из этой директории в Git", gittt3_3 = "Никак, Git работает только с файлами", gittt3_4 = "Команда git add -d";
        public static string gittt4 = "Для чего надо добавлять файлы в .gitignore?", gittt4_1 = "Чтобы Git удалял их историю, храня только последнюю версию", gittt4_2 = "Чтобы Git при работе с ними переспрашивал 'Are you sure you want to interact with this file?'", gittt4_3 = "Чтобы Git не замечал их и любые команды Git не могли их заафектить", gittt4_4 = "Это больше не используется";
        public static string gittt5 = "Как применить патч в Git?", gittt5_1 = "Команда git apply path/to/file", gittt5_2 = "Команда git patch path/to/file", gittt5_3 = "Команда git add path/to/file", gittt5_4 = "Такого понятия все еще нет";
        public static string gittt6 = "Как привести измененный файл в начальное состояние (до изменения)?", gittt6_1 = "Команда git abort path/to/file", gittt6_2 = "Команда git checkout path/to/file", gittt6_3 = "Команда git pull path/to/file", gittt6_4 = "Команда git commit path/to/file";
        public static string gittt7 = "Как узнать, кто автор строчки в файле, используя систему Git?", gittt7_1 = "Команда git show --author", gittt7_2 = "Команда git commit --autho", gittt7_3 = "Команда git blame", gittt7_4 = "Команда git status";
        public static string gittt8 = "Как удалить локальную ветку my_branch?", gittt8_1 = "Команда git delete my_branch", gittt8_2 = "Команда git branch -D my_branch", gittt8_3 = "Команда git reset my_branch", gittt8_4 = "Никак, Git как раз существует для того, чтобы никакие изменения нельзя было удалить";
        public static string gittt9 = "Как скачать ветку their_branch, если она уже есть в удаленном (remote) репозитории, но не локально?", gittt9_1 = "Команда git clone their_branch", gittt9_2 = "Команда git get origin their_branch", gittt9_3 = "Команда git fetch origin their_branch", gittt9_4 = "Команда git clone origin their_branch";
        public static string gittt10 = "Как сделать ветку с названием my_branch?", gittt10_1 = "Команда git branch my_branch", gittt10_2 = "Команда git create branch my_branch", gittt10_3 = "Команда git commit origin my_branch", gittt10_4 = "Команда git checkout my_branch";

        public static string sql1 = "Какой запрос позволит получить всех пользователей так, чтобы новые оказались в начале.\nДата регистрации - 'reg_date'", sql1_1 = "SELECT * FROM users ORDER BY reg_date DESC", sql1_2 = "SELECT * FROM users SORT BY reg_date DESC", sql1_3 = "SELECT * FROM users ORDER ASC BY reg_date", sql1_4 = "SELECT * FROM users SORT(reg_date)";
        public static string sql2 = "Как обновить запись в таблице 'Users', где имя пользователя (поле 'name') равно 'FakeUser'?", sql2_1 = "MODIFY Users SET activated = 0 WHERE name = 'FakeUser'", sql2_2 = "ALTER Users MODIFY activated = 0 WHERE name = 'FakeUser'", sql2_3 = "UPDATE Users(activated = 0) WHERE name is 'FakeUser'", sql2_4 = "UPDATE Users MODIFY activated = 0 WHERE name = 'FakeUser'";
        public static string sql3 = "Как выбрать все записи из таблицы 'Customers', где значение поля 'City' начинается с 'V'?", sql3_1 = "SELECT * FROM Customers WHERE City='V%'", sql3_2 = "SELECT * FROM Customers WHERE City LIKE 'V%'", sql3_3 = "SELECT * FROM Customers WHERE City LIKE '%V'", sql3_4 = "SELECT * FROM Customers WHERE City='%V%'";
        public static string sql4 = "Какой оператор SQL используется для вставки новых данных в базу данных?", sql4_1 = "STORE", sql4_2 = "ADD", sql4_3 = "INSERT INTO", sql4_4 = "ADD RECORD"; 
        public static string sql5 = "Как вернуть количество записей в таблице 'Customers'?", sql5_1 = "SELECT COUNT(*) FROM Customers", sql5_2 = "SELECT COLUMNS(*) FROM Customers", sql5_3 = "SELECT COUNT() FROM Customers", sql5_4 = "SELECT COLUMNS() FROM Customers"; 
        public static string sql6 = "Как добавить новую запись в таблицу 'Customers'?", sql6_1 = "INSERT VALUES ('Michael','Boston') INTO Customers", sql6_2 = "INSERT ('Michael','Boston') INTO Customers", sql6_3 = "None of the listed above", sql6_4 = "INSERT INTO Customers ('Michael','Boston')"; 
        public static string sql7 = "Какое ключевое слово используется для получения значений в промежутке?", sql7_1 = "RANGE", sql7_2 = "WITHIN", sql7_3 = "BETWEEN", sql7_4 = "IN"; 
        public static string sql8 = "Как выбрать столбец с именем 'FirstName' из таблицы с именем 'Persons'?", sql8_1 = "EXTRACT FirstName FROM Persons", sql8_2 = "SELECT Persons.FirstName", sql8_3 = "SELECT Persons FROM FirstName", sql8_4 = "SELECT FirstName FROM Persons"; 
        public static string sql9 = "Что такое реляционные базы данных?", sql9_1 = "База данных, в которой информация хранится в виде двумерных таблиц, связанных между собой", sql9_2 = "База данных, в которой одна ни с чем не связанная таблица", sql9_3 = "Любая база данных - реляционная", sql9_4 = "Совокупность данных, не связанных между собой"; 
        public static string sql10 = "Как выглядит запрос для вывода всех значений из таблицы 'Orders'?", sql10_1 = "SELECT ALL FROM Orders", sql10_2 = "SELECT % FROM Orders", sql10_3 = "SELECT * FROM Orders", sql10_4 = "SELECT * Orders FROM Orders";

        public static string sqll1 = "Какие данные мы получим из этого запроса?\nselect id, date, customer_name from Orders", sqll1_1 = "Неотсортированные номера и даты всех заказов с именами заказчиков", sqll1_2 = "Никакие, запрос составлен неверно", sqll1_3 = "Номера и даты всех заказов с именами заказчиков, отсортированные по первой колонке", sqll1_4 = "Номера и даты всех заказов с именами заказчиков, отсортированные по всем колонкам, содержащим слово Order";
        public static string sqll2 = "Что покажет следующий запрос:\nselect concat('index,' ','city') AS delivery_address from Orders", sqll2_1 = "соединит поля с индексом и адресом из таблицы Orders и покажет их с псевдонимом delivery_address", sqll2_2 = "покажет уникальные значения индексов и адресов из таблицы Orders", sqll2_3 = "ничего, запрос составлен неверно", sqll2_4 = "соединит поля с индексом и адресом из таблицы Orders, но покажет их без псевдонима";
        public static string sqll3 = "Что покажет следующий запрос?\nselect * from Orders where date between '2017-01-01' and '2017-12-31'", sqll3_1 = "Все данные по заказам, совершенным за 2017 год, за исключением 01 января 2017 года", sqll3_2 = "Все данные по заказам, совершенным за 2017 год, за исключением 31 декабря 2017 года", sqll3_3 = "Все данные по заказам, совершенным за 2017 год", sqll3_4 = "Ничего, запрос составлен неверно";
        public static string sqll4 = "Что не так с этим запросом?\nselect id, date from Orders where seller_id=NULL", sqll4_1 = "Все верно, запрос покажет все заказы, продавцы которых не проставлены", sqll4_2 = "NULL нужно взять в кавычки", sqll4_3 = "Сравнение с NULL можно проводить только с оператором IS", sqll4_4 = "Сравнение с NULL можно проводить только с оператором ON";
        public static string sqll5 = "Порядок выполнения операторов AND и OR следующий:", sqll5_1 = "Сначала выполняется AND, а затем OR", sqll5_2 = "Сначала выполняется OR, а затем AND", sqll5_3 = "Порядок выполнения операторов AND и OR зависит от того, какой из операторов стоит первым", sqll5_4 = "Операторы AND и OR выполняются одновременно";
        public static string sqll6 = "Что покажет следующий запрос?\nselect DISTINCT seller_id order by seller_id from Orders", sqll6_1 = "Уникальные ID продавцов, отсортированные по возрастанию", sqll6_2 = "Уникальные ID продавцов, отсортированные по убыванию", sqll6_3 = "Ничего, запрос составлен неверно, ORDER BY всегда ставится в конце запроса", sqll6_4 = "Неотсортированные никак уникальные ID продавцов";
        public static string sqll7 = "Что делает спецсимвол '_' в паре с оператором LIKT:\nselecr * from Orders where customer_name like 'mik_'", sqll7_1 = "Запрос составлен неверно, в паре с оператором like не используются спецсимволы", sqll7_2 = "найдет все имена, которые начинаются на mik, вне зависимости от того, из какого количества символов они состоят", sqll7_3 = "найдет данные, где имя равно mik", sqll7_4 = "найдет все имена, которые начинаются на mik и состоят из 4 символов";
        public static string sqll8 = "Выберите корректный пример использования функции CONCAT:", sqll8_1 = "select concat = index and city from Orders", sqll8_2 = "select concat IN ('index','city' from Orders", sqll8_3 = "select concat ('index',' ','city' from Orders", sqll8_4 = "нет правильного примера";
        public static string sqll9 = "Есть ли ошибка в запросе?\nselect id, date, customer_name from Orders where customer_name=Mike", sqll9_1 = "Запрос составлен правильно", sqll9_2 = "Mike необходимо записать в кавычках 'Mike'", sqll9_3 = "Нужно убрать лишние поля из запроса", sqll9_4 = "Строчку с where поменять местами с from";
        public static string sqll10 = "Выберите правильный пример использования функции округления ROUND", sqll10_1 = "select id,price * discount AS total price from Orders ROUND (2)", sqll10_2 = "select id,price * discount ROUND(2) AS total price from Orders", sqll10_3 = "нет правильного примера", sqll10_4 = "select id, ROUND(price * discount, 2) AS total price from Orders";

        public static string sqlll1 = "Что покажет следующий запрос:\nselect seller_id, count(*) from Orders GROUP BY seller_id HAVING seller_id IN (2,4,6)", sqlll1_1 = "количество заказов, сгруппированное по продавцам 2,4 и 6", sqlll1_2 = "количество продавцов, у которых 2,4 или 6 товаров", sqlll1_3 = "ничего, запрос составлен неверно, HAVING указыватся до группировки", sqlll1_4 = "ничего, запрос составлен неверно, для указания условия должно быть использовано WHERE";
        public static string sqlll2 = "В какиз командах можно использовать LIMIT?", sqlll2_1 = "Только select", sqlll2_2 = "select и insert", sqlll2_3 = "select, update, delete", sqlll2_4 = "select, insert, delete, update";
        public static string sqlll3 = "Выберите корректный пример составленного запроса с использованием JOIN.\nДанный запросвыведет нам данные ID заказа, имя заказчика и продавца:", sqlll3_1 = "select Orders.id, Orders.customer_name, Sellers.id from Orders LEFT JOIN ON Sellers AND Orders.seller_id = Sellers.id", sqlll3_2 = "select id AND customer_name AND seller_id from Orders LEFT JOIN ON seller_id = id", sqlll3_3 = "select Orders.id, Orders.customer_name, Sellers.id from Orders LEFT JOIN Sellers ON Orders.seller_id = Sellers.id", sqlll3_4 = "select Orders.id, Orders.customer_name, Sellers.id from Orders JOIN Sellers WHEN Orders.seller_id = Sellers.id";
        public static string sqlll4 = "Возможно ли использование одновременно двух агрегирующих функций:\nselect min(price), max(price) from Orders", sqlll4_1 = "Да, но данный запрос составлен неверно, надо так: select * from Orders where price IN (min,max)", sqlll4_2 = "Да, в результате мы получим минимальную и максимальную стоимости", sqlll4_3 = "Да, в результате мы получим стоимости, отсортированные от минимальной к максимальной", sqlll4_4 = "Нет, две функции использовать одновременно нельзя";
        public static string sqlll5 = "Выберите правильный пример запроса с использованием UNION:", sqlll5_1 = "select id, city from Orders order by id union select id, city from Sellers order by city", sqlll5_2 = "select id, city, seller_id from Orders and select city, id from Sellers order by id", sqlll5_3 = "Все запросы верные", sqlll5_4 = "select id, city from Orders union select id, city from Sellers order by id";
        public static string sqlll6 = "Какого строкового типа данных нет в SQL?", sqlll6_1 = "VARCHAR", sqlll6_2 = "STRING", sqlll6_3 = "CHAR", sqlll6_4 = "TEXT";
        public static string sqlll7 = "Какие поля из таблицы обязательно перечислять в INSERT для вставки данных?", sqlll7_1 = "Те, у которых нет DEFAULT значения и которые не имеют атрибут auto_increment", sqlll7_2 = "Абсолютно все", sqlll7_3 = "Только те, у которых нет DEFAULT значения", sqlll7_4 = "Все поля имеют негласное DEFAULT значение, обяхательных полей в SQL нет";
        public static string sqlll8 = "Как получить значение текущего года в SQL?", sqlll8_1 = "select now()", sqlll8_2 = "select year()", sqlll8_3 = "select year(now())", sqlll8_4 = "select year from Date";
        public static string sqlll9 = "Как правильно добавить строку в таблицу? Какой запрос верный?", sqlll9_1 = "UODATE INTO 'SimpleTable' SET 'some_text'='my txt'", sqlll9_2 = "INSERT INTO 'SimpleTable' SET 'some_text'='my text'", sqlll9_3 = "SET INTO 'SimpleTable' VALUE 'some_text'='my text'", sqlll9_4 = "INSERT INTO 'SimpleTable' ('some_text') VALUES('my text')";
        public static string sqlll10 = "Как сделать несколько записей в таблицу за один запрос?", sqlll10_1 = "Использовать MULTI INSERT INTO вместо INSERT INTO", sqlll10_2 = "Перечислить через запятую все наборы значений после VALUES", sqlll10_3 = "Использовать подзапрос", sqlll10_4 = "Никак";

        public static string java1 = "Для чего используется оператор NEW?", java1_1 = "Для создания экземпляра класса.", java1_2 = "Для создания новой переменной.", java1_3 = "Это антагонист оператора OLD.", java1_4 = "Для объявления нового класса.";
        public static string java2 = "Что означает перегрузка метода в Java (overload)?", java22_1 = "Изменение поведения метода класса относительно родительского.", java22_2 = "Изменение поведения метода класса относительно дочернего.", java2_3 = "Несколько разных классов с одинаковым методом.", java22_4 = "Несколько методов с одинаковым названием, но разным набором параметров.";
        public static string java3 = "Что означает ключевое слово extends?", java33_1 = "Что это дополнительный модуль класса, который расширяет его свойства.", java33_2 = "Что данный класс наследуется от другого.", java33_3 = "Что это самый большой класс в программе.", java3_4 = "Что два класса делают одно и то же.";
        public static string java4 = "Что означает переопределение метода в Java (override)?", java4_1 = "Несколько разных классов с одинаковым методом.", java4_2 = "Несколько методов с одинаковым названием, но разным набором параметров.", java4_3 = "Изменение поведения метода класса относительно родительского.", java4_4 = "Изменение поведения метода класса относительно дочернего.";
        public static string java5 = "Что вернет метод, объявленный следующим образом:\npublic static int getAmount()", java5_1 = "Вернет целочисленное значение.", java5_2 = "Вернет static-поле класса.", java5_3 = "Не ясно, надо смотреть код метода.", java5_4 = "Вернет ссылку на объект класса this.";
        public static string java6 = "Чем отличаются static-метод класса от обычного метода класса.", java6_1 = "Static-метод класса можно вызывать только внутри класса, а обычный - в любой части кода.", java6_2 = "Обычный метод класса можно перегрузить, а static-метод нельзя.", java6_3 = "Обычный метод класса работает от объекта класса, а static-метод от всего класса.", java6_4 = "Обычный метод класса можно переопределить, а static-метод нельзя.";
        public static string java7 = "Каков результат?", java7_1 = "Cat", java7_2 = "Dog", java7_3 = "Ошибка компиляции.", java7_4 = "Исключение во время выполнения.";
        public static string java8 = "Каков результат выполнения следующего кода?\nint[] a = new int[15];", java8_1 = "a[0] равно null", java8_2 = "a[14] не определено", java8_3 = "a[14] равно 0", java8_4 = "a.length равно 14";
        public static string java9 = "Каков тип литерала 5.345?", java9_1 = "float", java9_2 = "double", java9_3 = "Double", java9_4 = "Float";
        public static string java10 = "Можно ли вызвать static-метод внутри обычного?", java10_1 = "Никак, static-метод можно вызвать только от объекта класса.", java10_2 = "Можно, ничего дополнительно делать не надо.", java10_3 = "Можно, надо перед этим перегрузить обычный метод класса.", java10_4 = "Можно, надо перед этим переопределить обычный метод класса.";

        public static string javaa1 = "Что будет в результате выполнения кода?", javaa1_1 = "Код выведет на консоль X и программа завершится", javaa1_2 = "Код выведет на консоль XY и завершится практически сразу", javaa1_3 = "Код выведет на консоль X и никогда не завершится", javaa1_4 = "Ошибка времени выполнения";
        public static string javaa2 = "Каков результат выполнения кода?", javaa2_1 = "Ошибка компиляции.", javaa2_2 = "На консоль выведется 16.", javaa2_3 = "На консоль выведется 0.", javaa2_4 = "Ничего не выведется";
        public static string javaa3 = "Как вызвать обычный метод класса внутри static-метода?", javaa3_1 = "Можно, ничего дополнительно делать не надо.", javaa3_2 = "Можно, надо перед этим переопределить обычный метод класса.", javaa3_3 = "Можно, надо перед этим перегрузить обычный метод класса.", javaa3_4 = "Никак, static-метод не работает с объектом класса.";
        public static string javaa4 = "Что будет, если скомпилировать и запустить\nэтот код?", javaa4_1 = "Код отработает успешно", javaa4_2 = "StackOverflowException", javaa4_3 = "NullPointerException", javaa4_4 = "Ошибка во время компиляции";
        public static string javaa5 = "Каким будет вывод программы?", javaa5_1 = "Она откомпилируется и ничего не выведет на консоль.", javaa5_2 = "Компилируется, но выбросится исключение во время выполнения.", javaa5_3 = "Ошибка компиляции.", javaa5_4 = "Напечатает \"complete\".";
        public static string javaa6 = "Что будет в результате выполнения операции?\n2 + 2 == 5 && 12 / 4 == 3 || 2 == 5 % 3?", javaa6_1 = "true", javaa6_2 = "false", javaa6_3 = "null", javaa6_4 = "0";
        public static string javaa7 = "Каков результат выполнения следующего кода?", javaa7_1 = "Ничего не выведется на консоль.", javaa7_2 = "Hello world!!!", javaa7_3 = "Ошибка времени выполнения.", javaa7_4 = "Ошибка времени выполнения.";
        public static string javaa8 = "Что будет, если скомпилировать и выполнить код:", javaa8_1 = "Ошибка во время компиляции", javaa8_2 = "Код выполнится без ошибок, но и в консоль ничего не будет выведено", javaa8_3 = "NullPointerException", javaa8_4 = "В консоль будет выведено «Hello!»";
        public static string javaa9 = "Каким будет результат выполнения кода?", javaa9_1 = "Файл будет перезаписан.", javaa9_2 = "\"1\" будет добавлена к содержимому файла.", javaa9_3 = "Ничего не произойдет, т.к. файл уже существует.", javaa9_4 = "Будет выброшено IOException исключение.";
        public static string javaa10 = "Какие методы могут получить доступ к private членам класса?", javaa10_1 = "Только классы доступные в том же пакете.", javaa10_2 = "Только методы, определенные в том же классе.", javaa10_3 = "Только экземпляры того же класса.", javaa10_4 = "Только статические методы того же класса.";

        public static string javaaa1 = "Каким будет результат выполнения кода?", javaaa1_1 = "One Two Three + Исключение", javaaa1_2 = "Зациклится", javaaa1_3 = "One + Исключение", javaaa1_4 = "Не скомпилируется";
        public static string javaaa2 = "Что будет, если скомпилировать и выполнить код:", javaaa2_1 = "Ошибка компиляции в строке 2", javaaa2_2 = "woof burble", javaaa2_3 = "Ошибка компиляции в строке 4", javaaa2_4 = "Ошибка компиляции в строке 3";
        public static string javaaa3 = "Каким будет результат выполнения кода?", javaaa3_1 = "many", javaaa3_2 = "a few", javaaa3_3 = "Ошибка компиляции.", javaaa3_4 = "Ошибка времени выполнения.";
        public static string javaaa4 = "Напишите функциональный дескриптор функционального интерфейса BiFunction<T, U, R>.", javaaa4_1 = "(U)  -> R", javaaa4_2 = "(T)  -> R", javaaa4_3 = "(T, U)  -> R", javaaa4_4 = "(U, R) -> T";
        public static string javaaa5 = "Напишите функциональный дескриптор для интерфейса UnaryOperator.", javaaa5_1 = "T -> T -> T", javaaa5_2 = "T -> T", javaaa5_3 = "(T, T) -> T", javaaa5_4 = "T -> void";
        public static string javaaa6 = "Каков результат выполнения следующего кода?\nLocalDate.of(2014, 1, 2).atTime(14, 30, 59, 999999);", javaaa6_1 = "LocalDateTime со значением 2014-01-02 14:30:59:999999", javaaa6_2 = "LocalTime со значением 14:30:59:999999", javaaa6_3 = "LocalDate со значением 2014-01-02", javaaa6_4 = "Ошибка времени выполнения.";
        public static string javaaa7 = "Каким будет результат выполнения кода:", javaaa7_1 = "2 1", javaaa7_2 = "2 + Исключение", javaaa7_3 = "2 2", javaaa7_4 = "Не скомпилируется";
        public static string javaaa8 = "Каким будет результат выполнения кода?", javaaa8_1 = "Ошибка компиляции из-за множества ошибок", javaaa8_2 = "Ошибка компиляции из-за ошибки в строке 7", javaaa8_3 = "Ошибка компиляции из-за ошибки в строке 6", javaaa8_4 = "Ошибка компиляции из-за ошибки в строке 11";
        public static string javaaa9 = "Что произойдет после вызова метода test()?", javaaa9_1 = "Не скомпилируется", javaaa9_2 = "В консоль выведется «Hello!»", javaaa9_3 = "Ничего не будет напечатано", javaaa9_4 = "Исключение в рантайме";
        public static string javaaa10 = "Какое утверждение верно для статического вложенного класса?", javaaa10_1 = "Должна быть ссылка на экземпляр включающего класса, чтобы создать его экземпляр.", javaaa10_2 = "Он должен расширять класс вложения.", javaaa10_3 = "Это переменные и методы должны быть статическими.", javaaa10_4 = "Он не имеет доступа к нестатическим членам окружающего класса.";

        public static string python1 = "Определить тип данных переменной a:\n>>>a = 2", python1_1 = "dict", python1_2 = "int", python1_3 = "tuple", python1_4 = "list";
        public static string python2 = "Чему равно i в 3 строке?\n>>> i = 0\n>>> while 100.-1: i += 1\n>>> print i", python22_1 = "99", python22_2 = "Очень большому числу", python22_3 = "101", python22_4 = "100";
        public static string python3 = "Каков результат выполнения следующего кода:\n>>>2/3", python3_1 = "TypeError: unsupported operand type(s)", python3_2 = "1", python33_3 = "0", python3_4 = "0.6666666666666666";
        public static string python4 = "Каков результат выполнения следующего кода:\n>>> a = b = c = 3\n>>> b = a/3\n>>> print c", python4_1 = "Ошибка! Строка 1", python4_2 = "0", python4_3 = "1", python4_4 = "3";
        public static string python5 = "Что напечатает следующий код?\nfruits = { 'apple', 'banana', 'apple' }\nprint(fruits)", python5_1 = "{'apple','banana'}", python5_2 = "Возникнет синтаксическая ошибка", python5_3 = "{'apple','banana','apple'}", python5_4 = "{'apple','apple','banana'}";
        public static string python6 = "Каков результат выполнения следующего кода:\n>>> a = [-99.9, -53.14, -78.8, -36.7]\n>>> b = -100.2\n>>> print a > b", python6_1 = "false", python6_2 = "true", python6_3 = "False", python6_4 = "True";
        public static string python7 = "Что будет напечатано при исполнении следующего кода?\nprint(type(1 / 2))", python7_1 = "type 'number'", python7_2 = "type 'double'", python7_3 = "type 'int'", python7_4 = "type 'turple'";
        public static string python8 = "Каков результат выполнения следующего кода:\n>>> y = [0, 5, -10, 0, 82, 99]\n>>> print y[2]", python8_1 = "-10", python8_2 = "2", python8_3 = "Ошибка! Нет такого элемента в списке.", python8_4 = "Ошибка! Неправильное обращение к элементы списка.";
        public static string python9 = "Какой результат даст выражение?\nTrue + 4", python9_1 = "5", python9_2 = "Произойдет ошибка", python9_3 = "True", python9_4 = "False";
        public static string python10 = "Каков результат выполнения следующего кода:\n>>> s = 'as'\n>>> print 2*s", python10_1 = "asas", python10_2 = "as2as2", python10_3 = "as2", python10_4 = "2as2as";

        public static string pythonn1 = "Каков результат выполнения следующего кода:", pythonn1_1 = "Ошибка! В 4 строке", pythonn1_2 = "[0, 1, 2, 3, 4, 5]", pythonn1_3 = "[0, 1, 2, 3, 4, 5, 6, 7, 8, 0, 1, 2, 3, 4, 5]", pythonn1_4 = "[0, 1, 2, 3, 4, 5, 6, 7, 8]";
        public static string pythonn2 = "Каков результат выполнения следующего кода:", pythonn2_1 = "False", pythonn2_2 = "Ошибка в 4 строке!", pythonn2_3 = "True", pythonn2_4 = "Ошибка в 2 строке!";
        public static string pythonn3 = "Каким будет результат выполнения кода?", pythonn3_1 = "1", pythonn3_2 = "2", pythonn3_3 = "3", pythonn3_4 = "Возникнет ошибка";
        public static string pythonn4 = "Каков результат выполнения следующего кода:", pythonn4_1 = "[1, ['abc', 'xyz'], 3]", pythonn4_2 = "[1, 2, 'abc', 'xyz']", pythonn4_3 = "[1, 2, ['abc', 'xyz']]", pythonn4_4 = "[[1, 2], 'abc', 'xyz']";
        public static string pythonn5 = "Каков результат выполнения следующего кода?", pythonn5_1 = "1", pythonn5_2 = "2", pythonn5_3 = "func(0)", pythonn5_4 = "Возникнет ошибка";
        public static string pythonn6 = "Каков результат выполнения следующего кода:", pythonn6_1 = "['two', 3, 1]", pythonn6_2 = "['one', 2, 3]", pythonn6_3 = "['one', 'two', 'three']", pythonn6_4 = "[2, 'three', 'one']";
        public static string pythonn7 = "Что выведет следующий фрагмент кода?\nx = 4.5\ny = 2\nprint(x // y)", pythonn7_1 = "2.25", pythonn7_2 = "21", pythonn7_3 = "2", pythonn7_4 = "9";
        public static string pythonn8 = "Каков результат выполнения следующего кода:\n>>> a = 5\n>>> print 12 < a < -12", pythonn8_1 = "true", pythonn8_2 = "false", pythonn8_3 = "True", pythonn8_4 = "False";
        public static string pythonn9 = "Что будет напечатано?\nname = \"snow storm\"\nprint(\"%s\" % name[6:8])", pythonn9_1 = "to", pythonn9_2 = "st", pythonn9_3 = "sto", pythonn9_4 = "Syntax Error";
        public static string pythonn10 = "Каков результат выполнения следующего кода:\n>>> import random\n>>> d = random.random()\n>>> print abs(d) > 1", pythonn10_1 = "True", pythonn10_2 = "true", pythonn10_3 = "False", pythonn10_4 = "false";

        public static string pythonnn1 = "Что выведет следующий код при его исполнении?\nprint(type(1 / 2))", pythonnn1_1 = "class 'float'", pythonnn1_2 = "class 'int'", pythonnn1_3 = "class 'double'", pythonnn1_4 = "class 'tuple'";
        public static string pythonnn2 = "Что делает следующий код?\ndef a(b, c, d): pass", pythonnn2_1 = "Определяет список и инициализирует его", pythonnn2_2 = "Определяет функцию, которая ничего не делает", pythonnn2_3 = "Определяет функцию, которая передает параметры", pythonnn2_4 = "Определяет пустой класс";
        public static string pythonnn3 = "Каков результат выполнения следующего кода:", pythonnn3_1 = "Polly is 12 years old. Or 12.2?", pythonnn3_2 = "Ошибка! Строка 5", pythonnn3_3 = "Polly is 12.2 years old. Or 12?", pythonnn3_4 = "Ошибка! Строка 2";
        public static string pythonnn4 = "Как получить порядок базовых классов, в котором будет производиться поиск нужного метода во\nвремя исполнения программы?", pythonnn4_1 = "Это невозможно", pythonnn4_2 = "cls.get_bases()", pythonnn4_3 = "cls.__mro__", pythonnn4_4 = "cls.__bro__";
        public static string pythonnn5 = "Что интерпретатор выведет на месте знаков вопроса?", pythonnn5_1 = "False, True", pythonnn5_2 = "False, False", pythonnn5_3 = "True, False", pythonnn5_4 = "True, True";
        public static string pythonnn6 = "Что выведет следующая программа?\na = [1,2,3,None,(),[],]\nprint(len(a))", pythonnn6_1 = "Syntax Error", pythonnn6_2 = "4", pythonnn6_3 = "5", pythonnn6_4 = "6";
        public static string pythonnn7 = "Каким будет результат выполнения данной программы?", pythonnn7_1 = "45", pythonnn7_2 = "42", pythonnn7_3 = "102", pythonnn7_4 = "Ошибка интерпретатора";
        public static string pythonnn8 = "Каков результат выполнения следующего кода:", pythonnn8_1 = "-7", pythonnn8_2 = "-5", pythonnn8_3 = "9", pythonnn8_4 = "10";
        public static string pythonnn9 = "Чему будет равно а?\na = [1,2,3]\na[-3:-1] = 10,20,30,40", pythonnn9_1 = "[10,20,30,40,3]", pythonnn9_2 = "TypeError", pythonnn9_3 = "IndexError", pythonnn9_4 = "[10,20,30,40,2,3]";
        public static string pythonnn10 = "Какая из функций вернет итерируемый объект?", pythonnn10_1 = "len()", pythonnn10_2 = "xrange()", pythonnn10_3 = "range()", pythonnn10_4 = "ord()";

        public static string csh1 = "Где верно происходит вывод данных в консоль?", csh1_1 = "print(\"Hi\");", csh1_2 = "console.log(\"Hi\");", csh1_3 = "Console.write(\"Hi\");", csh1_4 = "Console.WriteLine(\"Hi\");";
        public static string csh2 = "В чем отличие между break и continue?", csh2_1 = "Continue пропускает итерацию, break выходит из цикла", csh2_2 = "Continue работает только в циклах, break дополнительно в методах", csh2_3 = "Нет отличий", csh2_4 = "Break используется в Switch case, а continue в циклах";
        public static string csh3 = "Какие типы переменных существуют?", csh3_1 = "int, char, bool, double, uint, short", csh3_2 = "Все перечисленные", csh3_3 = "Ни один из них", csh3_4 = "int, char, bool, float, double";
        public static string csh4 = "Какие циклы существуют в языке C#?", csh4_1 = "for, while, do while, foreach", csh4_2 = "for, while", csh4_3 = "for, while, foreach", csh4_4 = "for, while, do while";
        public static string csh5 = "Где правильно создан массив?", csh5_1 = "int arr = [2,5];", csh5_2 = "int arr = {2,5};", csh5_3 = "int[] arr = new Array [2,5];", csh5_4 = "int[] arr = new int [2]{2,5};";
        public static string csh6 = "Что такое перегрузка методов?", csh6_1 = "Использование одного имени для разных методов", csh6_2 = "Передача слишком большого файла через return", csh6_3 = "Передача слишком больших данных в функцию", csh6_4 = "Написание слишком большого кода в методе";
        public static string csh7 = "Какая функция корректно сравнивает две подстроки?", csh7_1 = "String.Check(\"hi\",\"hello\");", csh7_2 = "String.Match(\"hi\",\"hello\");", csh7_3 = "String.Compare(\"hi\",\"hello\");", csh7_4 = "String.Equal(\"hi\",\"hello\");";
        public static string csh8 = "Где правильно создана переменная?", csh8_1 = "float big_num = 23.2234;", csh8_2 = "char symbol = 'A';", csh8_3 = "int num = \"1\";", csh8_4 = "$x = 10;";
        public static string csh9 = "Что делает try-catch?", csh9_1 = "Работает с исключениями", csh9_2 = "Работает с файлами", csh9_3 = "Работает с классами", csh9_4 = "Работает с базой данных";
        public static string csh10 = "Для чего можно использовать язык C#?", csh10_1 = "Для написания игр", csh10_2 = "Все перечисленное", csh10_3 = "Для создания веб сайтов", csh10_4 = "Для создания программ под ПК";

        public static string cshh1 = "Какой из операторов передает управление следующей итерации цикла?", cshh1_1 = "stop", cshh1_2 = "continue", cshh1_3 = "break", cshh1_4 = "await";
        public static string cshh2 = "В чем разница между перегрузкой(overloading) и переопределением(overriding)?", cshh2_1 = "Ее не существует.Оба понятия описывают одно и то же явление.", cshh2_2 = "Переопределение означает наличие двух или более методов в одном классе с тем же именем, но с разными аргументами.", cshh2_3 = "Перегрузка означает наличие двух методов с теми же аргументами, но с различными реализациями. ", cshh2_4 = "Ни один из предложенных вариантов не является верным.";
        public static string cshh3 = "Какой из перечисленных типов не является значимым?", cshh3_1 = "int", cshh3_2 = "enum", cshh3_3 = "class", cshh3_4 = "float";
        public static string cshh4 = "Какое утверждение неверно?", cshh4_1 = "В каждом свойстве должны быть get и set блоки.", cshh4_2 = "В свойства мы можем вложить дополнительную логику.", cshh4_3 = "Свойства позволяют управлять доступом к переменной.", cshh4_4 = "Благодаря свойствам реализуется один из принципов ООП – инкапсуляция.";
        public static string cshh5 = "ООП поддерживает два вида связывания объектов с кодом методов.\nСоответствующие методы называются:", cshh5_1 = "Виртуальными и абстрактными", cshh5_2 = "Статическими и виртуальными", cshh5_3 = "Абстрактными и статическими", cshh5_4 = "Публичными и приватными";
        public static string cshh6 = "С чем нельзя использовать модификатор abstract?", cshh6_1 = "Со свойствами.", cshh6_2 = "С индексаторами.", cshh6_3 = "С событиями.", cshh6_4 = "Со структурами.";
        public static string cshh7 = "В каком из приведенных вариантов правильно описано объявление типа перечисления?", cshh7_1 = "enum Day {Sat, Sun, Mon, Tue, Wed, Thu, Fri}", cshh7_2 = "list Day { Sat, Sun, Mon, Tue, Wed, Thu, Fri }", cshh7_3 = "enum { Sat, Sun, Mon, Tue, Wed, Thu, Fri }", cshh7_4 = "{ Sat, Sun, Mon, Tue, Wed, Thu, Fri } as Enum";
        public static string cshh8 = "Какое утверждение верно?", cshh8_1 = "Если структура или объект класса присваивается новому объекту, то в новую переменную сохраняется копия всех данных.", cshh8_2 = "Структура является размерным типом, а класс – ссылочный.", cshh8_3 = "В структуре нельзя определять конструктор.", cshh8_4 = "От структуры так же, как и от класса, можно наследоваться.";
        public static string cshh9 = "Пусть объявлен кортеж: var tuple = (5, 10). Как обратиться к значению 5?", cshh9_1 = "tuple[0]", cshh9_2 = "tuple.first", cshh9_3 = "touple.5", cshh9_4 = "tuple.Item1";
        public static string cshh10 = "Какое из утверждений верно?", cshh10_1 = "У интерфейсов можно объявлять свойства с помощью ключевого слова property.", cshh10_2 = "У интерфейсов можно объявлять свойства так же, как и у классов.", cshh10_3 = "У интерфейсов можно объявлять свойства, но без тела у методов доступа свойства.", cshh10_4 = "У интерфейсов нельзя объявлять свойства.";

        public static string cshhh1 = "Унифицированная коллекция здается при помощи ключевого слова:", cshhh1_1 = "HashSet<T>", cshhh1_2 = "Lis<T>", cshhh1_3 = "Dictionary<K,T>", cshhh1_4 = "ArrayList";
        public static string cshhh2 = "Можно ли перехватить добавление и удаление делегата из события? Если да, то как?", cshhh2_1 = "Это невозможно", cshhh2_2 = "Можно.Для этого нужно использовать ключевые слова get и set", cshhh2_3 = "Можно. Для этого нужно использовать ключевые слова add и remove", cshhh2_4 = "Можно.Для этого нужно переопределить операторы - и + для делегата";
        public static string cshhh3 = "Какие из перечисленных ниже типов являются ссылочными?\n1. string\n2. delegate\n3. uint\n4. enum\n5. struct", cshhh3_1 = "1 и 2 варианты", cshhh3_2 = "2 и 4 варианты", cshhh3_3 = "1, 3 и 5 варианты", cshhh3_4 = "1, 2, 4 и 5 варианты";
        public static string cshhh4 = "С помощью чего можно создать многопоточное приложение в C#?\n1. Класс ThreadPool\n2. Класс Collection\n3. Action-класс с лямбда-функциями\n4. Метод BeginInvoke()\n5. Метод Join()", cshhh4_1 = "1, 2 и 3 варианты", cshhh4_2 = "1, 3 и 4 варианты", cshhh4_3 = "1 и 5 варианты", cshhh4_4 = "1 и 4 варианты";
        public static string cshhh5 = "Какой уровень изоляции устанавливает разделяемые блокировки на все считываемые данные и\nудерживает их до подтверждения или отмены транзакции?", cshhh5_1 = "READ UNCOMMITTED", cshhh5_2 = "READ COMMITTED", cshhh5_3 = "REPEATABLE READ", cshhh5_4 = "SERIALIZABLE";
        public static string cshhh6 = "Какие операции являются атомарными в C#?\n1. Операции инкремента/декремента\n2. Операции чтения/записи в 32-битные типы вроде bool, char, byte, sbyte, short\n3. Операции ссылочного присваивания\n4. Операции создания потоков, а также их синхронизации и блокировки", cshhh6_1 = "1 и 3 варианты", cshhh6_2 = "2 и 3 варианты", cshhh6_3 = "2 и 4 варианты", cshhh6_4 = "3 и 4 варианты";
        public static string cshhh7 = "Что будет результатом выполнения этого фрагмента кода?\nusing System; using System.Threading; public class Program { public static void Main() {\nobject sync = new object(); var thread = new Thread(()=> { try { Work(); } finally { lock (sync) {\nMonitor.PulseAll(sync); } } }); thread.Start(); lock (sync) { Monitor.Wait(sync); }\nConsole.WriteLine(\"test\"); } private static void Work() { Thread.Sleep(1000); } }", cshhh7_1 = "Будет выброшено исключение SynchronizationLockException", cshhh7_2 = "В результате взаимоблокировки (deadlock) test не будет выведено", cshhh7_3 = "Одно из двух: либо будет выведено test, либо произойдёт взаимоблокировка", cshhh7_4 = "Всегда будет выведено test";
        public static string cshhh8 = "Какой уровень изоляции устанавливает блокировку на всю область данных, считываемых\nсоответствующей транзакцией?", cshhh8_1 = "SERIALIZABLE", cshhh8_2 = "SNAPSHOT", cshhh8_3 = "READ COMMITTED", cshhh8_4 = "READ UNCOMMITTED";
        public static string cshhh9 = "Что поможет оптимизировать SQL-запросы, если нужно ссылаться на две колонки в условии\nвыражения WHERE?", cshhh9_1 = "Композитные индексы", cshhh9_2 = "Распределённая/кластерная конфигурация", cshhh9_3 = "Частое повторение одинаковых запросов", cshhh9_4 = "Отсутствие индексов";
        public static string cshhh10 = "Что из нижеперечисленного не может иметь модификатор virtual?", cshhh10_1 = "Свойства.", cshhh10_2 = "Индексаторы.", cshhh10_3 = "Поля.", cshhh10_4 = "Методы.";

        public Questions()
        {
            InitializeComponent();
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Method();
            count++;
        }
        public void Method()
        {
            switch (DifficultyLevels.num)
            {
                case 1: // Git
                    switch(lev)
                    {
                        case 1: // Начальный уровень
                            switch(count)
                            {
                                case 1: // вопросы
                                    Met(git1, git1_1, git1_2, git1_3, git1_4);
                                    otvet = b2.Name;
                                    break;
                                case 2:
                                    Met(git2, git2_1, git2_2, git2_3, git2_4);
                                    otvet = b2.Name;
                                    break;
                                case 3:
                                    Met(git3, git3_1, git3_2, git3_3, git3_4);
                                    otvet = b4.Name;
                                    break;
                                case 4:
                                    Met(git2, git4_1, git4_2, git4_3, git4_4);
                                    otvet = b1.Name;
                                    break;
                                case 5:
                                    Met(git5, git5_1, git5_2, git5_3, git5_4);
                                    otvet = b1.Name;
                                    break;
                                case 6:
                                    Met(git6, git6_1, git6_2, git6_3, git6_4);
                                    otvet = b3.Name;
                                    break;
                                case 7:
                                    Met(git7, git7_1, git7_2, git7_3, git7_4);
                                    otvet = b2.Name;
                                    break;
                                case 8:
                                    Met(git8, git8_1, git8_2, git8_3, git8_4);
                                    otvet = b3.Name;
                                    break;
                                case 9:
                                    Met(git9, git9_1, git9_2, git9_3, git9_4);
                                    otvet = b3.Name;
                                    break;
                                case 10:
                                    Met(git10, git10_1, git10_2, git10_3, git10_4);
                                    otvet = b3.Name;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case 2: // Средний уровень
                            switch (count)
                            {
                                case 1: // вопросы
                                    Met(gitt1, gitt1_1, gitt1_2, gitt1_3, gitt1_4);
                                    otvet = b3.Name;
                                    break;
                                case 2:
                                    Met(gitt2, gitt2_1, gitt2_2, gitt2_3, gitt2_4);
                                    otvet = b4.Name;
                                    break;
                                case 3:
                                    Met(gitt3, gitt3_1, gitt3_2, gitt3_3, gitt3_4);
                                    otvet = b2.Name;
                                    break;
                                case 4:
                                    Met(gitt2, gitt4_1, gitt4_2, gitt4_3, gitt4_4);
                                    otvet = b1.Name;
                                    break;
                                case 5:
                                    Met(gitt5, gitt5_1, gitt5_2, gitt5_3, gitt5_4);
                                    otvet = b1.Name;
                                    break;
                                case 6:
                                    Met(gitt6, gitt6_1, gitt6_2, gitt6_3, gitt6_4);
                                    otvet = b3.Name;
                                    break;
                                case 7:
                                    Met(gitt7, gitt7_1, gitt7_2, gitt7_3, gitt7_4);
                                    otvet = b2.Name;
                                    break;
                                case 8:
                                    Met(gitt8, gitt8_1, gitt8_2, gitt8_3, gitt8_4);
                                    otvet = b1.Name;
                                    break;
                                case 9:
                                    Met(gitt9, gitt9_1, gitt9_2, gitt9_3, gitt9_4);
                                    otvet = b1.Name;
                                    break;
                                case 10:
                                    Met(gitt10, gitt10_1, gitt10_2, gitt10_3, gitt10_4);
                                    otvet = b2.Name;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case 3: // Сложный уровень
                            switch (count)
                            {
                                case 1: // вопросы
                                    Met(gittt1, gittt1_1, gittt1_2, gittt1_3, gittt1_4);
                                    otvet = b3.Name;
                                    break;
                                case 2:
                                    Met(gittt2, gittt2_1, gittt2_2, gittt2_3, gittt2_4);
                                    otvet = b4.Name;
                                    break;
                                case 3:
                                    Met(gittt3, gittt3_1, gittt3_2, gittt3_3, gittt3_4);
                                    otvet = b2.Name;
                                    break;
                                case 4:
                                    Met(gittt4, gittt4_1, gittt4_2, gittt4_3, gittt4_4);
                                    otvet = b3.Name;
                                    break;
                                case 5:
                                    Met(gittt5, gittt5_1, gittt5_2, gittt5_3, gittt5_4);
                                    otvet = b1.Name;
                                    break;
                                case 6:
                                    Met(gittt6, gittt6_1, gittt6_2, gittt6_3, gittt6_4);
                                    otvet = b2.Name;
                                    break;
                                case 7:
                                    Met(gittt7, gittt7_1, gittt7_2, gittt7_3, gittt7_4);
                                    otvet = b3.Name;
                                    break;
                                case 8:
                                    Met(gittt8, gittt8_1, gittt8_2, gittt8_3, gittt8_4); 
                                    otvet = b2.Name;
                                    break;
                                case 9:
                                    Met(gittt9, gittt9_1, gittt9_2, gittt9_3, gittt9_4); 
                                    otvet = b3.Name;
                                    break;
                                case 10:
                                    Met(gittt10, gittt10_1, gittt10_2, gittt10_3, gittt10_4); 
                                    otvet = b1.Name;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        default:
                            break;
                    }
                    break;
                case 2: // SQL
                    switch (lev)
                    {
                        case 1: // Начальный уровень
                            switch (count)
                            {
                                case 1: // вопросы
                                    Met(sql1, sql1_1, sql1_2, sql1_3, sql1_4);
                                    otvet = b1.Name;
                                    break;
                                case 2:
                                    Met(sql2, sql2_1, sql2_2, sql2_3, sql2_4);
                                    otvet = b2.Name;
                                    break;
                                case 3:
                                    Met(sql3, sql3_1, sql3_2, sql3_3, sql3_4);
                                    otvet = b2.Name;
                                    break;
                                case 4:
                                    Met(sql4, sql4_1, sql4_2, sql4_3, sql4_4);
                                    otvet = b3.Name;
                                    break;
                                case 5:
                                    Met(sql5, sql5_1, sql5_2, sql5_3, sql5_4);
                                    otvet = b1.Name;
                                    break;
                                case 6:
                                    Met(sql6, sql6_1, sql6_2, sql6_3, sql6_4);
                                    otvet = b4.Name;
                                    break;
                                case 7:
                                    Met(sql7, sql7_1, sql7_2, sql7_3, sql7_4);
                                    otvet = b3.Name;
                                    break;
                                case 8:
                                    Met(sql8, sql8_1, sql8_2, sql8_3, sql8_4);
                                    otvet = b4.Name;
                                    break;
                                case 9:
                                    Met(sql9, sql9_1, sql9_2, sql9_3, sql9_4);
                                    otvet = b1.Name;
                                    break;
                                case 10:
                                    Met(sql10, sql10_1, sql10_2, sql10_3, sql10_4);
                                    otvet = b3.Name;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case 2: // Средний уровень
                            switch (count)
                            {
                                case 1: // вопросы
                                    Met(sqll1, sqll1_1, sqll1_2, sqll1_3, sqll1_4);
                                    otvet = b1.Name;
                                    break;
                                case 2:
                                    Met(sqll2, sqll2_1, sqll2_2, sqll2_3, sqll2_4);
                                    otvet = b2.Name;
                                    break;
                                case 3:
                                    Met(sqll3, sqll3_1, sqll3_2, sqll3_3, sqll3_4);
                                    otvet = b2.Name;
                                    break;
                                case 4:
                                    Met(sqll4, sqll4_1, sqll4_2, sqll4_3, sqll4_4);
                                    otvet = b3.Name;
                                    break;
                                case 5:
                                    Met(sqll5, sqll5_1, sqll5_2, sqll5_3, sqll5_4);
                                    otvet = b1.Name;
                                    break;
                                case 6:
                                    Met(sqll6, sqll6_1, sqll6_2, sqll6_3, sqll6_4);
                                    otvet = b4.Name;
                                    break;
                                case 7:
                                    Met(sqll7, sqll7_1, sqll7_2, sqll7_3, sqll7_4);
                                    otvet = b3.Name;
                                    break;
                                case 8:
                                    Met(sqll8, sqll8_1, sqll8_2, sqll8_3, sqll8_4);
                                    otvet = b4.Name;
                                    break;
                                case 9:
                                    Met(sqll9, sqll9_1, sqll9_2, sqll9_3, sqll9_4);
                                    otvet = b1.Name;
                                    break;
                                case 10:
                                    Met(sqll10, sqll10_1, sqll10_2, sqll10_3, sqll10_4);
                                    otvet = b3.Name;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case 3: // Сложный уровень
                            switch (count)
                            {
                                case 1: // вопросы
                                    Met(sqlll1, sqlll1_1, sqlll1_2, sqlll1_3, sqlll1_4);
                                    otvet = b1.Name;
                                    break;
                                case 2:
                                    Met(sqlll2, sqlll2_1, sqlll2_2, sqlll2_3, sqlll2_4);
                                    otvet = b2.Name;
                                    break;
                                case 3:
                                    Met(sqlll3, sqlll3_1, sqlll3_2, sqlll3_3, sqlll3_4);
                                    otvet = b2.Name;
                                    break;
                                case 4:
                                    Met(sqlll4, sqlll4_1, sqlll4_2, sqlll4_3, sqlll4_4);
                                    otvet = b3.Name;
                                    break;
                                case 5:
                                    Met(sqlll5, sqlll5_1, sqlll5_2, sqlll5_3, sqlll5_4);
                                    otvet = b1.Name;
                                    break;
                                case 6:
                                    Met(sqlll6, sqlll6_1, sqlll6_2, sqlll6_3, sqlll6_4);
                                    otvet = b4.Name;
                                    break;
                                case 7:
                                    Met(sqll7, sqlll7_1, sqlll7_2, sqlll7_3, sqlll7_4);
                                    otvet = b3.Name;
                                    break;
                                case 8:
                                    Met(sqlll8, sqlll8_1, sqlll8_2, sqlll8_3, sqlll8_4);
                                    otvet = b4.Name;
                                    break;
                                case 9:
                                    Met(sqlll9, sqlll9_1, sqlll9_2, sqlll9_3, sqlll9_4);
                                    otvet = b1.Name;
                                    break;
                                case 10:
                                    Met(sqlll10, sqlll10_1, sqlll10_2, sqlll10_3, sqlll10_4);
                                    otvet = b3.Name;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        default:
                            break;
                    }
                    break;
                case 3: // Java
                    switch (lev)
                    {
                        case 1:  // Начальный уровень
                            switch (count)
                            {
                                case 1: // вопросы
                                    Met(java1, java1_1, java1_2, java1_3, java1_4);
                                    otvet = b1.Name;
                                    break;
                                case 2:
                                    Met(java2, java22_1, java22_2, java2_3, java22_4);
                                    otvet = b4.Name;
                                    break;
                                case 3:
                                    Met(java3, java33_1, java33_2, java33_3, java3_4);
                                    otvet = b2.Name;
                                    break;
                                case 4:
                                    Met(java4, java4_1, java4_2, java4_3, java4_4);
                                    otvet = b3.Name;
                                    break;
                                case 5:
                                    Met(java5, java5_1, java5_2, java5_3, java5_4);
                                    otvet = b1.Name;
                                    break;
                                case 6:
                                    Met(java6, java6_1, java6_2, java6_3, java6_4);
                                    otvet = b3.Name;
                                    break;
                                case 7:
                                    java1_7.Visibility = Visibility.Visible;
                                    Met(java7, java7_1, java7_2, java7_3, java7_4);
                                    otvet = b2.Name;
                                    break;
                                case 8:
                                    java1_7.Visibility = Visibility.Hidden;
                                    Met(java8, java8_1, java8_2, java8_3, java8_4);
                                    otvet = b3.Name;
                                    break;
                                case 9:
                                    Met(java9, java9_1, java9_2, java9_3, java9_4);
                                    otvet = b2.Name;
                                    break;
                                case 10:
                                    Met(java10, java10_1, java10_2, java10_3, java10_4);
                                    otvet = b2.Name;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case 2: // Средний уровень
                            switch (count)
                            {
                                case 1: // вопросы
                                    java2_1.Visibility = Visibility.Visible;
                                    Met(javaa1, javaa1_1, javaa1_2, javaa1_3, javaa1_4);
                                    otvet = b4.Name;
                                    break;
                                case 2:
                                    java2_1.Visibility = Visibility.Hidden;
                                    java2_2.Visibility = Visibility.Visible;
                                    Met(javaa2, javaa2_1, javaa2_2, javaa2_3, javaa2_4);
                                    otvet = b1.Name;
                                    break;
                                case 3:
                                    java2_2.Visibility = Visibility.Hidden;
                                    Met(javaa3, javaa3_1, javaa3_2, javaa3_3, javaa3_4);
                                    otvet = b4.Name;
                                    break;
                                case 4:
                                    java2_4.Visibility = Visibility.Visible;
                                    Met(javaa4, javaa4_1, javaa4_2, javaa4_3, javaa4_4);
                                    otvet = b1.Name;
                                    break;
                                case 5:
                                    java2_4.Visibility = Visibility.Hidden;
                                    java2_5.Visibility = Visibility.Visible;
                                    Met(javaa5, javaa5_1, javaa5_2, javaa5_3, javaa5_4);
                                    otvet = b3.Name;
                                    break;
                                case 6:
                                    java2_5.Visibility = Visibility.Hidden;
                                    Met(javaa6, javaa6_1, javaa6_2, javaa6_3, javaa6_4);
                                    otvet = b1.Name;
                                    break;
                                case 7:
                                    java2_7.Visibility = Visibility.Visible;
                                    Met(javaa7, javaa7_1, javaa7_2, javaa7_3, javaa7_4);
                                    otvet = b1.Name;
                                    break;
                                case 8:
                                    java2_7.Visibility = Visibility.Hidden;
                                    java2_8.Visibility = Visibility.Visible;
                                    Met(javaa8, javaa8_1, javaa8_2, javaa8_3, javaa8_4);
                                    otvet = b4.Name;
                                    break;
                                case 9:
                                    java2_8.Visibility = Visibility.Hidden;
                                    java2_9.Visibility = Visibility.Visible;
                                    Met(javaa9, javaa9_1, javaa9_2, javaa9_3, javaa9_4);
                                    otvet = b1.Name;
                                    break;
                                case 10:
                                    java2_9.Visibility = Visibility.Hidden;
                                    Met(javaa10, javaa10_1, javaa10_2, javaa10_3, javaa10_4);
                                    otvet = b2.Name;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case 3:  // Сложный уровень
                            switch (count)
                            {
                                case 1: // вопросы
                                    java3_1.Visibility = Visibility.Visible;
                                    Met(javaaa1, javaaa1_1, javaaa1_2, javaaa1_3, javaaa1_4);
                                    otvet = b1.Name;
                                    break;
                                case 2:
                                    java3_1.Visibility = Visibility.Hidden;
                                    java3_2.Visibility = Visibility.Visible;
                                    Met(javaaa2, javaaa2_1, javaaa2_2, javaaa2_3, javaaa2_4);
                                    otvet = b2.Name;
                                    break;
                                case 3:
                                    java3_2.Visibility = Visibility.Hidden;
                                    java3_3.Visibility = Visibility.Visible;
                                    Met(javaaa3, javaaa3_1, javaaa3_2, javaaa3_3, javaaa3_4);
                                    otvet = b3.Name;
                                    break;
                                case 4:
                                    java3_3.Visibility = Visibility.Hidden;
                                    Met(javaaa4, javaaa4_1, javaaa4_2, javaaa4_3, javaaa4_4);
                                    otvet = b3.Name;
                                    break;
                                case 5:
                                    Met(javaaa5, javaaa5_1, javaaa5_2, javaaa5_3, javaaa5_4);
                                    otvet = b2.Name;
                                    break;
                                case 6:
                                    Met(javaaa6, javaaa6_1, javaaa6_2, javaaa6_3, javaaa6_4);
                                    otvet = b1.Name;
                                    break;
                                case 7:
                                    java3_7.Visibility = Visibility.Visible;
                                    Met(javaaa7, javaaa7_1, javaaa7_2, javaaa7_3, javaaa7_4);
                                    otvet = b3.Name;
                                    break;
                                case 8:
                                    java3_7.Visibility = Visibility.Hidden;
                                    java3_8.Visibility = Visibility.Visible;
                                    Met(javaaa8, javaaa8_1, javaaa8_2, javaaa8_3, javaaa8_4);
                                    otvet = b2.Name;
                                    break;
                                case 9:
                                    java3_8.Visibility = Visibility.Hidden;
                                    java3_9.Visibility = Visibility.Visible;
                                    Met(javaaa9, javaaa9_1, javaaa9_2, javaaa9_3, javaaa9_4);
                                    otvet = b3.Name;
                                    break;
                                case 10:
                                    java3_9.Visibility = Visibility.Hidden;
                                    Met(javaaa10, javaaa10_1, javaaa10_2, javaaa10_3, javaaa10_4);
                                    otvet = b4.Name;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        default:
                            break;
                    }
                    break;
                case 4: // Python
                    switch (lev)
                    {
                        case 1:  // Начальный уровень
                            switch (count)
                            {
                                case 1: // вопросы
                                    Met(python1, python1_1, python1_2, python1_3, python1_4);
                                    otvet = b3.Name;
                                    break;
                                case 2:
                                    Met(python2, python22_1, python22_2, python22_3, python22_4);
                                    otvet = b2.Name;
                                    break;
                                case 3:
                                    Met(python3, python3_1, python3_2, python33_3, python3_4);
                                    otvet = b3.Name;
                                    break;
                                case 4:
                                    Met(python4, python4_1, python4_2, python4_3, python4_4);
                                    otvet = b4.Name;
                                    break;
                                case 5:
                                    Met(python5, python5_1, python5_2, python5_3, python5_4);
                                    otvet = b1.Name;
                                    break;
                                case 6:
                                    Met(python6, python6_1, python6_2, python6_3, python6_4);
                                    otvet = b4.Name;
                                    break;
                                case 7:
                                    Met(python7, python7_1, python7_2, python7_3, python7_4);
                                    otvet = b3.Name;
                                    break;
                                case 8:
                                    Met(python8, python8_1, python8_2, python8_3, python8_4);
                                    otvet = b1.Name;
                                    break;
                                case 9:
                                    Met(python9, python9_1, python9_2, python9_3, python9_4);
                                    otvet = b1.Name;
                                    break;
                                case 10:
                                    Met(python10, python10_1, python10_2, python10_3, python10_4);
                                    otvet = b1.Name;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case 2: // Средний уровень
                            switch (count)
                            {
                                case 1: // вопросы
                                    python2_1.Visibility = Visibility.Visible;
                                    Met(pythonn1, pythonn1_1, pythonn1_2, pythonn1_3, pythonn1_4);
                                    otvet = b1.Name;
                                    break;
                                case 2:
                                    python2_1.Visibility = Visibility.Hidden;
                                    python2_2.Visibility = Visibility.Visible;
                                    Met(pythonn2, pythonn2_1, pythonn2_2, pythonn2_3, pythonn2_4);
                                    otvet = b2.Name;
                                    break;
                                case 3:
                                    python2_2.Visibility = Visibility.Hidden;
                                    python2_3.Visibility = Visibility.Visible;
                                    Met(pythonn3, pythonn3_1, pythonn3_2, pythonn3_3, pythonn3_4);
                                    otvet = b2.Name;
                                    break;
                                case 4:
                                    python2_3.Visibility = Visibility.Hidden;
                                    python2_4.Visibility = Visibility.Visible;
                                    Met(pythonn4, pythonn4_1, pythonn4_2, pythonn4_3, pythonn4_4);
                                    otvet = b3.Name;
                                    break;
                                case 5:
                                    python2_4.Visibility = Visibility.Hidden;
                                    python2_5.Visibility = Visibility.Visible;
                                    Met(pythonn5, pythonn5_1, pythonn5_2, pythonn5_3, pythonn5_4);
                                    otvet = b1.Name;
                                    break;
                                case 6:
                                    python2_5.Visibility = Visibility.Hidden;
                                    python2_6.Visibility = Visibility.Visible;
                                    Met(pythonn6, pythonn6_1, pythonn6_2, pythonn6_3, pythonn6_4);
                                    otvet = b4.Name;
                                    break;
                                case 7:
                                    python2_6.Visibility = Visibility.Hidden;
                                    Met(pythonn7, pythonn7_1, pythonn7_2, pythonn7_3, pythonn7_4);
                                    otvet = b3.Name;
                                    break;
                                case 8:
                                    Met(pythonn8, pythonn8_1, pythonn8_2, pythonn8_3, pythonn8_4);
                                    otvet = b4.Name;
                                    break;
                                case 9:
                                    Met(pythonn9, pythonn9_1, pythonn9_2, pythonn9_3, pythonn9_4);
                                    otvet = b1.Name;
                                    break;
                                case 10:
                                    Met(pythonn10, pythonn10_1, pythonn10_2, pythonn10_3, pythonn10_4);
                                    otvet = b3.Name;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case 3:  // Сложный уровень
                            switch (count)
                            {
                                case 1: // вопросы
                                    Met(pythonnn1, pythonnn1_1, pythonnn1_2, pythonnn1_3, pythonnn1_4);
                                    otvet = b1.Name;
                                    break;
                                case 2:
                                    Met(pythonnn2, pythonnn2_1, pythonnn2_2, pythonnn2_3, pythonnn2_4);
                                    otvet = b2.Name;
                                    break;
                                case 3:
                                    python3_3.Visibility = Visibility.Visible;
                                    Met(pythonnn3, pythonnn3_1, pythonnn3_2, pythonnn3_3, pythonnn3_4);
                                    otvet = b2.Name;
                                    break;
                                case 4:
                                    python3_3.Visibility = Visibility.Hidden;
                                    Met(pythonnn4, pythonnn4_1, pythonnn4_2, pythonnn4_3, pythonnn4_4);
                                    otvet = b3.Name;
                                    break;
                                case 5:
                                    python3_5.Visibility = Visibility.Visible;
                                    Met(pythonnn5, pythonnn5_1, pythonnn5_2, pythonnn5_3, pythonnn5_4);
                                    otvet = b1.Name;
                                    break;
                                case 6:
                                    python3_5.Visibility = Visibility.Hidden;
                                    Met(pythonnn6, pythonnn6_1, pythonnn6_2, pythonnn6_3, pythonnn6_4);
                                    otvet = b4.Name;
                                    break;
                                case 7:
                                    python3_7.Visibility = Visibility.Visible;
                                    Met(pythonnn7, pythonnn7_1, pythonnn7_2, pythonnn7_3, pythonnn7_4);
                                    otvet = b3.Name;
                                    break;
                                case 8:
                                    python3_7.Visibility = Visibility.Hidden;
                                    python3_8.Visibility = Visibility.Visible;
                                    Met(pythonnn8, pythonnn8_1, pythonnn8_2, pythonnn8_3, pythonnn8_4);
                                    otvet = b4.Name;
                                    break;
                                case 9:
                                    python3_8.Visibility = Visibility.Hidden;
                                    Met(pythonnn9, pythonnn9_1, pythonnn9_2, pythonnn9_3, pythonnn9_4);
                                    otvet = b1.Name;
                                    break;
                                case 10:
                                    Met(pythonnn10, pythonnn10_1, pythonnn10_2, pythonnn10_3, pythonnn10_4);
                                    otvet = b3.Name;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        default:
                            break;
                    }
                    break;
                case 5: // C++
                    switch (lev)
                    {
                        case 1:  // Начальный уровень 
                            switch (count)
                            {
                                case 1: // вопросы
                                    Met("Какой из вариантов записи подключения стандартной библиотеки является правильным с точки\nзрения стандарта C++?", "#include 'iostream'", "#include <iostream>", "Оба варианта неправильные", "Оба варианта правильные");
                                    otvet = b2.Name;
                                    break;
                                case 2:
                                    Met("Какой из перечисленных модификаторов является модификатором разрема?", "long", "signed", "unsigned", "Ни один из перечисленных не является модификатором размера");
                                    otvet = b1.Name;
                                    break;
                                case 3:
                                    Met("Выберите недопустимый идентификатор переменной из нижеуказанного.", "Int", "bool", "DOUBLE", "0");
                                    otvet = b2.Name;
                                    break;
                                case 4:
                                    Met("Какая из записей является ошибочной?", "Все варианты правильные", "Все варианты неправильные", "double a2 {2.3}", "int d2 = {2.3}");
                                    otvet = b4.Name;
                                    break;
                                case 5:
                                    cplus1_5.Visibility = Visibility.Visible;
                                    Met("Какой результат работы следующей программы?", "10", "'мусор", "Ошибка компиляции", "Ошибка выполнения");
                                    otvet = b3.Name;
                                    break;
                                case 6:
                                    cplus1_5.Visibility = Visibility.Hidden;
                                    Met("Какой из вариантов записи спецификатора auto является ошибочным?", "auto b; b = 14", "const auto a = 'Hello'", "auto a = -58", "auto = 0");
                                    otvet = b1.Name;
                                    break;
                                case 7:
                                    Met("Кто является автором языка программирования C++?", "Charles Babbage", "Dennis Ritchie", "Brain Kernighan", "Bjarne Stroustrup");
                                    otvet = b4.Name;
                                    break;
                                case 8:
                                    Met("Какой из перечисленных типов данных не является фундаментальным?", "double", "void", "string", "bool");
                                    otvet = b3.Name;
                                    break;
                                case 9:
                                    cplus1_9.Visibility = Visibility.Visible;
                                    Met("Сколько раз выполнится цикл?", "Один", "Два", "Цикл бесконечный", "Не выполнится, в коде есть ошибка");
                                    otvet = b1.Name;
                                    break;
                                case 10:
                                    cplus1_9.Visibility = Visibility.Hidden;
                                    Met("Как нужно объявлять константу?", "#define x = 10", "int = 10", "const int = 10", "const 5");
                                    otvet = b3.Name;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case 2: // Средний уровень
                            switch (count)
                            {
                                case 1: // вопросы
                                    cplus2_1.Visibility = Visibility.Visible;
                                    Met("Сколько раз вызовется любая move операция класса x?", "Хотя бы раз", "Хотя бы два раза", "Нисколько", "Хотя бы три раза");
                                    otvet = b1.Name;
                                    break;
                                case 2:
                                    cplus2_1.Visibility = Visibility.Hidden;
                                    Met("Что такое рефакторинг?", "Процесс изменения внутренней структуры программы, не затрагивающий ее внешнего поведения", "Тоже самое, что и компиляция", "Удаление неиспользуемых классов, функций и переменных", "Сборка мусора");
                                    otvet = b1.Name;
                                    break;
                                case 3:
                                    Met("Что такое enum?", "Перечисление", "Целочисленный тип данных", "Такое не используется в C++", "Команда");
                                    otvet = b2.Name;
                                    break;
                                case 4:
                                    Met("Для чего нужен оператор continue в цикле?", "Позволяет сразу перейти в начало тела цикла", "Позволяет продолжить работу после применения оператора return", "Позволяет продолжить работу программы в случае возникновения ошибки или исключения", "Позволяет сразу перейти в конец тела цикла, пропуская весь код, который находится под ним");
                                    otvet = b4.Name;
                                    break;
                                case 5:
                                    Met("Что такое абстрактный метод?", "Метод класса, в котором присутствуют абстрактные поля", "Метод абстрактного класса", "Метод класса, в котором присутствует инкапсуляция", "Метод класса, реализация для которого отсутствует");
                                    otvet = b4.Name;
                                    break;
                                case 6:
                                    Met("Что нужно подключить для работы с файлами?", "Заголовочный файл fstream", "Библиотеку file", "Ничего, работа с файлами есть в стандартной библиотеке", "Библиотеку IO");
                                    otvet = b1.Name;
                                    break;
                                case 7:
                                    Met("Как еще называют логическое отрицание?", "Инверсия", "Дизъюнкция", "Импликация", "Инкрементация");
                                    otvet = b1.Name;
                                    break;
                                case 8:
                                    Met("Что такое flush?", "Чтение файла", "Освобождение буфера", "Запись в файл", "Заполнение буфера");
                                    otvet = b2.Name;
                                    break;
                                case 9:
                                    Met("Что означает ключевое слово override?", "Свойство, которое может принимать несколько значений", "Класс, который наследуется от абстрактного", "Виртуальный метод, который переопределяет виртуальный метод базового класса", "Тип данных");
                                    otvet = b3.Name;
                                    break;
                                case 10:
                                    Met("Как объявить список значений типа под названием num?", "list&lt;T&gt; num", "collection&lt;int&gt; num", "collection&lt;int&gt; num", "list&lt;int&gt; num");
                                    otvet = b4.Name;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case 3:  // Сложный уровень
                            switch (count)
                            {
                                case 1: // вопросы
                                    cplus3_1.Visibility = Visibility.Visible;
                                    Met("Что выведет программа?", "d", "di", "i", "Не скомпилируется");
                                    otvet = b1.Name;
                                    break;
                                case 2:
                                    cplus3_2.Visibility = Visibility.Visible;
                                    cplus3_1.Visibility = Visibility.Hidden;
                                    Met("Какой контейнер типа Container нельзя передавать в\nфункцию EraseIf?", "std::unordered_set", "std::map", "std::list", "std::vector");
                                    otvet = b4.Name;
                                    break;
                                case 3:
                                    cplus3_3.Visibility = Visibility.Visible;
                                    cplus3_2.Visibility = Visibility.Hidden;
                                    Met("Что напечатает программа?", "Не скомпилируется", "0", "1", "Implementation defined");
                                    otvet = b3.Name;
                                    break;
                                case 4:
                                    cplus3_4.Visibility = Visibility.Visible;
                                    cplus3_3.Visibility = Visibility.Hidden;
                                    Met("Что выведет программа?", "021", "210", "201", "012");
                                    otvet = b2.Name;
                                    break;
                                case 5:
                                    cplus3_5.Visibility = Visibility.Visible;
                                    cplus3_4.Visibility = Visibility.Hidden;
                                    Met("Что выведет программа?", "Не скомпилируется", "0", "1", "Неопределенное поведение");
                                    otvet = b3.Name;
                                    break;
                                case 6:
                                    cplus3_6.Visibility = Visibility.Visible;
                                    cplus3_5.Visibility = Visibility.Hidden;
                                    Met("Укажите performance проблему с эти кодом из представленных\nвариантов.", "Размер вектора будет расти во время цикла, стоит сделать .reserve перед циклом", "Код оптимален, простых оптимизаций нет", "push_back сделает копию возвращаемых значений, надо использовать emplace_back", "Код содержит неопределенное поведение, т.к. ConvertData генерирует временный объект");
                                    otvet = b1.Name;
                                    break;
                                case 7:
                                    cplus3_7.Visibility = Visibility.Visible;
                                    cplus3_6.Visibility = Visibility.Hidden;
                                    Met("Код скомпилируется?", "Нет, вызывать std::move в capture нельзя", "Да, переменные в lambda неконстантные, поэтому их можно менять", "Да, так как только константные переменные могут изменяться, if contexpr не разрешит", "Нет, переменные в lambda иммутабельные, но decltype вернет, что тип мутабельный");
                                    otvet = b4.Name;
                                    break;
                                case 8:
                                    cplus3_8.Visibility = Visibility.Visible;
                                    cplus3_7.Visibility = Visibility.Hidden;
                                    Met("Какой контейнер типа Container нельзя передавать в функцию\nEraself?", "std::list", "std::vector", "std::map", "Все варианты верные");
                                    otvet = b4.Name;
                                    break;
                                case 9:
                                    cplus3_9.Visibility = Visibility.Visible;
                                    cplus3_8.Visibility = Visibility.Hidden;
                                    Met("Что выведет программа?", "A1", "B1", "A2", "B2");
                                    otvet = b2.Name;
                                    break;
                                case 10:
                                    cplus3_9.Visibility = Visibility.Hidden;
                                    Met("Что такое конъюнкция?", "Логическое сложение", "Логическое умножение", "Функция для обработки строк", "Вычитание строк");
                                    otvet = b2.Name;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        default:
                            break;
                    }
                    break;
                case 6: // C#
                    switch (lev)
                    {
                        case 1:  // Начальный уровень
                            switch (count)
                            {
                                case 1: // вопросы
                                    Met(csh1, csh1_1, csh1_2, csh1_3, csh1_4);
                                    otvet = b4.Name;
                                    break;
                                case 2:
                                    Met(csh2, csh2_1, csh2_2, csh2_3, csh2_4);
                                    break;
                                case 3:
                                    Met(csh3, csh3_1, csh3_2, csh3_3, csh3_4);
                                    otvet = b2.Name;
                                    break;
                                case 4:
                                    Met(csh4, csh4_1, csh4_2, csh4_3, csh4_4);
                                    otvet = b1.Name;
                                    break;
                                case 5:
                                    Met(csh5, csh5_1, csh5_2, csh5_3, csh5_4);
                                    otvet = b4.Name;
                                    break;
                                case 6:
                                    Met(csh6, csh6_1, csh6_2, csh6_3, csh6_4);
                                    otvet = b1.Name;
                                    break;
                                case 7:
                                    Met(csh7, csh7_1, csh7_2, csh7_3, csh7_4);
                                    otvet = b3.Name;
                                    break;
                                case 8:
                                    Met(csh8, csh8_1, csh8_2, csh8_3, csh8_4);
                                    otvet = b2.Name;
                                    break;
                                case 9:
                                    Met(csh9, csh9_1, csh9_2, csh9_3, csh9_4);
                                    otvet = b1.Name;
                                    break;
                                case 10:
                                    Met(csh10, csh10_1, csh10_2, csh10_3, csh10_4);
                                    otvet = b2.Name;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case 2: // Средний уровень
                            switch (count)
                            {
                                case 1: // вопросы
                                    Met(cshh1, cshh1_1, cshh1_2, cshh1_3, cshh1_4);
                                    otvet = b2.Name;
                                    break;
                                case 2:
                                    Met(cshh2, cshh2_1, cshh2_2, cshh2_3, cshh2_4);
                                    otvet = b4.Name;
                                    break;
                                case 3:
                                    Met(cshh3, cshh3_1, cshh3_2, cshh3_3, cshh3_4);
                                    otvet = b3.Name;
                                    break;
                                case 4:
                                    Met(cshh4, cshh4_1, cshh4_2, cshh4_3, cshh4_4);
                                    otvet = b1.Name;
                                    break;
                                case 5:
                                    Met(cshh5, cshh5_1, cshh5_2, cshh5_3, cshh5_4);
                                    otvet = b2.Name;
                                    break;
                                case 6:
                                    Met(cshh6, cshh6_1, cshh6_2, cshh6_3, cshh6_4);
                                    otvet = b4.Name;
                                    break;
                                case 7:
                                    Met(cshh7, cshh7_1, cshh7_2, cshh7_3, cshh7_4);
                                    otvet = b1.Name;
                                    break;
                                case 8:
                                    Met(cshh8, cshh8_1, cshh8_2, cshh8_3, cshh8_4);
                                    otvet = b2.Name;
                                    break;
                                case 9:
                                    Met(cshh9, cshh9_1, cshh9_2, cshh9_3, cshh9_4);
                                    otvet = b4.Name;
                                    break;
                                case 10:
                                    Met(cshh10, cshh10_1, cshh10_2, cshh10_3, cshh10_4);
                                    otvet = b3.Name;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case 3:  // Сложный уровень
                            switch (count)
                            {
                                case 1: // вопросы
                                    Met(cshhh1, cshhh1_1, cshhh1_2, cshhh1_3, cshhh1_4);
                                    otvet = b2.Name;
                                    break;
                                case 2:
                                    Met(cshhh2, cshhh2_1, cshhh2_2, cshhh2_3, cshhh2_4);
                                    otvet = b3.Name;
                                    break;
                                case 3:
                                    Met(cshhh3, cshhh3_1, cshhh3_2, cshhh3_3, cshhh3_4);
                                    otvet = b1.Name;
                                    break;
                                case 4:
                                    Met(cshhh4, cshhh4_1, cshhh4_2, cshhh4_3, cshhh4_4);
                                    otvet = b4.Name;
                                    break;
                                case 5:
                                    Met(cshhh5, cshhh5_1, cshhh5_2, cshhh5_3, cshhh5_4);
                                    otvet = b3.Name;
                                    break;
                                case 6:
                                    Met(cshhh6, cshhh6_1, cshhh6_2, cshhh6_3, cshhh6_4);
                                    otvet = b2.Name;
                                    break;
                                case 7:
                                    Met(cshhh7, cshhh7_1, cshhh7_2, cshhh7_3, cshhh7_4);
                                    otvet = b4.Name;
                                    break;
                                case 8:
                                    Met(cshhh8, cshhh8_1, cshhh8_2, cshhh8_3, cshhh8_4);
                                    otvet = b1.Name;
                                    break;
                                case 9:
                                    Met(cshhh9, cshhh9_1, cshhh9_2, cshhh9_3, cshhh9_4);
                                    otvet = b1.Name;
                                    break;
                                case 10:
                                    Met(cshhh10, cshhh10_1, cshhh10_2, cshhh10_3, cshhh10_4);
                                    otvet = b3.Name;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        default:
                            break;
                    }
                    break;
                case 7: // Java Script
                    switch (lev)
                    {
                        case 1:  // Начальный уровень
                            switch (count)
                            {
                                case 1: // вопросы
                                    Met("Как правильно объявить массив?", "var colors = \"red\", \"green\", \"blue\"", "var colors = 1 = (\"red\"), 2 = (\"green\"), 3 = (\"blue\")", "var colors = (1:\"red\", 2:\"green\", 3:\"blue\"", "var colors = [\"red\", \"green\", \"blue\"]");
                                    otvet = b4.Name;
                                    break;
                                case 2:
                                    Met("Какой из вариантов откроет новое окно?", "w2 = window.new(\"http://proghub.ru\");", "w2 = window.open(\"http://proghub.ru\")", "w2 = window.create(\"http://proghub.ru\")", "w2 = window.get(\"http://proghub.ru\")");
                                    otvet = b2.Name;
                                    break;
                                case 3:
                                    Met("JavaScript регистрозависимый язык?", "Да", "Нет", "Данное свойство можно изменить в настройках", "Зависит от среды разработки");
                                    otvet = b1.Name;
                                    break;
                                case 4:
                                    js1_4.Visibility = Visibility.Visible;
                                    Met("Что будет выведено в консоль?", "0", "1", "Error", "Ничего");
                                    otvet = b2.Name;
                                    break;
                                case 5:
                                    js1_4.Visibility = Visibility.Hidden;
                                    Met("Что такое ajax?", "Библиотека для асинхронной работы с данными", "Библиотека для асинхронной работы с DOM-элементами", "Группа связанных технологий, используемых для асинхронного отображения данных", "Библиотека для работы с потоками");
                                    otvet = b3.Name;
                                    break;
                                case 6:
                                    Met("Как объявить функцию в JS?", "def fn(){}", "set fn = function(){}", "public static function(){}", "function fn(){}");
                                    otvet = b4.Name;
                                    break;
                                case 7:
                                    Met("Как написать if в JavaScript?", "if i = 5 then", "if i = 5", "if (i == 5)", "if i == 5 then");
                                    otvet = b3.Name;
                                    break;
                                case 8:
                                    Met("Как правильно объявить цикл while?", "while (i <= 10)", "while (i <= 10; i++)", "while i = 1 to 10", "while i == 1 then");
                                    otvet = b1.Name;
                                    break;
                                case 9:
                                    js1_9.Visibility = Visibility.Visible;
                                    Met("Что выведет в консоль?", "Ничего не выведет, возникнет ошибка", "false", "true", "undefined");
                                    otvet = b2.Name;
                                    break;
                                case 10:
                                    js1_9.Visibility = Visibility.Hidden;
                                    Met("Что выведет следующий код?\nconsole.log(employeeId);\nvar employeeId = '19000';", "undefined", "some value", "TypeError", "ReferenceError: employeeId is not defined");
                                    otvet = b1.Name;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case 2: // Средний уровень
                            switch (count)
                            {
                                case 1: // вопросы
                                    Met("Как сгенерировать случайное целое число от 1 до 10?", "Math.random(10)+1;", "Math.random() * 10+1;", "Math.floor(Math.random(10)+1);", "Math.floor((Math.random() * 10) + 1;");
                                    otvet = b4.Name;
                                    break;
                                case 2:
                                    js2_2.Visibility = Visibility.Visible;
                                    Met("Результатов выполнения следующего кода будет:", "31337", "ReferenceError", "TypeError", "SyntaxError");
                                    otvet = b3.Name;
                                    break;
                                case 3:
                                    js2_2.Visibility = Visibility.Hidden;
                                    js2_3.Visibility = Visibility.Visible;
                                    Met("Что выведет на экран следующий код?", "2", "3", "obj", "Произойдет ошибка");
                                    otvet = b1.Name;
                                    break;
                                case 4:
                                    js2_4.Visibility = Visibility.Visible;
                                    js2_3.Visibility = Visibility.Hidden;
                                    Met("Что выведет этот код?", "false false", "boolean false", "true false", "boolean true");
                                    otvet = b2.Name;
                                    break;
                                case 5:
                                    js2_5.Visibility = Visibility.Visible;
                                    js2_4.Visibility = Visibility.Hidden;
                                    Met("Что выведет код?", "{1, 2, 3, 4}", "{1, 1, 2, 3, 4}", "[1, 2, 3, 4]", "[1, 1, 2, 3, 4]");
                                    otvet = b1.Name;
                                    break;
                                case 6:
                                    js2_6.Visibility = Visibility.Visible;
                                    js2_5.Visibility = Visibility.Hidden;
                                    Met("Дан код. Какое значение будет выведено в консоли?", "true", "false", "1", "0");
                                    otvet = b4.Name;
                                    break;
                                case 7:
                                    js2_7.Visibility = Visibility.Visible;
                                    js2_6.Visibility = Visibility.Hidden;
                                    Met("Что выведет этот код?", "false true true false", "false false true false", "false true true true", "true true true false");
                                    otvet = b3.Name;
                                    break;
                                case 8:
                                    js2_8.Visibility = Visibility.Visible;
                                    js2_7.Visibility = Visibility.Hidden;
                                    Met("Что выведет следующий код?", "1", "5", "undefined", "null");
                                    otvet = b3.Name;
                                    break;
                                case 9:
                                    js2_8.Visibility = Visibility.Hidden;
                                    js2_9.Visibility = Visibility.Visible;
                                    Met("Каким будет результат выполнения данного кода?", "[\"mytest\"]", "[\"myteststring\"]", "[\"m,y,t,e,s,t,s,t,i,n,g\"]", "[\"m,y,t,e,s,t\"]");
                                    otvet = b2.Name;
                                    break;
                                case 10:
                                    js2_9.Visibility = Visibility.Hidden;
                                    Met("Что выведет Alert?\nalert( 0 / 0 );", "NaN", "Infinity", "Undefined", "Ничего");
                                    otvet = b1.Name;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case 3:  // Сложный уровень
                            switch (count)
                            {
                                case 1: // вопросы
                                    Met("Какое значение будет на выходе?\nconsole.loog('${(x => x)('I love')} to program')", "I love to program", "undefined to program", "${(x => x)('I love') to program}", "TypeError");
                                    otvet = b1.Name;
                                    break;
                                case 2:
                                    js3_2.Visibility = Visibility.Visible;
                                    Met("Что выведет код?", "null \"\" true", "{} \"\" []", "null null true", "false null []");
                                    otvet = b2.Name;
                                    break;
                                case 3:
                                    js3_3.Visibility = Visibility.Visible;
                                    js3_2.Visibility = Visibility.Hidden;
                                    Met("Какие методы вернут значение Hello world! ?", "myMap.get('greeting')", "myMap.get(() => 'greeting')", "myMap.get(myFinc)", "Нет верного ответа");
                                    otvet = b3.Name;
                                    break;
                                case 4:
                                    js3_4.Visibility = Visibility.Visible;
                                    js3_3.Visibility = Visibility.Hidden;
                                    Met("Что выведет код?", "Прости, ты слишком молод для этого :(", "А ты достаточно стар для этого :)", "ReferenceError", "undefined");
                                    otvet = b3.Name;
                                    break;
                                case 5:
                                    js3_4.Visibility = Visibility.Hidden;
                                    Met("Что возвращает метод setInterval?\nset interval(() => console.log(\"Hi\"), 1000);", "Уникальный id", "Указанное количество миллисекунд", "Переданную функцию", "undefined");
                                    otvet = b1.Name;
                                    break;
                                case 6:
                                    js3_6.Visibility = Visibility.Visible;
                                    Met("Что выведет код?", "true", "false", "undefined", "TypeError");
                                    otvet = b4.Name;
                                    break;
                                case 7:
                                    js3_7.Visibility = Visibility.Visible;
                                    js3_6.Visibility = Visibility.Hidden;
                                    Met("Что выведет код?", "undefined", "{ name: \"taras\", age: 26 }", "Error", "taras, 26");
                                    otvet = b1.Name;
                                    break;
                                case 8:
                                    js3_8.Visibility = Visibility.Visible;
                                    js3_7.Visibility = Visibility.Hidden;
                                    Met("Что выведет код?", "{ constructor: ...} { constructor: ...}", "{ constructor: ...} undefined", "{} { constructor: ...}", "{ constructor: ...} {}");
                                    otvet = b2.Name;
                                    break;
                                case 9:
                                    js3_9.Visibility = Visibility.Visible;
                                    js3_8.Visibility = Visibility.Hidden;
                                    Met("Какой будет вывод?", "\"Ihor\"", "\"myName\"", "undefined", "ReferenceError");
                                    otvet = b4.Name;
                                    break;
                                case 10:
                                    js3_9.Visibility = Visibility.Hidden;
                                    Met("Чему равно значение?\nPromise.resolve(5)", "5", "Promise {<pending>: 5}", "Promise {<fullfilled>: 5}", "Error");
                                    otvet = b3.Name;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        default:
                            break;
                    }
                    break;
                case 8: // HTML
                    switch (lev)
                    {
                        case 1:  // Начальный уровень
                            switch (count)
                            {
                                case 1: // вопросы
                                    Met("Какой атрибут тега <ol> начинает нумерацию списка с определенного значения?", "type", "number", "start", "begin");
                                    otvet = b3.Name;
                                    break;
                                case 2:
                                    Met("Какой HTML-тэг используется для отображения на странице программного кода?", "programm", "script", "program", "code");
                                    otvet = b4.Name;
                                    break;
                                case 3:
                                    Met("Что означает тег <marquee>?", "Бегущая строка", "Маркированный текст", "Параграф", "Изображение-ссылка");
                                    otvet = b1.Name;
                                    break;
                                case 4:
                                    Met("Из предложенных вариантов указания заголовка в таблице, выберите правильный.", "<table><caption>title</caption>...</table>", "<table caption=\"titile\">...</table>", "<table title=\"title\">...</table>", "<table lable=\"title\">...</table>");
                                    otvet = b1.Name;
                                    break;
                                case 5:
                                    Met("Выберите html-код, создающий checkbox.", "<checkbox/>", "<input type=\"checkbox\"/>", "<check/>", "<input type=\"check\"/>");
                                    otvet = b2.Name;
                                    break;
                                case 6:
                                    Met("Какого значения не может быть у параметра align в теге <img>?", "bottom", "right", "left", "center");
                                    otvet = b4.Name;
                                    break;
                                case 7:
                                    Met("Выберите фрагмент HTML-кода, создающий ссылку со всплывающей подсказкой.", "<a title='подсказка'>текст ссылки</a>", "<a help='подсказка'>текст ссылки</a>", "<a baloon='подсказка'>текст ссылки</a>", "<a tip='подсказка'>текст ссылки</a>");
                                    otvet = b3.Name;
                                    break;
                                case 8:
                                    Met("Что определяет атрибут BORDER у элемента разметки TABLE?", "Определяет толщину рамки", "Определяет расположение таблицы в документе", "Управляет линиями, разделяющими ячейки таблицы", "Устанавливает цвет окантовки");
                                    otvet = b1.Name;
                                    break;
                                case 9:
                                    html1_9.Visibility = Visibility.Visible;
                                    Met("Является ли следующий HTML код валидным\nи well-formed?", "Да", "Только валидным", "Только well-formed", "Код не является валидным и well-formed");
                                    otvet = b3.Name;
                                    break;
                                case 10:
                                    html1_9.Visibility = Visibility.Hidden;
                                    Met("Какой тег предназначен для заголовков наименьшего размера?", "<h1>", "<h6>", "<hmin>", "<h7>");
                                    otvet = b2.Name;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case 2: // Средний уровень
                            switch (count)
                            {
                                case 1: // вопросы..
                                    Met("С помощью какого тега следует разделять абзацы?", "<p>", "<br>", "<span>", "<b>");
                                    otvet = b1.Name;
                                    break;
                                case 2:
                                    Met("Какое число заголовков первого уровня считается допустимым?", "3", "2", "1", "Неограниченное количество");
                                    otvet = b3.Name;
                                    break;
                                case 3:
                                    Met("С помощью какого атрибута задается ширина поля textarea?", "width", "cols", "size", "rows");
                                    otvet = b2.Name;
                                    break;
                                case 4:
                                    Met("Каким является следующий адрес ссылки:\n/page2.html", "Полным", "Абсолютным", "Неполным", "Относительным");
                                    otvet = b4.Name;
                                    break;
                                case 5:
                                    Met("Как выделить текст курсивом?", "<hr>курсив</hr>", "<em>курсив</em>", "<br>курсив</br>", "<p>курсив</p>");
                                    otvet = b1.Name;
                                    break;
                                case 6:
                                    Met("С помощью какого атрибута можно задать текст для картинки, который будет отображен, если ее\nне удастся загрузить?", "title", "alt", "popup", "caption");
                                    otvet = b2.Name;
                                    break;
                                case 7:
                                    Met("С помощью какого свойства можно сделать отступы внутри ячейки в таблице?", "margin", "case", "space", "padding");
                                    otvet = b4.Name;
                                    break;
                                case 8:
                                    Met("Как сделать текст жирным?", "<strong>жирный</strong>", "<p>жирный</p>", "<a>жирный</a>", "<br>жирный</br>");
                                    otvet = b1.Name;
                                    break;
                                case 9:
                                    Met("Какой тег при создании страницы является необязательным?", "doctype", "head", "strong", "body");
                                    otvet = b3.Name;
                                    break;
                                case 10:
                                    Met("Каких тегов в HTML не существует?", "Парных", "Все есть", "Одиночных", "Тройных");
                                    otvet = b4.Name;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case 3:  // Сложный уровень
                            switch (count)
                            {
                                case 1: // вопросы
                                    Met("Выберите теги для работы с таблицами.", "<table><tr><td>", "<table><head><tfoot>", "<table><tr><tt>", "<thead><body><tr>");
                                    otvet = b1.Name;
                                    break;
                                case 2:
                                    Met("Выберите тег для создания ненумерованного списка.", "<dl>", "<list>", "<ol>", "<ul>");
                                    otvet = b4.Name;
                                    break;
                                case 3:
                                    Met("Выберите верный способ создания выпадающего списка.", "<select>", "<list>", "<input type=\"dropdown\">", "<input type=\"list\">");
                                    otvet = b1.Name;
                                    break;
                                case 4:
                                    Met("Какое значение по умолчанию имеет атрибут method у лега <form>?", "POST", "Атрибут methodn является обязательным и не имеет значения по умолчанию", "GET", "Значение по умолчани зависит от браузера");
                                    otvet = b3.Name;
                                    break;
                                case 5:
                                    Met("Необходимо защитить текстовое поле формы от изменения значения пользователем.\nКакое из представленных фрагментов кода позволят решить поставленную задачу?", "<imput value=\"$999\" checked/>", "<input value=\"$999\" size=\"0\"/>", "<input value=\"#999\" readonly/>", "<input value=\"$999\" maxlength=\"0\"/>");
                                    otvet = b3.Name;
                                    break;
                                case 6:
                                    Met("Выберите верный способ создания текстового поля для ввода информации.", "<input type=\"textfield\">", "<textinput type=\"text\">", "<textfield>", "<input type=\"text\"");
                                    otvet = b4.Name;
                                    break;
                                case 7:
                                    Met("Выберите верный способ создания многострочного текстового поля.", "<input type=\"textbox\">", "<textarea>", "<input type=\"textarea\"", "<textarea=\"textbox\">");
                                    otvet = b2.Name;
                                    break;
                                case 8:
                                    Met("Выберите верный способ создания чекбокса.", "<checkbox>", "<input type=\"checkbox\">", "<input type=\"check\">", "<check>");
                                    otvet = b2.Name;
                                    break;
                                case 9:
                                    Met("Выберите верный способ вставки изображения?", "<img alt=\"MyImage\">image.gif", "<img src=\"image.gif\" alt=\"MyImage\">", "<img href=\"image.gif\" alt=\"MyImage\">", "<image src=\"image.gif\" alt=\"MyImage\">");
                                    otvet = b2.Name;
                                    break;
                                case 10:
                                    Met("Выберите верный способ установки фонового изображения страницы?", "<body background=\"background.gif\">", "<ing src=\"background.gif\" background>", "<background img=\"background.gif\">", "<img background=\"background.gif\">");
                                    otvet = b1.Name;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        default:
                            break;
                    }
                    break;
                case 9: // CSS
                    switch (lev)
                    {
                        case 1:  // Начальный уровень
                            switch (count)
                            {
                                case 1: // вопросы
                                    Met("Чтобы задать форму курсора, необходимо использовать CSS свойство:", "cursor", "pointer", "arrow", "Нельзя задать свою форму курсора в CSS");
                                    otvet = b1.Name;
                                    break;
                                case 2:
                                    Met("Чтобы задать прозрачность элемента на веб-странице необходимо использовать свойство:", "background-color", "opacity", "display", "position");
                                    otvet = b2.Name;
                                    break;
                                case 3:
                                    Met("Предназначение свойства clear в CSS:", "Очищает всю страницу, кроме блока к которому применяется данное свойство", "Устанавливает, с какой стороны элемента запрещено его обтекание другими элементами", "Очищает блок, к которому применяется данное свойство", "Очищает всю страницу");
                                    otvet = b2.Name;
                                    break;
                                case 4:
                                    Met("В чем отличие свойства outline от border:", "Здесь нет правильного ответа", "Между ними нет разницы", "Свойство outline не влияет на положение блока и его ширину", "Свойство border не влияет на положение блока и его ширину");
                                    otvet = b3.Name;
                                    break;
                                case 5:
                                    Met("При абсолютном позиционировании, чтобы изменить позицию элемента необходимо использовать\nсвойства:", "left, top, right и bottom", "При абсолютном позиционировании нельзя изменять положение элемента", "Только top и bottom", "Только left и right");
                                    otvet = b1.Name;
                                    break;
                                case 6:
                                    Met("За что отвечает свойство text-transform?", "Вращение текста по часовой стрелке", "Изменением цвета текста", "Динамическим изменением шрифта", "Преобразованием текста в заглавные и прописные символы");
                                    otvet = b4.Name;
                                    break;
                                case 7:
                                    Met("Для чего предназначено свойство padding в CSS?", "Фоновый цвет", "Рамка для контейнера", "Внутренний отступ", "Внешний отступ");
                                    otvet = b3.Name;
                                    break;
                                case 8:
                                    Met("Для чего предназначено свойство z-index:", "Определяет размер элемента", "Ни на что оно не влияет", "Обычное позиционирование влево-вправо, вверх-вниз", "Располагать элементы в определенном порядке, имитируя третье измерение, которое перпендикулярно экрану");
                                    otvet = b4.Name;
                                    break;
                                case 9:
                                    Met("Выберите правильное значение свойства position:", "absolute", "top", "bottom", "left");
                                    otvet = b1.Name;
                                    break;
                                case 10:
                                    Met("Какое CSS свойство используется для изменения стиля самой ссылки?", "a:hover", "a:visited", "a:link", "a:vlink");
                                    otvet = b3.Name;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case 2: // Средний уровень
                            switch (count)
                            {
                                case 1: // вопросы
                                    Met("Какое из указанных значений свойства border-style, не существует?", "dashed", "dotted", "hiden", "Все свойства существуют");
                                    otvet = b3.Name;
                                    break;
                                case 2:
                                    Met("Сколько числовых значений может быть указано у свойства paddding?", "только одно", "до 4 включительно", "только два", "до 8 включительно");
                                    otvet = b2.Name;
                                    break;
                                case 3:
                                    css2_3.Visibility = Visibility.Visible;
                                    Met("Какой будет цвет у слова \"blah\"?", "Красный", "Фиолетовый", "Цвет по умолчанию", "Чёрный");
                                    otvet = b1.Name;
                                    break;
                                case 4:
                                    css2_3.Visibility = Visibility.Hidden;
                                    Met("Какую из операций не поддерживает функция calc()?", "+", "-", "*", "%");
                                    otvet = b4.Name;
                                    break;
                                case 5:
                                    Met("Выберите неправильный вариант указания размера текста.", "ek", "px", "pt", "проценты");
                                    otvet = b1.Name;
                                    break;
                                case 6:
                                    Met("Что необходимо написать в CSS чтобы изображение повторялось по вертикали:", "background-repeat: no-repeat;", "background-repeat: inherit;", "background-repeat: repeat-x;", "background-repeat: repeat-y;");
                                    otvet = b4.Name;
                                    break;
                                case 7:
                                    Met("Какой из вариантов задания цвета являются неверным?", "background-color: RGB(255, 0, 255);", "background-color: red;", "background-color:&hE99295;", "background-color:#ff00ff;");
                                    otvet = b3.Name;
                                    break;
                                case 8:
                                    Met("Выберите правильный вариант отступа сверху в 10 пикселей:", "margin: 0 10px 0 10px;", "Нет правильного ответа", "margin: 0 10px 10px 10px;", "margin: 10px 0 0 0;");
                                    otvet = b4.Name;
                                    break;
                                case 9:
                                    Met("Если для контейнера в CSS написать margin: 10px, то к чему будет применено данное свойство?", "К каждой из сторон контейнера", "К трем сторонам на усмотрение браузера", "Только к правой и левой стороне контейнера", "Только к верхней и нижней стороне контейнера");
                                    otvet = b1.Name;
                                    break;
                                case 10:
                                    Met("Какое CSS свойство используется для изменения стиля самой ссылки?", "a:hover", "a:visited", "a:link", "a:vlink");
                                    otvet = b3.Name;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case 3:  // Сложный уровень
                            switch (count)
                            {
                                case 1: // вопросы
                                    Met("Какая величина в медиа-запросах позволяет применять стили при определенном сотношении сторон\nна устройстве отображения?", "device-aspect-ratio", "min-orientation", "orientation", "min-device-width");
                                    otvet = b1.Name;
                                    break;
                                case 2:
                                    Met("При задании свойств padding в процентах, относительно чего считаются эти проценты?", "padding родительского элемента", "Ширины самого элемента", "margin родительского элемента", "Ширины родительского элемента");
                                    otvet = b4.Name;
                                    break;
                                case 3:
                                    Met("Какое из значений свойств display делает HTML-элемент строчно-блочным?", "inline", "inline-block", "block", "list-item");
                                    otvet = b2.Name;
                                    break;
                                case 4:
                                    Met("Какой порядок следования отступов у свойства padding: 10px 20px 30px 40px", "сверха, слева, снизу, справа", "справа, снизу, слева, сверху", "сверха, слева, справа, снизу", "сверха, справа, снизу, слева");
                                    otvet = b4.Name;
                                    break;
                                case 5:
                                    Met("Какое CSS свойство используется для изменения стиля уже кликнутой ссылки?", "a:hover", "a:vlink", "a:visited", "a:link");
                                    otvet = b3.Name;
                                    break;
                                case 6:
                                    Met("Какой из стилей подключения css верен?", "@import css(“styles.css”)", "<link href=”styles.css” type=”stylesheet”>", "@import url(“styles.css”)", "<style href=”styles.css” />");
                                    otvet = b3.Name;
                                    break;
                                case 7:
                                    Met("Какой из селекторов выберет все HTML-элементы div с атрибутом title содержащим внутри значения\nподслово «bar»?", "div[title*=\"bar\"]", "div[title|=\"bar\"]", "div[title^=\"bar\"]", "div[title~=\"bar\"]");
                                    otvet = b1.Name;
                                    break;
                                case 8:
                                    css3_8.Visibility = Visibility.Visible;
                                    Met("Какой будет цвет у слова \"blah\"?", "Фиолетовый", "Цвет по умолчанию", "Чёрный", "Красный");
                                    otvet = b2.Name;
                                    break;
                                case 9:
                                    css3_8.Visibility = Visibility.Hidden;
                                    Met("Селектор «.some .next span» выберет для оформления HTML-элемент span в следующих фрагментах\nразметки", "<div class=\"some next\"> <div><span>текст</span></div> </div>", "<div class=\"some\"> <div class=\"next\"><span>текст</span></div> </div>", "<div class=\"some next\"> <div><p class=\"next\"><span>текст</span></p></div> </div>", "<nav class=\"some\"><a href=\"#\" class=\"next\"><span></span></a></nav>");
                                    otvet = b1.Name;
                                    break;
                                case 10:
                                    Met("Какое CSS свойство используется для определения стиля при наведении на ссылку курсора мыши,\nно при этом элемент еще не активирован?", "a:hover", "a:visited", "a:vlink", "a:link");
                                    otvet = b1.Name;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }
        }

        private void git_res(object sender, RoutedEventArgs e)
        {
            ResultWindow W1 = new ResultWindow();
            W1.Show();
            this.Close();
        }

        private void git_Click(object sender, RoutedEventArgs e)
        {
            Button Name2 = (Button)sender;
            if (count == 11)
            {
                if (Name2.Name == otvet)
                {
                    res++;
                }

                result.Visibility = Visibility.Visible;
                count = 0;
            }
            else
            {
                if (Name2.Name == otvet)
                {
                    res++;
                }

                Method();
                count++;
            }
        }

        void Met(string v, string o1, string o2, string o3, string o4)
        {
            txt.Content = v;
            b1.Content = o1;
            b2.Content = o2;
            b3.Content = o3;
            b4.Content = o4;
        }
    }
}
