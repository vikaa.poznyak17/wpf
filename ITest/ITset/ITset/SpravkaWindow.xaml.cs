﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ITset
{
    /// <summary>
    /// Логика взаимодействия для SpravkaWindow.xaml
    /// </summary>
    public partial class SpravkaWindow : Window
    {
        public SpravkaWindow()
        {
            InitializeComponent();
        }

        private void button_backk_Click(object sender, RoutedEventArgs e)
        {
            Administrator W1 = new Administrator();
            W1.Show();
            this.Close();
        }
    }
}
